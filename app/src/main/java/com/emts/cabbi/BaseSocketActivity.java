//package com.emts.cabbi;
//
//import android.annotation.SuppressLint;
//import android.app.AlertDialog;
//import android.app.Dialog;
//import android.app.KeyguardManager;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.media.Ringtone;
//import android.media.RingtoneManager;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.CountDownTimer;
//import android.os.PowerManager;
//import android.os.VibrationEffect;
//import android.os.Vibrator;
//import android.support.annotation.Nullable;
//import android.support.v7.app.AppCompatActivity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.Button;
//import android.widget.LinearLayout;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//
//import com.android.volley.Request;
//import com.android.volley.VolleyError;
//import com.emts.cabbi.activity.MainActivity;
//import com.emts.cabbi.helper.AlertUtils;
//import com.emts.cabbi.helper.Api;
//import com.emts.cabbi.helper.Logger;
//import com.emts.cabbi.helper.VolleyHelper;
//import com.emts.cabbi.model.Booking;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.net.URISyntaxException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.HashMap;
//
//import io.socket.client.Ack;
//import io.socket.client.IO;
//import io.socket.client.Socket;
//import io.socket.emitter.Emitter;
//
//public abstract class BaseSocketActivity extends AppCompatActivity {
//    private static final String TAG = "BaseSocketActivity";
//    private static Dialog bookingDialog;
//    static CountDownTimer countDownTimer;
//
//    private static ProgressBar progressBar;
//    private static TextView textViewShowTime;
//
//    public static Socket socket;
//    public static boolean isActivityOnForGround;
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        isActivityOnForGround = true;
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        isActivityOnForGround = false;
//    }
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        connectToServerSocket();
//
//        if (bookingDialog == null) {
//            createAlert(this);
//        }
//    }
//
//    private void connectToServerSocket() {
//        if (socket != null && socket.connected()) {
//            return;
//        }
//        try {
//            socket = IO.socket(Api.getInstance().socketUrl);
//            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Logger.e(TAG + " connectToServerSocket event_connect", "****Connected : " + args);
//                }
//
//            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Logger.e(TAG + " connectToServerSocket event_disconnected", "Connection lost )))) : " + args);
//                }
//
//            });
//            socket.connect();
//        } catch (URISyntaxException e) {
//            Logger.e(TAG + " connectToServerSocket ex", e.getMessage() + " ");
//        }
//    }
//
//    //    START NODE JS LISTENERS
//    public void emitConnectAndJoinTeamRoom(String hostId, String gameId) {
//        if (socket.connected()) {
//            Logger.e(TAG + " emitConnectAndJoinTeamRoom emit", "emitter for socket connected and join to room");
//            //emitter to post data
//            try {
//                JSONObject roomObj = new JSONObject();
//                roomObj.put("user_id", hostId);
//                roomObj.put("game_id", gameId);
//                Logger.e(TAG + " nodeJs emitConnectAndJoinTeamRoom emit data", roomObj.toString() + " **");
//
//                socket.emit("connect_to_tournament_viewer_list", roomObj, new Ack() {
//                    @Override
//                    public void call(Object... args) {
//                        Logger.e(TAG + " emitConnectAndJoinTeamRoom ack",
//                                "Ack received for connect_to_tournament_viewer_list :" +
//                                        (args.length > 0 ? args[0].toString() : "0"));
//                    }
//                });
//            } catch (JSONException e) {
//                Logger.e(TAG + " emitConnectAndJoinTeamRoom ex15", e.getMessage() + "");
//            }
//        }
//    }
//
//    public void listenForReadyTeam() {
//        if (socket.connected()) {
//            Logger.e(TAG + " nodeJs listener listenForReadyTeam", "listener set for listenForReadyTeam");
//
//            socket.on("get_team_join_information", new Emitter.Listener() {
//                @SuppressLint("NewApi")
//                @Override
//                public void call(final Object... args) {
//                    Logger.e(TAG + " nodeJs response listenForReadyTeam -->", args.length + " __");
//
//                    if (args.length > 0) {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                Logger.e(TAG + " nodeJs listenForReadyTeam res", args[0].toString() + " **");
//                                try {
//                                    JSONObject resObj = new JSONObject(args[0].toString());
//                                } catch (JSONException e) {
//                                    Logger.e(TAG + " nodeJs listenForReadyTeam listen ex", e.getMessage() + " ");
//                                }
//                            }
//                        });
//                    }
//                }
//            });
//        } else {
//            Logger.e(TAG + " nodeJs listenForReadyTeam", "not connected !!!");
//        }
//    }
//
//    private static void createAlert(Context context) {
//        AlertDialog.Builder alert = new AlertDialog.Builder(context);
//        alert.setCancelable(false);
//        View alertView = LayoutInflater.from(context).inflate(R.layout.alert_booking, null);
//        alert.setView(alertView);
//        bookingDialog = alert.create();
//    }
//
//    @Override
//    protected void onDestroy() {
//        recycle();
//        super.onDestroy();
//    }
//
//    public void showBookingAlert(final Context context, final Booking booking) {
//        if (bookingDialog == null) createAlert(context);
//
//        if (!bookingDialog.isShowing()) {
//            vibrate(context);
//            playNotificationSound(context);
//            bookingDialog.show();
//            wakeUpTheScreen(context);
//
//        }
//        TextView tvDate = bookingDialog.findViewById(R.id.tv_booking_date_time);
//        Logger.e("booking dialog", tvDate + " ***");
//        SimpleDateFormat resFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        SimpleDateFormat requireFormatter = new SimpleDateFormat("dd MMM yyyy, HH:mm a");
//        try {
//            tvDate.setText(requireFormatter.format(resFormatter.parse(booking.getDate())));
//        } catch (ParseException e) {
//            tvDate.setText(booking.getDate());
//        }
//
//        progressBar = bookingDialog.findViewById(R.id.progressbar1_timerview);
//        textViewShowTime = bookingDialog.findViewById(R.id.textView_timerview_time);
//
//        startTimer(booking.getTimeToLive());
//        progressBar.setVisibility(View.VISIBLE);
//
//        LinearLayout layPickupLocation, layPassengerContact;
//        layPickupLocation = bookingDialog.findViewById(R.id.lay_pickup_location_holder);
//        layPassengerContact = bookingDialog.findViewById(R.id.lay_phone_holder);
//        TextView tvBookingType = bookingDialog.findViewById(R.id.tv_booking_type);
//        if (booking.getBookingType().equals(Booking.BOOKING_TYPE_INSTANT)) {
//            tvBookingType.setText("Instant Booking");
//            TextView tvPickupLocation = layPickupLocation.findViewById(R.id.tv_pickup_location);
//            tvPickupLocation.setText(booking.getPickUpLocation());
//            layPickupLocation.setVisibility(View.VISIBLE);
//            layPassengerContact.setVisibility(View.INVISIBLE);
//        } else {
//            tvBookingType.setText("Pre Booking");
//            layPickupLocation.setVisibility(View.INVISIBLE);
//            TextView tvPassengerContact = layPassengerContact.findViewById(R.id.tv_phone_no);
//            tvPassengerContact.setText(booking.getPassengerContact());
//            layPassengerContact.setVisibility(View.VISIBLE);
//        }
//        TextView tvPassengerName = bookingDialog.findViewById(R.id.tv_passenger_name);
//        tvPassengerName.setText(booking.getPassengerName());
//
//        Button btnAccept, btnReject;
//        btnAccept = bookingDialog.findViewById(R.id.btn_accept);
//        btnAccept.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                acceptBookingTask(context, booking);
//            }
//        });
//        btnReject = bookingDialog.findViewById(R.id.btn_reject);
//        btnReject.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                bookingDialog.dismiss();
//                rejectBookingTask(context, booking);
//            }
//        });
//
//        bookingDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialogInterface) {
//                countDownTimer.cancel();
//            }
//        });
//    }
//
//    private void wakeUpTheScreen(Context context) {
//        //wake the screen
//        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
//        PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
//                PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "com.emts.cabbi:mywakelockTag");
//        wakeLock.acquire(1000);
//
//        //release the wake lock
//        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
//        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
//        keyguardLock.disableKeyguard();
//    }
//
//    private void playNotificationSound(Context context) {
//        try {
//            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            Ringtone r = RingtoneManager.getRingtone(context, notification);
//            r.play();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void vibrate(Context context) {
//        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
//        if (v == null) return;
//        // Vibrate for 500 milliseconds
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
//        } else {
//            //deprecated in API 26
//            v.vibrate(500);
//        }
//    }
//
//    private void acceptBookingTask(final Context context, Booking booking) {
//        final ProgressDialog pDialog = AlertUtils.showProgressDialog(context, "Accepting booking...");
//        VolleyHelper volleyHelper = VolleyHelper.getInstance(context);
//        HashMap<String, String> postParams = volleyHelper.getPostParams();
//        postParams.put("booking_id", booking.getBookingNo());
//
//        volleyHelper.addVolleyRequestListeners(Api.getInstance().acceptBooking, Request.Method.POST,
//                postParams, new VolleyHelper.VolleyHelperInterface() {
//                    @Override
//                    public void onSuccess(String response) {
//                        pDialog.dismiss();
//                        try {
//                            JSONObject responseData = new JSONObject(response);
//                            if (responseData.getBoolean("status")) {
//                                if (countDownTimer != null) {
//                                    countDownTimer.cancel();
//                                }
//                                Intent intent = new Intent(context, MainActivity.class);
//                                intent.putExtra("acceptBooking", true);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
//                                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                context.startActivity(intent);
//                                bookingDialog.dismiss();
//                            } else {
//                                AlertUtils.simpleAlert(context, false, "Booking Unsuccessful !!!",
//                                        responseData.getString("message"), "OK",
//                                        "", new AlertUtils.OnAlertButtonClickListener() {
//                                            @Override
//                                            public void onAlertButtonClick(boolean isPositiveButton) {
//                                                bookingDialog.dismiss();
//                                            }
//                                        });
//                            }
//                        } catch (JSONException e) {
//                            Logger.e("acceptBookingTask json exception ", e.getMessage());
//                        }
//                    }
//
//                    @Override
//                    public void onError(String errorResponse, VolleyError volleyError) {
//                        pDialog.dismiss();
//                        String errorMsg = "Unexpected Error, Please try again with internet access!";
//                        try {
//                            JSONObject errorObj = new JSONObject(errorResponse);
//                            errorMsg = errorObj.getString("message");
//                        } catch (JSONException e) {
//                            Logger.e("acceptBookingTask error ex", e.getMessage() + " ");
//                        }
//                        AlertUtils.simpleAlert(context, "Booking Unsuccessful !!!",
//                                errorMsg, "OK",
//                                "", new AlertUtils.OnAlertButtonClickListener() {
//                                    @Override
//                                    public void onAlertButtonClick(boolean isPositiveButton) {
//                                        bookingDialog.dismiss();
//                                    }
//                                });
//                    }
//                }, "acceptBookingTask");
//
//    }
//
//    private void rejectBookingTask(final Context context, Booking booking) {
//        final ProgressDialog pDialog = AlertUtils.showProgressDialog(context, "Rejecting booking...");
//        VolleyHelper volleyHelper = VolleyHelper.getInstance(context);
//        HashMap<String, String> postParams = volleyHelper.getPostParams();
//        postParams.put("booking_id", booking.getBookingNo());
//
//        volleyHelper.addVolleyRequestListeners(Api.getInstance().rejectBooking, Request.Method.POST,
//                postParams, new VolleyHelper.VolleyHelperInterface() {
//                    @Override
//                    public void onSuccess(String response) {
//                        pDialog.dismiss();
//                        try {
//                            JSONObject responseData = new JSONObject(response);
//                            if (responseData.getBoolean("status")) {
//                                if (countDownTimer != null) {
//                                    countDownTimer.cancel();
//                                }
//                                AlertUtils.simpleAlert(context, false, "Success !!!",
//                                        responseData.getString("message"), "OK",
//                                        "", new AlertUtils.OnAlertButtonClickListener() {
//                                            @Override
//                                            public void onAlertButtonClick(boolean isPositiveButton) {
////                                                    bookingDialog.dismiss();
//                                            }
//                                        });
//                            } else {
//                                AlertUtils.simpleAlert(context, false, "Error !!!",
//                                        responseData.getString("message"), "OK",
//                                        "", new AlertUtils.OnAlertButtonClickListener() {
//                                            @Override
//                                            public void onAlertButtonClick(boolean isPositiveButton) {
////                                                    bookingDialog.dismiss();
//                                            }
//                                        });
//                            }
//                        } catch (JSONException e) {
//                            Logger.e("rejectBookingTask json exception ", e.getMessage());
//                        }
//                    }
//
//                    @Override
//                    public void onError(String errorResponse, VolleyError volleyError) {
//                        pDialog.dismiss();
//                        String errorMsg = "Unexpected Error, Please try again with internet access!";
//                        try {
//                            JSONObject errorObj = new JSONObject(errorResponse);
//                            errorMsg = errorObj.getString("message");
//                        } catch (JSONException e) {
//                            Logger.e("rejectBookingTask error ex", e.getMessage() + " ");
//                        }
//                        AlertUtils.simpleAlert(context, "Error !!!",
//                                errorMsg, "OK",
//                                "", new AlertUtils.OnAlertButtonClickListener() {
//                                    @Override
//                                    public void onAlertButtonClick(boolean isPositiveButton) {
////                                            bookingDialog.dismiss();
//                                    }
//                                });
//                    }
//                }, "rejectBookingTask");
//
//    }
//
//    private void startTimer(int time) {
//        long totalTimeCountInMilliseconds = time * 1000;
//        progressBar.setProgress(100);
//        progressBar.setMax(time * 1000);
//
//        countDownTimer = new CountDownTimer(totalTimeCountInMilliseconds, 1) {
//            @SuppressLint({"DefaultLocale", "SetTextI18n"})
//            @Override
//            public void onTick(long leftTimeInMilliseconds) {
//                long seconds = leftTimeInMilliseconds / 1000;
//                progressBar.setProgress((int) (leftTimeInMilliseconds));
//                textViewShowTime.setText(String.format("%02d", seconds / 60) + "." + String.format("%02d", seconds % 60));
//            }
//
//            @Override
//            public void onFinish() {
//                textViewShowTime.setText("00:00");
//                textViewShowTime.setVisibility(View.VISIBLE);
//                progressBar.setVisibility(View.GONE);
//                bookingDialog.dismiss();
//            }
//        }.start();
//    }
//
//    public void recycle() {
//        //disconnect the socket
//        if (socket != null) {
//            socket.off();
//            socket.disconnect();
//        }
//        socket = null;
//        //alert recycler
//        if (bookingDialog != null) {
//            if (bookingDialog.isShowing()) {
//                bookingDialog.dismiss();
//            }
//            bookingDialog = null;
//        }
//        //don't leak the context
//        progressBar = null;
//        countDownTimer = null;
//    }
//}
