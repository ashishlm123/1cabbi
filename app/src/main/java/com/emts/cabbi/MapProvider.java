package com.emts.cabbi;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;

import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Logger;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class MapProvider {
    Context context;
    GoogleMap map;
    private Location pickUpLoc, dropOffLoc;

    public MapProvider with(Context context) {
        this.context = context;
        return MapProvider.this;
    }

    public MapProvider onMap(GoogleMap map) {
        this.map = map;
        return MapProvider.this;
    }

    public MapProvider setStartLocation(Location location, String label, int markerIcon) {
        if (map == null) throw new RuntimeException("Map cannot be null : setStartLocation()");

        pickUpLoc = location;
        addMarker(pickUpLoc, "Pick up", label, markerIcon);
        return MapProvider.this;
    }

    public MapProvider setEndLocation(Location location, String label, int markerIcon) {
        if (map == null) throw new RuntimeException("Map cannot be null : setEndLocation()");

        dropOffLoc = location;
        addMarker(dropOffLoc, "Drop off", label, markerIcon);
        return MapProvider.this;
    }

    public MapProvider animateTo(Location location, int zoomLevel) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));

        return MapProvider.this;
    }

    //    this is without animation
    public MapProvider moveTo(Location location, int zoomLevel) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));

        return MapProvider.this;
    }

    public MapProvider showRout(Location from, Location to) {
        LatLng source = new LatLng(from.getLatitude(), from.getLongitude());
        LatLng dest = new LatLng(to.getLatitude(), to.getLongitude());
        new GoogleRouteHelper(context, map, source, dest);
        return MapProvider.this;
    }

    public Marker addMarker(Location location, String title, String snippet, int icon) {
        try {
            if (location == null) {
                return null;
            }
            //pickup marker
            LatLng riderLoc = new LatLng(location.getLatitude(), location.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(riderLoc);
            if (!TextUtils.isEmpty(title)) {
                markerOptions.title(title);
            }
            if (!TextUtils.isEmpty(snippet)) {
                markerOptions.snippet(snippet);
            }
            if (icon > 0) {
                markerOptions.icon(BitmapDescriptorFactory.fromResource(icon));
            }
            Marker marker = map.addMarker(markerOptions);
            if (!TextUtils.isEmpty(snippet)) {
                marker.showInfoWindow();
            }
            return marker;
        } catch (Exception e) {
            Logger.e("showRiderLocation ex", e.getMessage() + "");
        }
        return null;
    }

    public static void showGPSEnableAlert(Context context, AlertUtils.OnAlertButtonClickListener listener) {
        AlertUtils.simpleAlert(context, false, "Enable GPS",
                context.getString(R.string.gps_before_procced), "OK", "", listener);
    }

    AlertUtils.OnAlertButtonClickListener listener;

    public MapProvider showCurrentLocation(final Context context, final boolean animateTo) {
        if (SmartLocation.with(context).location().state().isGpsAvailable()) {
//                    LocationParams locConfig = new LocationParams.Builder()
//                            .setAccuracy(LocationAccuracy.HIGH)
//                            .setDistance(15)
//                            .setInterval(30000)
//                            .build();
            Logger.e("listen for location ", "start once location listener");
            SmartLocation.with(context).location().oneFix()
//                            .config(locConfig)
                    .start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(Location location) {
                            Logger.e("onLocationUpdated onFix", location.toString());
                            addMarker(location, "", "", 0);

                            if (animateTo) {
                                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                                map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));
                            }
                        }
                    });
        } else {
            listener = new AlertUtils.OnAlertButtonClickListener() {
                @Override
                public void onAlertButtonClick(boolean isPositiveButton) {
                    if (!LocationHelper.checkIfGPSEnable(context)) {
                        MapProvider.showGPSEnableAlert(context, listener);
                    }
                }
            };
            MapProvider.showGPSEnableAlert(context, null);
        }
        return MapProvider.this;
    }

    public int getSmartZoom(double distanceInKm) {
        if (distanceInKm < 1) {
            return 16;
        } else if (distanceInKm < 3) {
            return 15;
        } else if (distanceInKm < 5) {
            return 14;
        } else if (distanceInKm < 7) {
            return 13;
        } else if (distanceInKm < 9) {
            return 12;
        } else {
            return 11;
        }
    }
}