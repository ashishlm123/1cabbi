package com.emts.cabbi.model;

import java.io.Serializable;

/**
 * Created by User on 2017-07-17.
 */
public class Booking implements Serializable {
    public static final String TRIP_STATUS_WAITING = "0";
    public static final String TRIP_STATUS_SCHEDULED = "1";
    public static final String TRIP_STATUS_IN_PROGRESS = "2";
    public static final String TRIP_STATUS_COMPLETED = "3";

    public static final String BOOKING_TYPE_INSTANT = "0";
    public static final String BOOKING_TYPE_PRE = "1";

    private String bookingNo, date, status, pickUpLocation, dropOffLocation, distance, tripTime, tripFare,
            cabbiServiceFee, operatorServiceFee, cooperateServiceFee, otherCharges,
            passengerName, passengerContact, bookingType, note;
    private double pickUpLat, pickUpLong, dropOffLat, dropOffLong;
    int timeToLive;

    public String getBookingNo() {
        return bookingNo;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPickUpLocation() {
        return pickUpLocation;
    }

    public void setPickUpLocation(String pickUpLocation) {
        this.pickUpLocation = pickUpLocation;
    }

    public String getDropOffLocation() {
        return dropOffLocation;
    }

    public void setDropOffLocation(String dropOffLocation) {
        this.dropOffLocation = dropOffLocation;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTripTime() {
        return tripTime;
    }

    public void setTripTime(String tripTime) {
        this.tripTime = tripTime;
    }

    public String getTripFare() {
        return tripFare;
    }

    public void setTripFare(String tripFare) {
        this.tripFare = tripFare;
    }

    public String getCabbiServiceFee() {
        return cabbiServiceFee;
    }

    public void setCabbiServiceFee(String cabbiServiceFee) {
        this.cabbiServiceFee = cabbiServiceFee;
    }

    public String getOperatorServiceFee() {
        return operatorServiceFee;
    }

    public void setOperatorServiceFee(String operatorServiceFee) {
        this.operatorServiceFee = operatorServiceFee;
    }

    public String getOtherCharges() {
        return otherCharges;
    }

    public void setOtherCharges(String otherCharges) {
        this.otherCharges = otherCharges;
    }

    public int getTimeToLive() {
        return timeToLive;
    }

    public void setTimeToLive(int timeToLive) {
        this.timeToLive = timeToLive;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getPassengerContact() {
        return passengerContact;
    }

    public void setPassengerContact(String passengerContact) {
        this.passengerContact = passengerContact;
    }

    public double getPickUpLat() {
        return pickUpLat;
    }

    public void setPickUpLat(double pickUpLat) {
        this.pickUpLat = pickUpLat;
    }

    public double getPickUpLong() {
        return pickUpLong;
    }

    public void setPickUpLong(double pickUpLong) {
        this.pickUpLong = pickUpLong;
    }

    public double getDropOffLat() {
        return dropOffLat;
    }

    public void setDropOffLat(double dropOffLat) {
        this.dropOffLat = dropOffLat;
    }

    public double getDropOffLong() {
        return dropOffLong;
    }

    public void setDropOffLong(double dropOffLong) {
        this.dropOffLong = dropOffLong;
    }

    public String getCooperateServiceFee() {
        return cooperateServiceFee;
    }

    public void setCooperateServiceFee(String cooperateServiceFee) {
        this.cooperateServiceFee = cooperateServiceFee;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
