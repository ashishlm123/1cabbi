package com.emts.cabbi.model;

import java.io.Serializable;

public class CashOutHistoryModel implements Serializable {
    private String cashOutAmt;
    private String cashOutDate;
    private String cashOutStatus;
    private String cashOutRequestId;

    public String getCashOutRequestId() {
        return cashOutRequestId;
    }

    public void setCashOutRequestId(String cashOutRequestId) {
        this.cashOutRequestId = cashOutRequestId;
    }

    public String getCashOutStatus() {
        return cashOutStatus;
    }

    public void setCashOutStatus(String cashOutStatus) {
        this.cashOutStatus = cashOutStatus;
    }


    public String getCashOutAmt() {
        return cashOutAmt;
    }

    public void setCashOutAmt(String cashOutAmt) {
        this.cashOutAmt = cashOutAmt;
    }

    public String getCashOutDate() {
        return cashOutDate;
    }

    public void setCashOutDate(String cashOutDate) {
        this.cashOutDate = cashOutDate;
    }

}
