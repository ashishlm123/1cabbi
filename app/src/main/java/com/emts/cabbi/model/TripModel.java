package com.emts.cabbi.model;

import java.io.Serializable;

public class TripModel implements Serializable {
    private String tripId;
    private String tripDate;
    private String totalIncome;
    private String tripStatus;
    private String totalTrips;
    private String tripBookingNo;

    public static String TRIP_PENDING = "0";
    public static String TRIP_SCHEDULED = "1";
    public static String TRIP_IN_PROGRESS = "2";
    public static String TRIP_COMPLETED = "3";
    public static String TRIP_CANCELLED = "4";

    public static String PENDING_TEXT = "Pending";
    public static String SCHEDULED_TEXT = "Scheduled";
    public static String IN_PROGRESS_TEXT = "In Progress";
    public static String COMPLETED_TEXT = "Completed";
    public static String CANCELLED_TEXT = "Cancelled";

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(String totalIncome) {
        this.totalIncome = totalIncome;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getTotalTrips() {
        return totalTrips;
    }

    public void setTotalTrips(String totalTrips) {
        this.totalTrips = totalTrips;
    }

    public String getTripBookingNo() {
        return tripBookingNo;
    }

    public void setTripBookingNo(String tripBookingNo) {
        this.tripBookingNo = tripBookingNo;
    }

}
