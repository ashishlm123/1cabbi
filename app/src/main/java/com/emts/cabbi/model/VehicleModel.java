package com.emts.cabbi.model;

import java.io.Serializable;

public class VehicleModel implements Serializable {
    private String id;
    private String modelName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    @Override
    public String toString() {
        return this.modelName;
    }
}
