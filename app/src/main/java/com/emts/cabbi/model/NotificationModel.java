package com.emts.cabbi.model;

import java.io.Serializable;

public class NotificationModel implements Serializable {
    public static final String NOT_BOOKING = "booking_alert";
    public static final String NOT_TYPE_UPDATE_APPROVE = "profile_update_approve";
    public static final String NOT_TYPE_UPDATE_REJECT = "profile_update_disapprove";
    public static final String NOT_TYPE_TRIP_COMPLETE = "trip_complete";
    public static final String NOT_TYPE_TRIP_CANCEL = "trip_cancelled_by_corp";

    public static final String NOT_CASHOUT_ACCEPTED = "cashout_request_approved";
    public static final String NOT_CASHOUT_REJECTED = "cashout_request_rejected";

    private String notificationId;
    private String notificationDate;
    private boolean isSeen;
    private String notificationBody;
    private String notType;

    public String getNotificationBody() {
        return notificationBody;
    }

    public void setNotificationBody(String notificationBody) {
        this.notificationBody = notificationBody;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(String notificationId) {
        this.notificationId = notificationId;
    }

    public boolean isSeen() {
        return isSeen;
    }

    public void setSeen(boolean seen) {
        isSeen = seen;
    }

    public String getNotType() {
        return notType;
    }

    public void setNotType(String notType) {
        this.notType = notType;
    }
}
