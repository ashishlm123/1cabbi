package com.emts.cabbi.helper;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import java.util.Map;
import java.util.Set;

/**
 * Created by User on 2016-03-04.
 */
public class PreferenceHelper implements SharedPreferences {
    private static final String APP_SHARED_PREFS = "1_cabbi_prefs";
    public static final String FCM_TOKEN_UPDATE = "fcm_token_update";
    public static final String IS_LOGIN = "is_login";
    public static final String TOKEN = "app_user_token";

    public static final String USER_NAME = "app_user_name";
    public static final String USER_IMG = "app_user_img";
    public static final String USER_ID = "app_user_id";
    public static final String USER_EMAIL = "app_user_email";
    public static final String USER_FULL_NAME = "app_user_full_name";
    public static final String USER_MOBILE_NO = "app_user_mobile_no";
    public static final String USER_VEHICLE_PLATE_NO = "vehicle_plate_no";
    public static final String USER_TOKEN = "user_token";
    public static final String DRIVER_STATUS = "driver_status";
    public static final String DRIVER_CATEGORY = "driver_cat";

    public static final String DRIVER_ONLINE = "1";
    public static final String DRIVER_OFFLINE = "0";
    public static final String DRIVER_BREAK = "2";

    public static final String FCM_TOKEN_UPDATED = "is_fcm_updated";

    private SharedPreferences prefs;
    private static PreferenceHelper helper;

    public static PreferenceHelper getInstance(Context context) {
        if (helper != null) {
            return helper;
        } else {
            helper = new PreferenceHelper(context);
            return helper;
        }

    }

    private PreferenceHelper(Context context) {
        prefs = context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
    }

    public SharedPreferences getPrefs() {
        return prefs;
    }

    @Override
    public Map<String, ?> getAll() {
        return prefs.getAll();
    }

    @Override
    public String getString(String s, String s2) {
        return prefs.getString(s, s2);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public Set<String> getStringSet(String s, Set<String> strings) {
        return prefs.getStringSet(s, strings);
    }

    @Override
    public int getInt(String s, int i) {
        return prefs.getInt(s, i);
    }

    @Override
    public long getLong(String s, long l) {
        return prefs.getLong(s, l);
    }

    @Override
    public float getFloat(String s, float v) {
        return prefs.getFloat(s, v);
    }

    @Override
    public boolean getBoolean(String s, boolean b) {
        return prefs.getBoolean(s, b);
    }

    @Override
    public boolean contains(String s) {
        return prefs.contains(s);
    }

    @Override
    public Editor edit() {
        return prefs.edit();
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {

    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {

    }

    public boolean isLogin() {
        return prefs.getBoolean(IS_LOGIN, false);
    }

    public String getUserId() {
        return prefs.getString(USER_ID, "-1");
    }

    public String getToken() {
        return prefs.getString(TOKEN, "");
    }

    public String getAppUserName() {
        return prefs.getString(USER_NAME, "");
    }

    public String getAppUserFullName() {
        return prefs.getString(USER_FULL_NAME, "");
    }

    public String getAppUserId() {
        return prefs.getString(USER_ID, "");
    }

    public String getAppUserImg() {
        return prefs.getString(USER_IMG, "");
    }

    public String getAppUserMobileNo() {
        return prefs.getString(USER_MOBILE_NO, "");
    }

    public String getUserVehiclePlateNo() {
        return prefs.getString(USER_VEHICLE_PLATE_NO, "");
    }

    public String getUserToken() {
        return prefs.getString(USER_TOKEN, "");
    }

    public String getDriverStatus() {
        return prefs.getString(DRIVER_STATUS, "");
    }

    public String getAboutUs() {
        return prefs.getString("aboutUs", "");
    }

    public String getContactNo() {
        return prefs.getString("contactUsNum", "");
    }
}
