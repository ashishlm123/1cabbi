package com.emts.cabbi.helper;

/**
 * Created by theone on 10/18/2016.
 */

public class Api {
    private static Api api;

    final String apiKey = "sdkfhiuews354983544";

    public static Api getInstance() {
        if (api != null) {
            return api;
        } else {
            api = new Api();
            return api;
        }
    }

    private String getHost() {
//        return "http://www.1cabbi.site";
            return "http://13.229.126.174"; //testing server
    }

    private String getApi() {
//        return getHost() + "/app/"; //server api loc
           return getHost() + "/onecabbi_staging/public_html/app/"; //testing server api loc
    }

//    public final String socketUrl = "http://13.229.126.174:3030"; //server socket
      public final String socketUrl = "http://13.229.126.174:5555";   // testing server socket

    public final String validateBookingApi = getApi() + "booking/validate_booking_alert";
    public final String loginApi = getApi() + "users/login";
    public final String forgotPasswordApi = getApi() + "users/forget_password";
    public final String changePasswordApi = getApi() + "mycontroller/change_password";
    public final String viewProfileApi = getApi() + "mycontroller/view_profile";
    public final String editProfileApi = getApi() + "mycontroller/edit_profile";
    public final String changeDriverStatusApi = getApi() + "mycontroller/change_status";
    public final String logoutApi = getApi() + "mycontroller/logout";
    public final String faqApi = getApi() + "mycontroller/get_faq";
    public final String termsAndConditionsApi = getApi() + "mycontroller/get_terms_condition";
    public final String privacyPolicyApi = getApi() + "mycontroller/get_content/privacy-and-policy";
    public final String clientTermsApi = getApi() + "mycontroller/get_content/client-terms-and-conditions";
    public final String emergencyHotLineApi = getApi() + "mycontroller/get_emergency_hotlines";
    public final String helpCenterApi = getApi() + "mycontroller/send_help_notification";
    public final String getUserWalletBalance = getApi() + "mycontroller/get_earnings";
    public final String requestCashOut = getApi() + "mycontroller/request_cashout";
    public final String cashOutHistory = getApi() + "mycontroller/cashout_history";
    public final String tripHistoryApi = getApi() + "booking/trips_datewise";
    public final String eachTripListsApi = getApi() + "booking/trips_per_day";
    public final String tripDetailsApi = getApi() + "booking/trips_history_detail";
    public final String updatePushToken = getApi() + "mycontroller/update_device_id";
    public final String acceptBooking = getApi() + "booking/accept_booking";
    public final String rejectBooking = getApi() + "booking/reject_booking";
    public final String cancelBooking = getApi() + "booking/cancel_trip";
    public final String getMyBookingsApi = getApi() + "mycontroller/my_trip";

    public final String arriveForPickUp = getApi() + "booking/arrive_for_pickup";
    public final String startEndTripApi = getApi() + "/booking/start_end_trip";

    public final String locationUpdateApi = getApi() + "mycontroller/update_driver_location";
    public final String versionCheckApi = getApi() + "check_version";
    public final String aboutUsApi = getApi() + "mycontroller/about_us";

    public final String notificationApi = getApi() + "mycontroller/my_notifications";
    public final String notReadUnReadApi = getApi() + "mycontroller/read_unread_notification";


}