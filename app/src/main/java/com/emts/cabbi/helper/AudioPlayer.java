package com.emts.cabbi.helper;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;

public class AudioPlayer {

    private MediaPlayer mMediaPlayer;
    private Handler handler = new Handler();

    public void stop() {
        handler.removeCallbacks(stopPlayerTask);
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    public void play(Context c, int rid, int stopTimeMills) {
        stop();

        mMediaPlayer = MediaPlayer.create(c, rid);
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                stop();
            }
        });

        mMediaPlayer.start();

        if (stopTimeMills > 0) {
            handler.postDelayed(stopPlayerTask, 9999);
        }
    }

    private Runnable stopPlayerTask = new Runnable() {
        @Override
        public void run() {
            stop();
        }
    };

}