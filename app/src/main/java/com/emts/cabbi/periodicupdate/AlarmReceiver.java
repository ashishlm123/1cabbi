package com.emts.cabbi.periodicupdate;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.emts.cabbi.helper.Logger;


public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Logger.e("Alarm receiver", "received -------");
        LocUpdateScheduler.scheduleJobOnceImmediate(context);

        LocUpdateScheduler.schedule(context, false);
    }
}
