package com.emts.cabbi.periodicupdate;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.PreferenceHelper;

public class LocUpdateScheduler {
    static final int MYJOBID = 232;
    private static final long INSPECTION_SESSION_SEND_DEADLINE = 0;
    private static final long PERIODIC_TIME = 1 * 60 * 1000L;

//    public static void schedule(Context context) {
//        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
//        ComponentName jobService = new ComponentName(context.getPackageName(), MyJobService.class.getName());
////        JobInfo jobInfo =
////                new JobInfo.Builder(MYJOBID, jobService).setPeriodic(PERIODIC_TIME)
//////                new JobInfo.Builder(MYJOBID, jobService).setPeriodic(PERIODIC_TIME)
//////                        .setExtras(bundle)
////                        .build();
//        JobInfo jobInfo = new JobInfo.Builder(
//                MYJOBID, jobService)
//                .setBackoffCriteria(TimeUnit.SECONDS.toMillis(30), JobInfo.BACKOFF_POLICY_EXPONENTIAL)
//                .setPersisted(true)
//                .setMinimumLatency(0)
//                .setOverrideDeadline(INSPECTION_SESSION_SEND_DEADLINE)
//                .setPeriodic(PERIODIC_TIME)
//                .build();
//        if (jobScheduler != null) {
//            int jobId = jobScheduler.schedule(jobInfo);
//
//            if (jobId > 0) {
//                Logger.e("status", "running");
//            } else {
//                Logger.e("status", "failed");
//            }
//        }
//        Logger.e("LocUpdateScheduler schedule", "Job Info: " + jobInfo.toString());
//    }

    static void scheduleJobOnceImmediate(Context context) {
        Logger.e("JobScheduler schedule Once", " job scheduled for once right now");
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(new JobInfo.Builder(MYJOBID,
                new ComponentName(context, MyJobService.class))
                .setMinimumLatency(1)
                .setOverrideDeadline(1)
                .build());
    }

    public static void schedule(Context context, boolean isInstant) {
        Intent downloader = new Intent(context, AlarmReceiver.class);
        PendingIntent recurringDownload = PendingIntent.getBroadcast(context, 1, downloader,
                PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarms = (AlarmManager) context.getSystemService(
                Context.ALARM_SERVICE);

        alarms.cancel(recurringDownload);

        if (!PreferenceHelper.getInstance(context).isLogin()) {
            return;
        }

        long time = System.currentTimeMillis();
        if (!isInstant) {
            time = time + PERIODIC_TIME;
        }

        if (Build.VERSION.SDK_INT > 22) {
            alarms.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                    time, recurringDownload);
        } else {
            alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, time,
                    PERIODIC_TIME, recurringDownload);
        }

        Logger.e("LocUpdate Alarm", "########### ALARM SET #################");
    }
}
