package com.emts.cabbi.periodicupdate;

import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.Context;
import android.location.Location;
import android.provider.Settings;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.PreferenceHelper;
import com.emts.cabbi.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class MyJobService extends JobService {
    static OnLocationUpdatedListener locationUpdatedListener;

    @Override
    public boolean onStartJob(final JobParameters jobParameters) {
        Logger.e("MyJobService onStartJob", "Time: " + System.currentTimeMillis()
                + "\nParams : " + jobParameters.toString());
        if (!PreferenceHelper.getInstance(this).isLogin()) {
            JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
            jobScheduler.cancel(LocUpdateScheduler.MYJOBID);
            return false;
        }
        int locationMode = 0;
        try {
            locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
        }
        //check location mode
        if (locationMode != Settings.Secure.LOCATION_MODE_OFF && locationMode == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY) {
            if (locationUpdatedListener == null) {
                locationUpdatedListener = new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        if (NetworkUtils.isInNetwork(getApplicationContext())) {

                            //check if last updated location is more than 1 km
//                            Location lastLoc = SmartLocation.with(MyJobService.this).location().getLastLocation();
                            Location lastUpdatedLocation = new Location("");
                            PreferenceHelper preferenceHelper = PreferenceHelper.getInstance(MyJobService.this);
                            lastUpdatedLocation.setLatitude(Double.longBitsToDouble(preferenceHelper.getLong("last_lat", 0)));
                            lastUpdatedLocation.setLongitude(Double.longBitsToDouble(preferenceHelper.getLong("last_lng", 0)));
                            if (lastUpdatedLocation.getLatitude() != 0 && lastUpdatedLocation.getLongitude() != 0) {
                                if (lastUpdatedLocation.distanceTo(location) < 1000) {
                                    jobFinished(jobParameters, false);
                                    return;
                                }
                            }

                            updateLocation(MyJobService.this, location.getLatitude(), location.getLongitude());
                            jobFinished(jobParameters, false);
                        }
                    }
                };
            }
            SmartLocation.with(MyJobService.this).location().oneFix().start(locationUpdatedListener);
        } else {
            // location enabled but not at high accuracy so use other location available
            Location location = SmartLocation.with(MyJobService.this).location().getLastLocation();
            if (location != null) {
                Logger.e("Location Update", "Last Known Location + " + location);
                updateLocation(MyJobService.this, location.getLatitude(), location.getLongitude());
                jobFinished(jobParameters, false);
            }
        }

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        Logger.e("MyJobService onStopJob", "Params : " + jobParameters.toString());
        SmartLocation.with(MyJobService.this).location().oneFix().stop();
        return false;
    }

    public static void updateLocation(final Context context, final double latitude, final double longitude) {
        VolleyHelper volleyHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("latitude", String.valueOf(latitude));
        postParams.put("longitude", String.valueOf(longitude));

        volleyHelper.addVolleyRequestListeners(Api.getInstance().locationUpdateApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject responseData = new JSONObject(response);
                            if (responseData.getBoolean("status")) {
                                PreferenceHelper.getInstance(context).edit().putLong("last_lat", Double.doubleToRawLongBits(latitude)).apply();
                                PreferenceHelper.getInstance(context).edit().putLong("last_lng", Double.doubleToRawLongBits(longitude)).apply();
                            }
                        } catch (JSONException e) {
                            Logger.e("updateLocation json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                    }
                }, "updateLocation");

    }
}
