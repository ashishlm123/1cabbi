package com.emts.cabbi.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.text.Html;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.BookingParser;
import com.emts.cabbi.R;
import com.emts.cabbi.activity.DriverProfileActivity;
import com.emts.cabbi.activity.HelpCenterActivity;
import com.emts.cabbi.activity.MainActivity;
import com.emts.cabbi.activity.SplashActivity;
import com.emts.cabbi.activity.TripDayActivity;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.PreferenceHelper;
import com.emts.cabbi.helper.VolleyHelper;
import com.emts.cabbi.model.Booking;
import com.emts.cabbi.model.NotificationModel;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MyFireBaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    public static final String CHANNEL_ID = "com.emts.1cabbi.notify_001";

    /*
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */

    @Override
    public void onNewToken(String token) {
        Logger.e(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        registerDeviceTokenToServer(token, this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
       /* There are two types of messages: data messages and notification messages. Data messages are handled
         here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
         traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
         is in the foreground. When the app is in the background an automatically generated notification is displayed.
         When the user taps on the notification they are returned to the app. Messages containing both notification
         and data payloads are treated as notification messages. The FireBase console always sends notification
         messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options */

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Logger.e(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Logger.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            Logger.e(TAG, "Message Notification data: " + remoteMessage.getNotification().getSound());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Logger.e(TAG, "Message data payload: " + remoteMessage.getData());

            handleDataNotification(remoteMessage);
        }
    }

    private void handleDataNotification(RemoteMessage remoteMessage) {
        Map<String, String> dataMap = remoteMessage.getData();
        String notType = dataMap.get("note_type");
        Logger.e(TAG + " notiType", notType + " ******");
        Intent intent = new Intent(MyFireBaseMessagingService.this, SplashActivity.class);
        boolean isBookingNotification = false;
        if (notType == null) return;
        switch (notType) {
            case NotificationModel.NOT_BOOKING:
                if (!BookingBaseActivity.isActivityOnForGround) {
                    //send notification
                    Booking booking = BookingParser.bookingFromPush(dataMap);

                    intent = new Intent(getApplicationContext(), MainActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK |
//                            Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("booking", booking);
                    Logger.e("MyFirebaseActivity Booking Alert",
                            "Activity is not in forGround so generate a notification");
                    isBookingNotification = true;
                } else {
                    //if send broadcast so that baseactivity will show the alert
                    Booking booking = BookingParser.bookingFromPush(dataMap);
                    Intent intent1 = new Intent(BookingBaseActivity.INTENT_BOOKING_BROADCAST);
                    intent1.putExtra("booking", booking);
                    sendBroadcast(intent1);
                    return;
                }
                break;
            case NotificationModel.NOT_TYPE_TRIP_COMPLETE:
                //goto trip of the day
                intent = new Intent(getApplicationContext(), TripDayActivity.class);
                intent.putExtra("tripDate", dataMap.get("date"));
                break;
            case NotificationModel.NOT_TYPE_UPDATE_APPROVE:
                //goto view profile
                intent = new Intent(getApplicationContext(), DriverProfileActivity.class);
                break;
            case NotificationModel.NOT_TYPE_UPDATE_REJECT:
                //goto help center
                intent = new Intent(getApplicationContext(), HelpCenterActivity.class);
                break;
            case NotificationModel.NOT_TYPE_TRIP_CANCEL:
//            intent = new Intent(getApplicationContext(), MainActivity.class);
//            intent.putExtra("acceptBooking", true); //NOT really accept booking but
//            // that extra will open mybookings fragment so reuse

                if (BookingBaseActivity.isActivityOnForGround) {
                    Intent cancelIntent = new Intent(BookingBaseActivity.INTENT_BOOKING_CANCEL);
                    cancelIntent.putExtra("message", dataMap.get("body"));
                    sendBroadcast(cancelIntent);
                    return;
                }
                break;
        }

        intent.putExtra("fromPush", true);

        String title = dataMap.get("title");
        String body = dataMap.get("body");
        Logger.e("notification title : body", title + " :**********: " + body);

        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.cabbi_8x);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 321,
                intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(Html.fromHtml(body));
        bigText.setBigContentTitle(Html.fromHtml(title));
//        if (isBookingNotification)
//            bigText.setSummaryText("The booking is valid for " + ((Booking) intent.getSerializableExtra("booking")).getTimeToLive() +
//                    " seconds only");

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.noti_icon);
        mBuilder.setContentTitle(Html.fromHtml(title));
        mBuilder.setContentText(Html.fromHtml(body));
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText);
//        mBuilder.setSound(defaultSoundUri);
        mBuilder.setSound(sound);
        mBuilder.setAutoCancel(true);

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "1cabbi Notification",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);

            //sound
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            channel.setSound(sound, attributes);
        }

        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        mNotificationManager.notify(m, mBuilder.build());
        Logger.e("firebase notificatio", "notification fired ID:" + m + " TITLE: " + title
                + " BODY:" + body + " ICON:");

        if (isBookingNotification) {
            clearNotification(mNotificationManager, m, Integer.parseInt(dataMap.get("max_time_to_driver_response")));
        }
    }

    private void clearNotification(final NotificationManager notificationManager,
                                   final int notId, long delayInMilliseconds) {
        Handler h = new Handler(Looper.getMainLooper());
//        long delayInMilliseconds = 5000;
        h.postDelayed(new Runnable() {
            public void run() {
                notificationManager.cancel(notId);
            }
        }, delayInMilliseconds * 1000);
    }

    public static void registerDeviceTokenToServer(String deviceToken, Context context) {
        if (NetworkUtils.isInNetwork(context)) {
            if (PreferenceHelper.getInstance(context).isLogin()) {
                updateDeviceToken(context, deviceToken);
            } else {
                PreferenceHelper.getInstance(context).edit().putBoolean(PreferenceHelper.FCM_TOKEN_UPDATED, false).apply();
            }
        } else {
            PreferenceHelper.getInstance(context).edit().putBoolean(PreferenceHelper.FCM_TOKEN_UPDATED, false).apply();
        }
    }

    private static void updateDeviceToken(Context context, String token) {
        final PreferenceHelper prefsHelper = PreferenceHelper.getInstance(context);
        VolleyHelper volleyHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("device_id", token);
        Logger.e("device_update", postParams.get("tokenid"));

        volleyHelper.addVolleyRequestListeners(Api.getInstance().updatePushToken, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject responseData = new JSONObject(response);
                            if (responseData.getBoolean("status")) {
                                prefsHelper.edit().putBoolean(PreferenceHelper.FCM_TOKEN_UPDATED, true).apply();
                            } else {
                                prefsHelper.edit().putBoolean(PreferenceHelper.FCM_TOKEN_UPDATED, false).apply();
                            }
                        } catch (JSONException e) {
                            Logger.e("updateDeviceToken json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        prefsHelper.edit().putBoolean(PreferenceHelper.FCM_TOKEN_UPDATED, false).apply();
                    }
                }, "updateDeviceToken");

    }
}
