package com.emts.cabbi.fragment;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.emts.cabbi.MapProvider;
import com.emts.cabbi.R;
import com.emts.cabbi.activity.CancelBookingActivity;
import com.emts.cabbi.activity.MainActivity;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.model.Booking;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BookingFrag extends Fragment {
    GoogleMap map;
    Booking booking;
    boolean isInstantBooking;
    View holderBtn;
    int REQUEST_CANCEL_BOOKING = 9877;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        booking = (Booking) getArguments().getSerializable("booking");
        isInstantBooking = getArguments().getBoolean("instantBooking");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_booking, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        View rootView = view.findViewById(R.id.bookingRoot);
        if (isInstantBooking) {
            rootView.setBackgroundResource(R.color.appGreen);
        } else {
            rootView.setBackgroundResource(R.color.appBlue);
        }
        TextView tvNoTrip = view.findViewById(R.id.tvNoTrip);
        View holderBooking = view.findViewById(R.id.holderBooking);

        //if booking is null take it as no booking at the moment
        if (booking == null) {
            tvNoTrip.setVisibility(View.VISIBLE);
            holderBooking.setVisibility(View.GONE);
            return;
        } else {
            tvNoTrip.setVisibility(View.GONE);
            holderBooking.setVisibility(View.VISIBLE);
        }

        holderBtn = view.findViewById(R.id.holderBtn);
        Button btnCancelBooking = view.findViewById(R.id.btnCancelTrip);
        Button btnBookingStats = view.findViewById(R.id.btnTripStatus);
        if (booking.getStatus().equals(Booking.TRIP_STATUS_SCHEDULED)) {
            //GOTO pickup fragment
            btnBookingStats.setText("Proceed");
            btnCancelBooking.setVisibility(View.VISIBLE);
            btnCancelBooking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), CancelBookingActivity.class);
                    intent.putExtra("booking", booking);
                    startActivityForResult(intent, REQUEST_CANCEL_BOOKING);
                }
            });
            if (!isInstantBooking) {
                //for pre booking enable pick up button on the day of pick up only
                Calendar todayCal = Calendar.getInstance();
                Calendar preBookingCal = Calendar.getInstance();
                SimpleDateFormat resFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date date = resFormatter.parse(booking.getDate());
                    preBookingCal.setTime(date);

                    if (todayCal.get(Calendar.YEAR) == preBookingCal.get(Calendar.YEAR) &&
                            todayCal.get(Calendar.MONTH) == preBookingCal.get(Calendar.MONTH) &&
                            todayCal.get(Calendar.DAY_OF_MONTH) == preBookingCal.get(Calendar.DAY_OF_MONTH)) {
                        btnBookingStats.setEnabled(true);
                        btnBookingStats.setBackgroundResource(R.color.colorAccent);
                    } else {
                        btnBookingStats.setEnabled(false);
                        btnBookingStats.setBackgroundResource(R.color.btnDisable);
                    }
                } catch (ParseException e) {
                    Logger.e("Booking frag", "pre booking date case ex :: " + e.getMessage());
                }

                TextView tvBookingDate = view.findViewById(R.id.bookingDate);
                tvBookingDate.setText("Booking Date : " + booking.getDate());
                tvBookingDate.setVisibility(View.VISIBLE);
            }
        } else {
            //GOTO dropoff fragment
            btnBookingStats.setText("End Trip");
        }
        btnBookingStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (booking.getStatus().equals(Booking.TRIP_STATUS_SCHEDULED)) {
                    ((MainActivity) getActivity()).goForPickUp(booking);
                } else {
                    ((MainActivity) getActivity()).goForDropOff(booking);
                }
            }
        });

        TextView tvFare = view.findViewById(R.id.tripFare);
        tvFare.setText("Trip Fare: MYR " + booking.getTripFare());
        final TextView bookingNo = view.findViewById(R.id.bookingNo);
        bookingNo.setText("Booking No: " + booking.getBookingNo());
        TextView passengerName = view.findViewById(R.id.passengerName);
        passengerName.setText(" : " + booking.getPassengerName());
        TextView passengerContact = view.findViewById(R.id.passengerContact);
        passengerContact.setText(" : " + booking.getPassengerContact());
        View holderPhone = view.findViewById(R.id.holderPhone);
        holderPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + booking.getPassengerContact()));
                startActivity(intent);
            }
        });

        //note
        View holderNote = view.findViewById(R.id.holderNote);
        TextView tvNote = view.findViewById(R.id.tvNote);
        if (TextUtils.isEmpty(booking.getNote())) {
            holderNote.setVisibility(View.GONE);
        } else {
            tvNote.setText(booking.getNote());
            holderNote.setVisibility(View.VISIBLE);
        }

        SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;

                Location pickUpLoc = new Location("");
                pickUpLoc.setLatitude(booking.getPickUpLat());
                pickUpLoc.setLongitude(booking.getPickUpLong());

                Location dropOffLoc = new Location("");
                dropOffLoc.setLatitude(booking.getDropOffLat());
                dropOffLoc.setLongitude(booking.getDropOffLong());

                MapProvider mapProvider = new MapProvider();
                mapProvider.with(getActivity())
                        .onMap(map)
                        .setStartLocation(pickUpLoc, booking.getPickUpLocation(), R.drawable.icon_pickupmarker)
                        .setEndLocation(dropOffLoc, booking.getDropOffLocation(), R.drawable.icon_pin_marker);

                mapProvider.showRout(pickUpLoc, dropOffLoc);
                mapProvider.animateTo(pickUpLoc, mapProvider.getSmartZoom(Double.parseDouble(booking.getDistance())));
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CANCEL_BOOKING && resultCode == Activity.RESULT_OK) {
//            holderBtn.setVisibility(View.INVISIBLE);

            MainActivity activity = ((MainActivity) getActivity());
            if (activity != null) {
                activity.openHomePage();
            }
        }
    }

}
