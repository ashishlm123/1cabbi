package com.emts.cabbi.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.emts.cabbi.R;
import com.emts.cabbi.activity.ChangePasswordActivity;
import com.emts.cabbi.activity.DriverProfileActivity;

public class ProfileFrag extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RelativeLayout menuProfile, menuChangePassword;
        menuProfile = view.findViewById(R.id.menu_profile);
        TextView tvProfile = menuProfile.findViewById(R.id.tv_menu_title);
        tvProfile.setText("View Profile");
        menuProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), DriverProfileActivity.class);
                startActivity(intent);
            }
        });


        menuChangePassword = view.findViewById(R.id.menu_change_password);
        TextView tvChangePassword = menuChangePassword.findViewById(R.id.tv_menu_title);
        tvChangePassword.setText("Change Password");
        menuChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
                startActivity(intent);
            }
        });


    }
}
