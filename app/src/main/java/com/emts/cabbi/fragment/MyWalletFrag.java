package com.emts.cabbi.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.R;
import com.emts.cabbi.activity.CashOutHistoryActivity;
import com.emts.cabbi.activity.RequestCashOutActivity;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.PreferenceHelper;
import com.emts.cabbi.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MyWalletFrag extends Fragment {
    public static final int REQUEST_CASH_OUT = 4343;
    PreferenceHelper prefsHelper;
    View holderContent;
    ProgressBar progressBar;
    TextView errorText;

    TextView tvTotalEarning;

    String bankInfo = "";
    double balance = 0.0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefsHelper = PreferenceHelper.getInstance(getActivity());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_wallet, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        holderContent = view.findViewById(R.id.holderContent);
        progressBar = view.findViewById(R.id.progress);
        errorText = view.findViewById(R.id.error_text);

        tvTotalEarning = view.findViewById(R.id.tv_total_earning);
        RelativeLayout menuRequestCashOut, menuCashOutHistory;
        menuRequestCashOut = view.findViewById(R.id.menu_request_cash_out);
        menuCashOutHistory = view.findViewById(R.id.menu_cash_out_history);

        TextView tvMenuRequestCashOut, tvMenuCashOutHistory;
        tvMenuRequestCashOut = menuRequestCashOut.findViewById(R.id.tv_menu_title);
        tvMenuRequestCashOut.setText("Request Cash Out");
        tvMenuCashOutHistory = menuCashOutHistory.findViewById(R.id.tv_menu_title);
        tvMenuCashOutHistory.setText("Cash Out History");

        menuRequestCashOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), RequestCashOutActivity.class);
                intent.putExtra("bankInfo", bankInfo);
                startActivityForResult(intent, REQUEST_CASH_OUT);
            }
        });

        menuCashOutHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CashOutHistoryActivity.class);
                startActivity(intent);
            }
        });

        if (prefsHelper.getString(PreferenceHelper.DRIVER_CATEGORY, "taxi").equals("limo")) {
            menuRequestCashOut.setVisibility(View.GONE);
            menuCashOutHistory.setVisibility(View.GONE);
        }

        if (NetworkUtils.isInNetwork(getActivity())) {
            getWalletBalanceTask();
        } else {
            holderContent.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            errorText.setText(R.string.no_internet);
            errorText.setVisibility(View.VISIBLE);
        }
    }

    private void getWalletBalanceTask() {
        holderContent.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().getUserWalletBalance, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                balance = Double.parseDouble(res.getString("total_earning"));
                                tvTotalEarning.setText("MYR " + String.format("%.2f", balance));
                                bankInfo = res.getJSONObject("bank_info").toString();
                                holderContent.setVisibility(View.VISIBLE);
                                errorText.setVisibility(View.GONE);
                            } else {
                                holderContent.setVisibility(View.GONE);
                                errorText.setText(res.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Logger.e("getWalletBalanceTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("getWalletBalanceTask error ex", e.getMessage() + " ");
                        }
                        holderContent.setVisibility(View.GONE);
                        errorText.setText(errorMsg);
                        errorText.setVisibility(View.VISIBLE);
                    }
                }, "getWalletBalanceTask");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            double remainBalance = balance - data.getDoubleExtra("amtWithdraw", 0);
            tvTotalEarning.setText("MYR " + String.format("%.2f", remainBalance));
        }
    }
}
