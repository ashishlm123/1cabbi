package com.emts.cabbi.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.R;
import com.emts.cabbi.activity.EmergencyHotLineActivity;
import com.emts.cabbi.activity.FaqTermsAndConditionsActivity;
import com.emts.cabbi.activity.HelpCenterActivity;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.PreferenceHelper;
import com.emts.cabbi.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class AboutFrag extends Fragment {
    TextView tvAbout;
    ProgressBar progressBar;
    PreferenceHelper preferenceHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvAbout = view.findViewById(R.id.tv_about_us);
        progressBar = view.findViewById(R.id.progress);

        RelativeLayout menuHelpCenter, menuEmergencyHotLine, menuFaq, menuTnc, menuPrivacyPolicy, menuClientTnC;
        menuHelpCenter = view.findViewById(R.id.menu_help_center);
        menuEmergencyHotLine = view.findViewById(R.id.menu_emergency_hotline);
        menuFaq = view.findViewById(R.id.menu_faq);
        menuTnc = view.findViewById(R.id.menu_tnc);
        menuPrivacyPolicy = view.findViewById(R.id.menu_privacy_policy);
        menuClientTnC = view.findViewById(R.id.menu_client_tnc);

        TextView tvMenuHelpCenter, tvEmergencyHotLine, tvMenuFaq, tvMenuTnc;
        tvMenuHelpCenter = menuHelpCenter.findViewById(R.id.tv_menu_title);
        tvMenuHelpCenter.setText("Help Center");
        menuHelpCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), HelpCenterActivity.class);
                startActivity(intent);
            }
        });

        tvEmergencyHotLine = menuEmergencyHotLine.findViewById(R.id.tv_menu_title);
        tvEmergencyHotLine.setText("Emergency Hotline");
        menuEmergencyHotLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EmergencyHotLineActivity.class);
                startActivity(intent);
            }
        });

        tvMenuFaq = menuFaq.findViewById(R.id.tv_menu_title);
        tvMenuFaq.setText("FAQ");
        menuFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FaqTermsAndConditionsActivity.class);
                intent.putExtra("isFaq", true);
                startActivity(intent);
            }
        });

        tvMenuTnc = menuTnc.findViewById(R.id.tv_menu_title);
        tvMenuTnc.setText("Fleet Terms and Conditions");
        menuTnc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FaqTermsAndConditionsActivity.class);
                startActivity(intent);
            }
        });

        TextView tvMenuPP = menuPrivacyPolicy.findViewById(R.id.tv_menu_title);
        tvMenuPP.setText("Privacy Policy");
        menuPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FaqTermsAndConditionsActivity.class);
                intent.putExtra("privacy_policy", true);
                startActivity(intent);
            }
        });

        TextView tvClientTnc = menuClientTnC.findViewById(R.id.tv_menu_title);
        tvClientTnc.setText("Client Terms and Conditions");
        menuClientTnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FaqTermsAndConditionsActivity.class);
                intent.putExtra("client_terms", true);
                startActivity(intent);
            }
        });



        //cache about us
        preferenceHelper = PreferenceHelper.getInstance(getActivity());
        String aboutUs = preferenceHelper.getAboutUs();
        if (TextUtils.isEmpty(aboutUs)) {
            progressBar.setVisibility(View.VISIBLE);
            tvAbout.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            tvAbout.setText(Html.fromHtml(aboutUs));
            tvAbout.setVisibility(View.VISIBLE);
        }

        if (NetworkUtils.isInNetwork(getActivity())) {
            aboutUsTask();
        }
    }


    private void aboutUsTask() {
        VolleyHelper volleyHelper = VolleyHelper.getInstance(getActivity());
        volleyHelper.addVolleyRequestListeners(Api.getInstance().aboutUsApi, Request.Method.POST,
                volleyHelper.getPostParams(), new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                String aboutUs = res.getJSONObject("about_us_data").getString("content");
                                preferenceHelper.edit().putString("aboutUs", aboutUs).commit();
                                preferenceHelper.edit().putString("contactUsNum", res.getString("call_support_no")).commit();
                                tvAbout.setText(Html.fromHtml(aboutUs));
                                tvAbout.setVisibility(View.VISIBLE);
                            }
                            progressBar.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            Logger.e("aboutUsTask res ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                    }
                }, "aboutUsTask");
    }
}
