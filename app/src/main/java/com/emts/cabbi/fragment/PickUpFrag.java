package com.emts.cabbi.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.LocationHelper;
import com.emts.cabbi.MapProvider;
import com.emts.cabbi.R;
import com.emts.cabbi.SwipeButton;
import com.emts.cabbi.activity.CancelBookingActivity;
import com.emts.cabbi.activity.MainActivity;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.VolleyHelper;
import com.emts.cabbi.model.Booking;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

/**
 * Created by User on 2017-08-02.
 */

public class PickUpFrag extends Fragment {
    Booking booking;
    GoogleMap map;
    AlertUtils.OnAlertButtonClickListener listener;
    TextView tvLocation;
    Marker marker;
    SwipeButton btnEndTrip, btnStartTrip;
    Button btnArrive, btnCancelTrip;
    //    private boolean isArrived = false;
    TextView tvLvlLocation;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        booking = (Booking) getArguments().getSerializable("booking");

        if (booking.getStatus().equals(Booking.TRIP_STATUS_SCHEDULED)) {
            ((MainActivity) getActivity()).setToolbarTitle("Pick Up");
        } else {
            ((MainActivity) getActivity()).setToolbarTitle("End Trip");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pick_up, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (booking == null) {
            return;
        }

        tvLvlLocation = view.findViewById(R.id.lvlLoc);

        LinearLayout llNav = view.findViewById(R.id.llNav);
        llNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (booking.getStatus().equals(Booking.TRIP_STATUS_SCHEDULED)) {
                    //navigate to pick up
                    startNavigationActivity(booking.getPickUpLat(), booking.getPickUpLong());
                } else {
                    //navigate to dropoff
                    startNavigationActivity(booking.getDropOffLat(), booking.getDropOffLong());
                }
            }
        });

        SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;

                //rider location
                if (!SmartLocation.with(getActivity()).location().state().isGpsAvailable()) {
                    listener = new AlertUtils.OnAlertButtonClickListener() {
                        @Override
                        public void onAlertButtonClick(boolean isPositiveButton) {
                            if (!LocationHelper.checkIfGPSEnable(getActivity())) {
                                MapProvider.showGPSEnableAlert(getActivity(), listener);
                            }
                        }
                    };
                    MapProvider.showGPSEnableAlert(getActivity(), listener);
                }

                Location pickUpLoc = new Location("");
                pickUpLoc.setLatitude(booking.getPickUpLat());
                pickUpLoc.setLongitude(booking.getPickUpLong());

                Location dropOffLoc = new Location("");
                dropOffLoc.setLatitude(booking.getDropOffLat());
                dropOffLoc.setLongitude(booking.getDropOffLong());

                final MapProvider mapProvider = new MapProvider();
                mapProvider.with(getActivity())
                        .onMap(map);
//                        .setStartLocation(pickUpLoc, booking.getPickUpLocation(), R.drawable.icon_location_a)
//                        .setEndLocation(dropOffLoc, booking.getDropOffLocation(), R.drawable.icon_pin_marker);

                mapProvider.showRout(pickUpLoc, dropOffLoc);
                mapProvider.moveTo(pickUpLoc, 10);

                SmartLocation.with(getActivity()).location().continuous().start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        Logger.e("PickUpFrag() onMapReady location continuous update", location.toString());
                        if (marker != null) {
                            marker.remove();
                        }
                        marker = mapProvider.addMarker(location, "", "", R.drawable.icon_pin_marker);
                    }
                });
            }
        });

        TextView tripFare = view.findViewById(R.id.tripFare);
        tripFare.setText("Trip Fare : " + "RM" + " " + booking.getTripFare());

        tvLocation = view.findViewById(R.id.tvLocationName);

        btnArrive = view.findViewById(R.id.btnPickUp);
        btnEndTrip = view.findViewById(R.id.btnEndTrip);
        btnStartTrip = view.findViewById(R.id.btnStartTrip);

        btnCancelTrip = view.findViewById(R.id.btnCancelTrip);
        btnCancelTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CancelBookingActivity.class);
                intent.putExtra("booking", booking);
                startActivityForResult(intent, REQUEST_CANCEL_BOOKING);
            }
        });

        if (booking.getStatus().equals(Booking.TRIP_STATUS_SCHEDULED)) {
            tvLvlLocation.setText("Pick Up Location");
            tvLocation.setText(booking.getPickUpLocation());
            btnArrive.setVisibility(View.VISIBLE);
            btnCancelTrip.setVisibility(View.VISIBLE);
            btnEndTrip.setVisibility(View.GONE);
            btnStartTrip.setVisibility(View.GONE);
        } else {
            tvLvlLocation.setText("Drop Off Location");
            tvLocation.setText(booking.getDropOffLocation());
            btnArrive.setVisibility(View.GONE);
            btnCancelTrip.setVisibility(View.GONE);
            btnEndTrip.setVisibility(View.VISIBLE);
            btnStartTrip.setVisibility(View.GONE);
            if (booking.getBookingType().equals(Booking.BOOKING_TYPE_INSTANT)) {
                tripFare.setVisibility(View.VISIBLE);
            }
        }
        btnArrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (SmartLocation.with(getActivity()).location().state().isGpsAvailable()) {
                    SmartLocation.with(getActivity()).location().oneFix().start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(Location location) {
                            if (NetworkUtils.isInNetwork(getActivity())) {
//                                if (isArrived) {
//                                    startTripTask(location.getLatitude(), location.getLongitude());
//                                } else {
                                arrivePickUpTask(location.getLatitude(), location.getLongitude());
//                                }
                            } else {
                                AlertUtils.showSnack(getActivity(), view, getString(R.string.no_internet));
                            }
                        }
                    });
                } else {
                    AlertUtils.simpleAlert(getActivity(), "Enable GPS", "Please enable GPS of the phone to proceed.",
                            "OK", "", null);
                }
            }
        });
        btnEndTrip.setOnSwipeListener(new SwipeButton.OnSwipeListener() {
            @Override
            public void onSwipeConfirm() {
                if (SmartLocation.with(getActivity()).location().state().isGpsAvailable()) {
                    SmartLocation.with(getActivity()).location().oneFix().start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(Location location) {
                            if (NetworkUtils.isInNetwork(getActivity())) {
                                endTripTask(location.getLatitude(), location.getLongitude());
                            } else {
                                AlertUtils.showSnack(getActivity(), view, getString(R.string.no_internet));
                            }
                        }
                    });
                } else {
                    AlertUtils.simpleAlert(getActivity(), "Enable GPS", "Please enable GPS of the phone to proceed.",
                            "OK", "", null);
                }
            }
        });
        btnStartTrip.setOnSwipeListener(new SwipeButton.OnSwipeListener() {
            @Override
            public void onSwipeConfirm() {
                if (SmartLocation.with(getActivity()).location().state().isGpsAvailable()) {
                    SmartLocation.with(getActivity()).location().oneFix().start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(Location location) {
                            if (NetworkUtils.isInNetwork(getActivity())) {
                                startTripTask(location.getLatitude(), location.getLongitude());
                            } else {
                                AlertUtils.showSnack(getActivity(), view, getString(R.string.no_internet));
                            }
                        }
                    });
                }
            }
        });

        TextView bookingNo = view.findViewById(R.id.bookingNo);
        bookingNo.setText("Booking No: " + booking.getBookingNo());
        TextView passengerName = view.findViewById(R.id.passengerName);
        passengerName.setText("Passenger : " + booking.getPassengerName());
    }

    int REQUEST_CANCEL_BOOKING = 9837;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CANCEL_BOOKING && resultCode == Activity.RESULT_OK) {
            btnCancelTrip.setVisibility(View.GONE);
            btnArrive.setVisibility(View.GONE);
            btnEndTrip.setVisibility(View.GONE);
        }
    }

    private void arrivePickUpTask(double latitude, double longitude) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Checking location...");
        VolleyHelper volleyHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("booking_id", booking.getBookingNo());
        postParams.put("latitude", String.valueOf(latitude));
        postParams.put("longitude", String.valueOf(longitude));

        volleyHelper.addVolleyRequestListeners(Api.getInstance().arriveForPickUp, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject responseData = new JSONObject(response);
                            if (responseData.getBoolean("status")) {
//                                showStartTripAlert();
//                                btnArrive.setText("Pick Up");
//                                btnCancelTrip.setVisibility(View.VISIBLE);
                                btnArrive.setVisibility(View.GONE);
                                btnCancelTrip.setVisibility(View.GONE);
                                btnStartTrip.setVisibility(View.VISIBLE);
                            } else {
                                AlertUtils.simpleAlert(getActivity(), false, "Error !!!",
                                        responseData.getString("message"), "OK",
                                        "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("arrivePickUpTask json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("arrivePickUpTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(getActivity(), "Error !!!",
                                errorMsg, "OK",
                                "", null);
                    }
                }, "arrivePickUpTask");

    }

    private void showStartTripAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View cusView = LayoutInflater.from(getActivity()).inflate(R.layout.alert_start_trip, null, false);
        builder.setView(cusView);
        final Dialog dialog = builder.show();

        SwipeButton btnStartTrip = cusView.findViewById(R.id.btn_start_trip);
        btnStartTrip.setOnSwipeListener(new SwipeButton.OnSwipeListener() {
            @Override
            public void onSwipeConfirm() {
                dialog.dismiss();
                if (SmartLocation.with(getActivity()).location().state().isGpsAvailable()) {
                    SmartLocation.with(getActivity()).location().oneFix().start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(Location location) {
                            if (NetworkUtils.isInNetwork(getActivity())) {
                                startTripTask(location.getLatitude(), location.getLongitude());
                            } else {
                                AlertUtils.showToast(getActivity(), getString(R.string.no_internet));
                            }
                        }
                    });
                } else {
                    AlertUtils.simpleAlert(getActivity(), "Enable GPS", "Please enable GPS of the phone to proceed.",
                            "OK", "", null);
                }
            }
        });
    }

    private void startTripTask(double latitude, double longitude) {
//        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Starting the trip...");
        VolleyHelper volleyHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("booking_id", booking.getBookingNo());
        postParams.put("latitude", String.valueOf(latitude));
        postParams.put("longitude", String.valueOf(longitude));
        postParams.put("action", "start");

        volleyHelper.addVolleyRequestListeners(Api.getInstance().startEndTripApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
//                        pDialog.dismiss();
                        try {
                            JSONObject responseData = new JSONObject(response);
                            if (responseData.getBoolean("status")) {
                                Activity activity = getActivity();
                                if (activity instanceof MainActivity) {
                                    ((MainActivity) activity).emitJobAccept(response);
                                }
                                //end trip
                                //change toolbar title, show end trip swipe button, update booking status
                                booking.setStatus(Booking.TRIP_STATUS_IN_PROGRESS);
                                ((MainActivity) getActivity()).setToolbarTitle("End Trip");
                                btnEndTrip.setVisibility(View.VISIBLE);
                                btnArrive.setVisibility(View.GONE);
                                btnCancelTrip.setVisibility(View.GONE);
                                btnStartTrip.setVisibility(View.GONE);
                                tvLvlLocation.setText("Drop Off Location");
                                tvLocation.setText(booking.getDropOffLocation());
                                btnStartTrip.showResultIcon(true, true);
//                                AlertUtils.showSnack(getActivity(), btnStartTrip, responseData.getString("message"));
                                AlertUtils.showSnack(getActivity(), btnStartTrip, "Trip Started !!! Start navigation to dropoff point");
                            } else {
                                btnStartTrip.showResultIcon(false, true);
                                AlertUtils.simpleAlert(getActivity(), false, "Error !!!",
                                        responseData.getString("message"), "OK",
                                        "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("startTripTask json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
//                        pDialog.dismiss();
                        btnStartTrip.showResultIcon(false, true);
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("startTripTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(getActivity(), "Error !!!",
                                errorMsg, "OK",
                                "", null);
                    }
                }, "startTripTask");

    }

    private void endTripTask(double latitude, double longitude) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), "Checking location...");
        VolleyHelper volleyHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("booking_id", booking.getBookingNo());
        postParams.put("latitude", String.valueOf(latitude));
        postParams.put("longitude", String.valueOf(longitude));
        postParams.put("action", "end");

        volleyHelper.addVolleyRequestListeners(Api.getInstance().startEndTripApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject responseData = new JSONObject(response);
                            if (responseData.getBoolean("status")) {
                                Activity activity = getActivity();
                                if (activity instanceof MainActivity) {
                                    ((MainActivity) activity).emitJobAccept(response);
                                }
                                btnEndTrip.showResultIcon(true);
                                //goto home
                                AlertUtils.simpleAlert(getActivity(), false, "Trip Completed !!!",
                                        responseData.getString("message"), "OK",
                                        "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(intent);
                                            }
                                        });
                            } else {
                                btnEndTrip.showResultIcon(false, true);
                                AlertUtils.simpleAlert(getActivity(), false, "Error !!!",
                                        responseData.getString("message"), "OK",
                                        "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("endTripTask json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("endTripTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(getActivity(), "Error !!!",
                                errorMsg, "OK",
                                "", null);
                    }
                }, "endTripTask");

    }

    private void startNavigationActivity(double lat, double lon) {
//        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + lat + "," + lon);
//        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//        mapIntent.setPackage("com.google.android.apps.maps");

        String uri = "geo:0,0?q=" + lat + "," + lon;
        Uri gmmIntentUri = Uri.parse(uri);
        final Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        try {
            startActivity(mapIntent);
        } catch (Exception e) {
            try {
                Intent navigation = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr=" + lat + "," + lon));
                startActivity(navigation);
            } catch (Exception e1) {
            }
        }
    }


}
