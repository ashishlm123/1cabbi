package com.emts.cabbi.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.emts.cabbi.LocationHelper;
import com.emts.cabbi.MapProvider;
import com.emts.cabbi.R;
import com.emts.cabbi.activity.MainActivity;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Logger;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationAccuracy;
import io.nlopez.smartlocation.location.config.LocationParams;

public class HomeFrag extends Fragment {
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 342;
    GoogleMap map;
    float zoomLevel;
    Marker riderMarker;
    ImageView btnMapCenter;
    AlertUtils.OnAlertButtonClickListener listener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getLocationPermission();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                btnMapCenter.setVisibility(View.VISIBLE);

                showCurrentLocation();

                map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {
                        zoomLevel = map.getCameraPosition().zoom;
                    }
                });
            }
        });

        btnMapCenter = view.findViewById(R.id.navigate_map);
        btnMapCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (map != null) {
                    showCurrentLocation();
                }
            }
        });

        Button btnMyBookingTrip = view.findViewById(R.id.btnBookingTrip);
        btnMyBookingTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).openMyBookingFrag();
            }
        });
    }

    private void showCurrentLocation() {
        if (LocationHelper.checkIfGPSEnable(getActivity())) {
            LocationParams locConfig = new LocationParams.Builder()
                    .setAccuracy(LocationAccuracy.HIGH)
                    .setDistance(15)
                    .setInterval(10000)
                    .build();
            Logger.e("listen for location ", "start once location listener");
            SmartLocation.with(getActivity()).location().continuous()
                    .config(locConfig)
                    .start(new OnLocationUpdatedListener() {
                        @Override
                        public void onLocationUpdated(Location location) {
                            Logger.e("onLocationUpdated onFix", location.toString());
                            showRiderMarker(location);
                        }
                    });
        } else {
            listener = new AlertUtils.OnAlertButtonClickListener() {
                @Override
                public void onAlertButtonClick(boolean isPositiveButton) {
                    if (!LocationHelper.checkIfGPSEnable(getActivity())) {
                        MapProvider.showGPSEnableAlert(getActivity(), listener);
                    } else {
                        showCurrentLocation();
                    }
                }
            };
            MapProvider.showGPSEnableAlert(getActivity(), listener);
        }
    }

    public void showRiderMarker(Location location) {
        try {
            if (location == null) {
                return;
            }
            //pickup marker
            LatLng riderLoc = new LatLng(location.getLatitude(), location.getLongitude());
            if (riderMarker != null) {
                if (riderMarker.isVisible()) {
                    riderMarker.remove();
                }
            }
            riderMarker = map.addMarker(new MarkerOptions()
                    .position(riderLoc)
//                    .snippet("here are you")
//                    .title("Title")
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_marker)));
            zoomLevel = 14;
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(riderLoc, zoomLevel));
        } catch (Exception e) {
            Logger.e("showRiderLocation ex", e.getMessage() + "");
        }
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
    }

    boolean mLocationPermissionGranted;

    @Override
    public void onStop() {
        SmartLocation.with(getActivity()).location().stop();
        super.onStop();
    }
}