package com.emts.cabbi.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.BookingParser;
import com.emts.cabbi.R;
import com.emts.cabbi.activity.MainActivity;
import com.emts.cabbi.adapter.FragmentPageAdapter;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.VolleyHelper;
import com.emts.cabbi.model.Booking;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MyBookingTripFrag extends Fragment {
    FragmentPageAdapter fragmentPageAdapter;
    TabLayout tabLayout;
    ViewPager viewPager;
    ProgressBar progressBar;
    TextView tvError;
    boolean isFirstTimeShowFrag;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        isFirstTimeShowFrag = bundle != null && bundle.getBoolean("isFirstPageShow");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_booking_trip, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = view.findViewById(R.id.progress);
        tvError = view.findViewById(R.id.error_text);

        tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = view.findViewById(R.id.view_pager);
        tabLayout.setupWithViewPager(viewPager);

        if (NetworkUtils.isInNetwork(getActivity())) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    myBookingTripsTask();
                }
            }, 200);
        } else {
            progressBar.setVisibility(View.GONE);
            tabLayout.setVisibility(View.GONE);
            viewPager.setVisibility(View.GONE);
            tvError.setText(R.string.no_internet);
            tvError.setVisibility(View.VISIBLE);
        }
    }

    public void addTabs(Booking instantBooking, Booking preBooking) {
        fragmentPageAdapter = new FragmentPageAdapter(getChildFragmentManager());

        Fragment instantBookingFrag = new BookingFrag();
        Bundle bundle = new Bundle();
        bundle.putSerializable("booking", instantBooking);
        bundle.putBoolean("instantBooking", true);
        instantBookingFrag.setArguments(bundle);
        fragmentPageAdapter.addFrag(instantBookingFrag, "Instant Booking");

        Fragment preBookingFrag = new BookingFrag();
        Bundle bundle1 = new Bundle();
        bundle1.putSerializable("booking", preBooking);
        bundle1.putBoolean("instantBooking", false);
        preBookingFrag.setArguments(bundle1);
        fragmentPageAdapter.addFrag(preBookingFrag, "Pre-Booking");

        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(fragmentPageAdapter);

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                //1st tab
                View view = tabLayout.getTabAt(0).view;
                view.setBackgroundResource(R.color.appGreen);


                //2nd tab
                View view1 = tabLayout.getTabAt(1).view;
                view1.setBackgroundResource(R.color.appBlue);
            }
        });
    }

    private void myBookingTripsTask() {
        progressBar.setVisibility(View.VISIBLE);
        tvError.setVisibility(View.GONE);
        tabLayout.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().getMyBookingsApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            progressBar.setVisibility(View.GONE);
                            JSONObject res = new JSONObject(response);

                            Booking instantBooking = null, preBooking = null;
                            if (res.getBoolean("status")) {
                                JSONArray instantArray = res.getJSONObject("data").getJSONArray("instant_booking");
                                if (instantArray.length() > 0) {
                                    instantBooking = BookingParser.getBooking(instantArray.getJSONObject(0).toString());
                                }
                                JSONArray preArray = res.getJSONObject("data").getJSONArray("pre_booking");
                                if (preArray.length() > 0) {
                                    preBooking = BookingParser.getBooking(preArray.getJSONObject(0).toString());
                                }
                            }
//                            else {
//                                tabLayout.setVisibility(View.GONE);
//                                viewPager.setVisibility(View.GONE);
//                                tvError.setText(res.getString("message"));
//                                tvError.setVisibility(View.VISIBLE);
//                            }

                            //CASE : show fragment if there is booking else show home fragment
                            if (isFirstTimeShowFrag && instantBooking == null && preBooking == null) {
                                ((MainActivity) getActivity()).openHomePage();
                                return;
                            }

                            addTabs(instantBooking, preBooking);
                            tabLayout.setVisibility(View.VISIBLE);
                            viewPager.setVisibility(View.VISIBLE);
                            tvError.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            Logger.e("myBookingTripsTask json ex", e.getMessage());
                        } catch (Exception e){

                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("myBookingTripsTask error ex", e.getMessage() + " ");
                        }
                        tabLayout.setVisibility(View.GONE);
                        viewPager.setVisibility(View.GONE);
                        tvError.setText(errorMsg);
                        tvError.setVisibility(View.VISIBLE);
                    }
                }, "myBookingTripsTask");
    }

    @Override
    public void onDetach() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("myBookingTripsTask");
        super.onDetach();
    }

//    @Override
//    public void onStop() {
//        VolleyHelper.getInstance(getActivity()).cancelRequest("myBookingTripsTask");
//        super.onStop();
//    }


    @Override
    public void onDestroy() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("myBookingTripsTask");
        super.onDestroy();
    }
}
