package com.emts.cabbi;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.emts.cabbi.activity.SplashActivity;
import com.emts.cabbi.helper.Logger;

import java.util.Date;

public class PreBookingAlertReceiver extends BroadcastReceiver {
    private static final String CHANNEL_ID = "booking_alert_channel";

    @Override
    public void onReceive(Context context, Intent intent) {
        Logger.e("PreBookingAlertReceiver onReceive", "Intent : " + intent);

        //show pre booking alert notification
        Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + context.getPackageName() + "/" + R.raw.cabbi_8x);

        Intent intent1 = new Intent(context, SplashActivity.class);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, CHANNEL_ID);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 235,
                intent1, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText("You have a pre-booking coming soon. Please check the app for your upcoming booking information.");
        bigText.setBigContentTitle("Upcoming Pre-Booking Alert");
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.noti_icon);
        mBuilder.setContentTitle("Upcoming Pre-Booking Alert");
        mBuilder.setContentText("You have a pre-booking coming soon. Please check the app for your upcoming booking information.");
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText);
        mBuilder.setSound(sound);
        mBuilder.setAutoCancel(true);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "1cabbi Notification",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);

            //sound
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            channel.setSound(sound, attributes);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[] { 1000, 1000, 1000, 1000, 1000});
        }

        int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        mNotificationManager.notify(m, mBuilder.build());
    }
}
