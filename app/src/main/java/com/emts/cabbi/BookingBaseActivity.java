package com.emts.cabbi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.activity.MainActivity;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.AudioPlayer;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.VolleyHelper;
import com.emts.cabbi.model.Booking;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Objects;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public abstract class BookingBaseActivity extends CabbiBaseActivity {
    private static final String TAG = "BookingBaseActivity socket";
    public BookingReceiver bookingReceiver;
    public BookingCancelledReceiver bookingCancelReceiver;
    public static final String INTENT_BOOKING_BROADCAST = "com.emts.icabbi.INTENT_BOOKING_BROADCAST";
    public static final String INTENT_BOOKING_CANCEL = "com.emts.icabbi.INTENT_BOOKING_CANCEL_BROADCAST";
    private Dialog bookingDialog;
    CountDownTimer countDownTimer;
    ProgressBar progressBar;
    TextView textViewShowTime;
    AudioPlayer audioPlayer;

    PowerManager.WakeLock wl;
    public static boolean isActivityOnForGround;
    public static Activity currentActivity;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(bookingReceiver, new IntentFilter(INTENT_BOOKING_BROADCAST));
        registerReceiver(bookingCancelReceiver, new IntentFilter(INTENT_BOOKING_CANCEL));

        getWakeLock();

        isActivityOnForGround = true;
    }


    private void getWakeLock() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //power manager
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "com.emts.comfort:mywakelocktag");
        wl.acquire();
    }

    private void releaseWakeLock() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //power manager
        try {
            if (wl != null && wl.isHeld()) {
                wl.release();
            }
        } catch (Exception e) {
            //doesnot matter is wake lock is locked
        }
    }

    @Override
    protected void onPause() {
        releaseWakeLock();
        super.onPause();
        if (bookingReceiver != null)
            unregisterReceiver(bookingReceiver);
        if (bookingCancelReceiver != null)
            unregisterReceiver(bookingCancelReceiver);
        Logger.e("baseActivity unRegisterReceiver", "booking receiver unregistered");

        isActivityOnForGround = false;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bookingReceiver = new BookingReceiver();
        bookingCancelReceiver = new BookingCancelledReceiver();

        //connect to socket here
        connectToServerSocket();
    }

    private void createAlert(Context context) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setCancelable(false);
        View alertView = LayoutInflater.from(context).inflate(R.layout.alert_booking, null);
        alert.setView(alertView);
        bookingDialog = alert.create();
        Objects.requireNonNull(bookingDialog.getWindow()).setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        Logger.e("Deal Alert", "222 create deal alert !!!");
    }

    private void dismisBookingAlert() {
        if (bookingDialog != null) {
            bookingDialog.dismiss();
        }
    }

    private class BookingReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Logger.e("baseActivity bookingReceiver onReceive", "Booking Reveived :" + intent);
            if (intent.getAction() == null || !intent.getAction().equals(INTENT_BOOKING_BROADCAST)) {
                return;
            }
            try {
                Booking booking = (Booking) intent.getSerializableExtra("booking");
                if (booking.getBookingType().equals(Booking.BOOKING_TYPE_PRE)
                        || booking.getBookingType().equals(Booking.BOOKING_TYPE_INSTANT)) {
                    //send broadCast and notification too

                    if (currentActivity == null || currentActivity.isFinishing()) {
                        Logger.e("baseActivity DealReceiver", "************************************" +
                                "\n******************************** \n" +
                                "deal received but activity not found\n"
                                + "*********************************************\n" +
                                "*******************************************");
                        return;
                    }
                    showBookingAlert(currentActivity, booking);
//                    showBookingAlert(context, booking);
                }
            } catch (Exception e) {
                Logger.e("BookingReceiver onReceive ex", e.getMessage() + " ");
            }
        }
    }

    private class BookingCancelledReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(final Context context, Intent intent) {
            Logger.e("baseActivity BookingCancelledReceiver onReceive", "Booking Cancelled :" + intent);
            try {
                final AudioPlayer audioPlayer = new AudioPlayer();
                AlertUtils.simpleAlert(BookingBaseActivity.this, "Booking Cancelled",
                        intent.getStringExtra("message"), "OK", "", new AlertUtils.OnAlertButtonClickListener() {
                            @Override
                            public void onAlertButtonClick(boolean isPositiveButton) {
                                audioPlayer.stop();
                                Intent intent = new Intent(context, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(intent);
                            }
                        });
                audioPlayer.play(BookingBaseActivity.this, R.raw.cabbi_cancel, 0);
            } catch (Exception e) {
                Logger.e("BookingCancelledReceiver onReceive ex", e.getMessage() + " ");
            }
        }
    }

    public void showBookingAlert(final Context context, final Booking booking) {
        if (bookingDialog != null) {
            dismisBookingAlert();
            bookingDialog = null;
        }
        if (countDownTimer != null) countDownTimer.cancel();
        createAlert(context);

        if (!bookingDialog.isShowing()) {
            vibrate(context);
            playNotificationSound(context);
            bookingDialog.show();
            wakeUpTheScreen(context);
            Objects.requireNonNull(bookingDialog.getWindow()).setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        //progress holder
        FrameLayout holderProgress = bookingDialog.findViewById(R.id.progressHolder);
        View progressView = null;
        if (booking.getBookingType().equals(Booking.BOOKING_TYPE_INSTANT)) {
            progressView = getLayoutInflater().inflate(R.layout.layout_instant_progress, null, false);
            holderProgress.addView(progressView);
        } else {
            progressView = getLayoutInflater().inflate(R.layout.layout_pre_progress, null, false);
            holderProgress.addView(progressView);
        }

        TextView tvDate = bookingDialog.findViewById(R.id.tv_booking_date_time);
        Logger.e("booking dialog", tvDate + " ***");
        SimpleDateFormat resFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat requireFormatter = new SimpleDateFormat("dd MMM yyyy, HH:mm a");
        try {
            tvDate.setText(requireFormatter.format(resFormatter.parse(booking.getDate())));
        } catch (ParseException e) {
            tvDate.setText(booking.getDate());
        }

        progressBar = progressView.findViewById(R.id.progressbar1_timerview);
        textViewShowTime = progressView.findViewById(R.id.textView_timerview_time);

        startTimer(booking.getTimeToLive());
        progressBar.setVisibility(View.VISIBLE);

        LinearLayout layPickupLocation, layPassengerContact;
        layPickupLocation = bookingDialog.findViewById(R.id.lay_pickup_location_holder);
        layPassengerContact = bookingDialog.findViewById(R.id.lay_phone_holder);
        TextView tvBookingType = bookingDialog.findViewById(R.id.tv_booking_type);

        View holderPreAddress = bookingDialog.findViewById(R.id.preAddressHolder);

        View holderNote = bookingDialog.findViewById(R.id.holderNote);

        TextView tvPassengerName = bookingDialog.findViewById(R.id.tv_passenger_name);
        tvPassengerName.setText(booking.getPassengerName());

        if (booking.getBookingType().equals(Booking.BOOKING_TYPE_INSTANT)) {
            tvBookingType.setText("Instant Booking");
            TextView tvPickupLocation = layPickupLocation.findViewById(R.id.tv_pickup_location);
            tvPickupLocation.setText(booking.getPickUpLocation());
            layPickupLocation.setVisibility(View.VISIBLE);
            layPassengerContact.setVisibility(View.GONE);
            holderPreAddress.setVisibility(View.GONE);
            holderNote.setVisibility(View.GONE);

            //center
            LinearLayout holderPassengerName = bookingDialog.findViewById(R.id.holderPassengerName);
            holderPassengerName.setGravity(Gravity.CENTER);

        } else {
            tvBookingType.setText("Pre Booking");
            layPickupLocation.setVisibility(View.GONE);
            TextView tvPassengerContact = layPassengerContact.findViewById(R.id.tv_phone_no);
            tvPassengerContact.setText(booking.getPassengerContact());
            layPassengerContact.setVisibility(View.VISIBLE);
            layPassengerContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + booking.getPassengerContact()));
                    startActivity(intent);
                }
            });

            TextView lvlPassengerName = bookingDialog.findViewById(R.id.lvlPassengerName);
            lvlPassengerName.setText("Passenger : ");

            TextView tvPickUpLoc = holderPreAddress.findViewById(R.id.tvPickupLocation);
            TextView tvDropOffLoc = holderPreAddress.findViewById(R.id.tv_drop_off_location);
            tvPickUpLoc.setText(booking.getPickUpLocation());
            tvDropOffLoc.setText(booking.getDropOffLocation());
            holderPreAddress.setVisibility(View.VISIBLE);

            //fare
            View holderFare = bookingDialog.findViewById(R.id.holderFare);
            TextView tvFare = holderFare.findViewById(R.id.tv_trip_fare);
            tvFare.setText("RM" + " " + booking.getTripFare());
            holderFare.setVisibility(View.VISIBLE);

            TextView tvNote = holderNote.findViewById(R.id.tvNote);
            String note = booking.getNote();
            if (TextUtils.isEmpty(note)) {
                holderNote.setVisibility(View.GONE);
            } else {
                tvNote.setText(note);
                holderNote.setVisibility(View.VISIBLE);
            }
        }

        Button btnAccept, btnReject;
        btnAccept = bookingDialog.findViewById(R.id.btn_accept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (audioPlayer != null) {
                    audioPlayer.stop();
                }
                acceptBookingTask(context, booking);
            }
        });
        btnReject = bookingDialog.findViewById(R.id.btn_reject);
        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (audioPlayer != null) {
                    audioPlayer.stop();
                }
                dismisBookingAlert();
                rejectBookingTask(context, booking);
            }
        });
    }

    private void wakeUpTheScreen(Context context) {
        //wake the screen
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK |
                PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "com.emts.cabbi:mywakelockTag");
        wakeLock.acquire(1000);

        //release the wake lock
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
        keyguardLock.disableKeyguard();
    }

    private void playNotificationSound(Context context) {
        try {
            //default notificaton sound
//            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            Ringtone r = RingtoneManager.getRingtone(context, notification);
//            r.play();

            //custom sound
            audioPlayer = new AudioPlayer();
            audioPlayer.play(BookingBaseActivity.this, R.raw.cabbi_8x, 9999);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void vibrate(Context context) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        if (v == null) return;
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(500);
        }
    }

    private void acceptBookingTask(final Context context, final Booking booking) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(context, "Accepting booking...");
        VolleyHelper volleyHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("booking_id", booking.getBookingNo());

        volleyHelper.addVolleyRequestListeners(Api.getInstance().acceptBooking, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject responseData = new JSONObject(response);
                            if (responseData.getBoolean("status")) {
                                emitJobAccept(response);
                                if (countDownTimer != null) {
                                    countDownTimer.cancel();
                                }
                                dismisBookingAlert();
                                //set remainder for driver 1 hour ago of pre booking
                                if (booking.getBookingType().equalsIgnoreCase(Booking.BOOKING_TYPE_PRE)) {
                                    setAlarm(responseData.getLong("time_remaining_in_sec"));
                                }

                                Intent intent = new Intent(context, MainActivity.class);
                                intent.putExtra("acceptBooking", true);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                context.startActivity(intent);
                            } else {
                                AlertUtils.simpleAlert(context, false, "Booking Unsuccessful !!!",
                                        responseData.getString("message"), "OK",
                                        "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                bookingDialog.dismiss();
                                            }
                                        });
                            }
                        } catch (JSONException e) {
                            Logger.e("acceptBookingTask json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("acceptBookingTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(context, "Booking Unsuccessful !!!",
                                errorMsg, "OK",
                                "", new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        bookingDialog.dismiss();
                                    }
                                });
                    }
                }, "acceptBookingTask");

    }

    private void setAlarm(long time) {
        time = time * 1000; //milliseconds
        Intent preBookingAlarmReceiver = new Intent(getApplicationContext(), PreBookingAlertReceiver.class);
        PendingIntent pen = PendingIntent.getBroadcast(getApplicationContext(), 1, preBookingAlarmReceiver,
                PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarms = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        if (Build.VERSION.SDK_INT > 22) {
//            alarms.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time, recurringDownload);
            alarms.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time, pen);
        } else {
            alarms.setExact(AlarmManager.RTC_WAKEUP, time, pen);
        }
        Logger.e("Pre - Booking Notifier", "alarm set for prebooking after time : " + time);
    }

    private void rejectBookingTask(final Context context, Booking booking) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(context, "Rejecting booking...");
        VolleyHelper volleyHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("booking_id", booking.getBookingNo());

        volleyHelper.addVolleyRequestListeners(Api.getInstance().rejectBooking, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject responseData = new JSONObject(response);
                            if (responseData.getBoolean("status")) {
                                emitJobAccept(response);
                                if (countDownTimer != null) {
                                    countDownTimer.cancel();
                                }
                                AlertUtils.simpleAlert(context, false, "Success !!!",
                                        responseData.getString("message"), "OK",
                                        "", null);
                            } else {
                                AlertUtils.simpleAlert(context, false, "Error !!!",
                                        responseData.getString("message"), "OK",
                                        "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("rejectBookingTask json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("rejectBookingTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(context, "Error !!!",
                                errorMsg, "OK",
                                "", null);
                    }
                }, "rejectBookingTask");

    }

    private void startTimer(int time) {
        long totalTimeCountInMilliseconds = time * 1000;
        progressBar.setProgress(100);
        progressBar.setMax(time * 1000);
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
        countDownTimer = new CountDownTimer(totalTimeCountInMilliseconds, 1) {
            @SuppressLint({"DefaultLocale", "SetTextI18n"})
            @Override
            public void onTick(long leftTimeInMilliseconds) {
                long seconds = leftTimeInMilliseconds / 1000;
                progressBar.setProgress((int) (leftTimeInMilliseconds));
                textViewShowTime.setText(String.format("%02d", seconds / 60) + "." + String.format("%02d", seconds % 60));
            }

            @Override
            public void onFinish() {
                Logger.e("countDownTimer onFinish", " timer finish ************ STOP ***********");
                textViewShowTime.setText("");
                progressBar.setProgress(100);
                textViewShowTime.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                if (bookingDialog != null) {
                    bookingDialog.dismiss();
                }
            }
        }.start();
    }

    public void recycle() {
        try {
            unregisterReceiver(bookingReceiver);
        } catch (Exception e) {
        }
        bookingReceiver = null;
        if (bookingDialog != null) {
            if (bookingDialog.isShowing()) {
                bookingDialog.dismiss();
            }
            bookingDialog = null;
        }
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        currentActivity = null;
    }

    public void checkFromPush(Intent intent) {
        try {
            if (intent.getBooleanExtra("fromPush", false)) {
                Intent intent1 = new Intent(getApplicationContext(), MainActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent1);
            }
        } catch (Exception e) {
        }
    }


    //socket io
    public static Socket socket;

    private void connectToServerSocket() {
        if (socket != null && socket.connected()) {
            return;
        }
        try {
            socket = IO.socket(Api.getInstance().socketUrl);
            Logger.e(TAG, "connectToServerSocket URL:" + Api.getInstance().socketUrl);
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Logger.e(TAG + " connectToServerSocket event_connect", "****Connected : " + args);
                }

            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Logger.e(TAG + " connectToServerSocket event_disconnected", "Connection lost )))) : " + args);
                }

            });
            socket.connect();
        } catch (URISyntaxException e) {
            Logger.e(TAG + " connectToServerSocket ex", e.getMessage() + " ");
        }
    }

    //    START NODE JS LISTENERS
    public void emitJobAccept(String response) {
        if (socket.connected()) {
            Logger.e(TAG + " emitJobAccept emit", "emitter for socket connected and join to room");
            //emitter to post data
            try {
                JSONObject roomObj = new JSONObject(response);
                Logger.e(TAG + " nodeJs emitJobAccept emit data", roomObj.toString() + " **");

                socket.emit("send_driver_response", roomObj, new Ack() {
                    @Override
                    public void call(Object... args) {
                        Logger.e(TAG + " emitJobAccept ack",
                                "Ack received for send_driver_response :" +
                                        (args.length > 0 ? args[0].toString() : "0"));
                    }
                });
            } catch (JSONException e) {
                Logger.e(TAG + " emitJobAccept ex15", e.getMessage() + "");
            }
        } else {
            connectToServerSocket();
        }
    }
}
