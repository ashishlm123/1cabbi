package com.emts.cabbi;

import android.content.Context;
import android.location.LocationManager;


public class LocationHelper {

    public static boolean checkIfGPSEnable(Context context) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

//    @SuppressLint("CheckResult")
//    public void getLastKnownLocation(Context context, Consumer<Location> consumer) {
//        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(context);
//        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
//                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            return;
//        }
//        Disposable dis = locationProvider.getLastKnownLocation()
//                .subscribe(consumer);
//    }
//
//    public Subscription setLocationSubscribe(Context context) {
//        if (ActivityCompat.checkSelfPermission(context,
//                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
//                && ActivityCompat.checkSelfPermission(context,
//                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            return null;
//        }
//
//        LocationRequest request = LocationRequest.create() //standard GMS LocationRequest
//                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
////                .setNumUpdates(5)
//                .setInterval(100);
////        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(context);
////        Subscription subscription = locationProvider.getUpdatedLocation(request)
//////                .filter(...)    // you can filter location updates
//////    .map(...)       // you can map location to sth different
//////    .flatMap(...)   // or event flat map
//////    ...             // and do everything else that is provided by RxJava
////                .subscribe(new Consumer<Location>() {
////                    @Override
////                    public void accept(Location location) throws Exception {
////
////                    }
////                });
////        return subscription;
//        return null;
//    }
}
