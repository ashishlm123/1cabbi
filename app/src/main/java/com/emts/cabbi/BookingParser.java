package com.emts.cabbi;

import android.content.Context;
import android.content.Intent;

import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.model.Booking;

import org.json.JSONObject;

import java.util.Map;

public class BookingParser {

    public static Booking getBooking(String response) {
        try {
            JSONObject bookingObj = new JSONObject(response);
            Booking booking = new Booking();
            booking.setOtherCharges(bookingObj.getString("other_charges"));
            booking.setOperatorServiceFee(bookingObj.getString("operator_service_fee"));
            booking.setBookingNo(bookingObj.getString("booking_no"));
//            booking.setTimeToLive(bookingObj.getInt("max_time_to_driver_response"));
            booking.setTripTime(bookingObj.getString("journey_time"));
            booking.setCabbiServiceFee(bookingObj.getString("cabbi_service_fee"));
            booking.setPickUpLat(bookingObj.getDouble("pickup_lat"));
            booking.setPickUpLong(bookingObj.getDouble("pickup_lng"));
            booking.setPickUpLocation(bookingObj.getString("pickup_location"));
            booking.setDropOffLat(bookingObj.getDouble("dropoff_lat"));
            booking.setDropOffLong(bookingObj.getDouble("dropoff_lng"));
            booking.setDropOffLocation(bookingObj.getString("dropoff_location"));
            booking.setTripFare(bookingObj.getString("trip_fare"));
            booking.setPassengerName(bookingObj.getString("passenger_name"));
            booking.setPassengerContact(bookingObj.getString("mobile_no"));
            booking.setStatus(bookingObj.getString("trip_status"));
            booking.setDistance(bookingObj.getString("distance"));
            booking.setCooperateServiceFee(bookingObj.getString("corporate_service_fee"));
            booking.setDate(bookingObj.getString("datetime"));
            booking.setBookingType(bookingObj.getString("booking_type"));
            booking.setNote(bookingObj.optString("note"));
            return booking;
        } catch (Exception e) {
            Logger.e("BookingParser getBooking ex", e.getMessage() + " ");
        }
        return null;
    }

    public static Booking bookingFromPush(Map<String, String> bookingPush) {
        Booking booking = new Booking();
        booking.setOtherCharges(bookingPush.get("other_charges"));
        booking.setOperatorServiceFee(bookingPush.get("operator_service_fee"));
        booking.setBookingNo(bookingPush.get("booking_no"));
        booking.setTimeToLive(Integer.parseInt(bookingPush.get("max_time_to_driver_response")));
        booking.setTripTime(bookingPush.get("journey_time"));
        booking.setCabbiServiceFee(bookingPush.get("cabbi_service_fee"));
        booking.setPickUpLat(Double.parseDouble(bookingPush.get("pickup_lat")));
        booking.setPickUpLong(Double.parseDouble(bookingPush.get("pickup_lng")));
        booking.setPickUpLocation(bookingPush.get("pickup_location"));
        booking.setDropOffLat(Double.parseDouble(bookingPush.get("dropoff_lat")));
        booking.setDropOffLong(Double.parseDouble(bookingPush.get("dropoff_lng")));
        booking.setDropOffLocation(bookingPush.get("dropoff_location"));
        booking.setTripFare(bookingPush.get("trip_fare"));
        booking.setPassengerName(bookingPush.get("passenger_name"));
        booking.setPassengerContact(bookingPush.get("mobile_no"));
        booking.setStatus(bookingPush.get("trip_status"));
        booking.setDistance(bookingPush.get("distance"));
        booking.setCooperateServiceFee(bookingPush.get("corporate_service_fee"));
        booking.setDate(bookingPush.get("datetime"));
        booking.setBookingType(bookingPush.get("booking_type"));
        booking.setNote(bookingPush.get("note"));
        return booking;
    }

    public static void getBookingAlertTest(Context context) {
        Booking booking = new Booking();
        booking.setOtherCharges("1212");
        booking.setOperatorServiceFee("12.12");
        booking.setBookingNo("00053T");
        booking.setTimeToLive(70);
        booking.setTripTime("12");
        booking.setCabbiServiceFee("111");
        booking.setPickUpLat(27.8812142);
        booking.setPickUpLong(84.5615322);
        booking.setPickUpLocation("Kupondal Lalitpur -11, Kathmandu Bagmati, Nepal +0977 Rupak School Nera Xa emts solutions lasdkfj asd lksjdf lkasdfj THE END ");
        booking.setDropOffLat(27.8812142);
        booking.setDropOffLong(84.5615322);
        booking.setDropOffLocation("Kupondal Lalitpur -11, Kathmandu Bagmati, Nepal +0977 Rupak School Nera Xa emts solutions lasdkfj asd lksjdf lkasdfj THE END ");
        booking.setTripFare("211");
        booking.setPassengerName("Theone Sharma");
        booking.setPassengerContact("9803681065");
        booking.setStatus("1");
        booking.setDistance("232");
        booking.setCooperateServiceFee("21211");
        booking.setDate("2018-11-13");
        booking.setBookingType(Booking.BOOKING_TYPE_PRE);
        booking.setNote("This is a note .|.");

        Intent intent1 = new Intent(BookingBaseActivity.INTENT_BOOKING_BROADCAST);
        intent1.putExtra("booking", booking);
        context.sendBroadcast(intent1);
    }
}
