package com.emts.cabbi.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.R;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FilterTripHistoryActivity extends BookingBaseActivity {
    Calendar startDateCalendar, endDateCalendar, calendarNow;
    Date sDate, eDate;

    @Override
    protected void onResume() {
        currentActivity = this;
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentActivity = this;
    }

    @Override
    protected void onStop() {
//        currentActivity = null;
        super.onStop();
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_filter_trip_history);

        calendarNow = Calendar.getInstance();

        final Intent intent = getIntent();
        String getStartDate = intent.getStringExtra("startDate");
        String getEndDate = intent.getStringExtra("endDate");

        final Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.cancel_button_menu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.cancel) {
                    FilterTripHistoryActivity.this.finish();
                }
                return false;
            }
        });
        toolbar.setNavigationIcon(null);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Filter");

        final TextView tvStartDate, tvEndDate;

        tvStartDate = findViewById(R.id.tv_start_date);
        tvEndDate = findViewById(R.id.tv_end_date);

        tvStartDate.setText(calendarNow.get(Calendar.YEAR) + "-" + String.format("%02d", (calendarNow.get(Calendar.MONTH) + 1))
                + "-" + String.format("%02d", (calendarNow.get(Calendar.DAY_OF_MONTH))));

        tvEndDate.setText(calendarNow.get(Calendar.YEAR) + "-" + String.format("%02d", (calendarNow.get(Calendar.MONTH) + 1))
                + "-" + String.format("%02d", (calendarNow.get(Calendar.DAY_OF_MONTH))));

        startDateCalendar = Calendar.getInstance();
        endDateCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener startDate = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                startDateCalendar.set(Calendar.YEAR, year);
                startDateCalendar.set(Calendar.MONTH, month);
                startDateCalendar.set(Calendar.DAY_OF_MONTH, day);
                tvStartDate.setText(year + "-" + String.format("%02d", (month + 1)) + "-" +
                        String.format("%02d", (day)));
            }
        };

        tvStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(FilterTripHistoryActivity.this, startDate, startDateCalendar
                        .get(Calendar.YEAR), startDateCalendar.get(Calendar.MONTH),
                        startDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener expiryDate = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                endDateCalendar.set(Calendar.YEAR, year);
                endDateCalendar.set(Calendar.MONTH, month);
                endDateCalendar.set(Calendar.DAY_OF_MONTH, day);
                tvEndDate.setText(year + "-" + String.format("%02d", (month + 1)) + "-"
                        + String.format("%02d", (day)));
            }
        };

        tvEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(FilterTripHistoryActivity.this, expiryDate, endDateCalendar
                        .get(Calendar.YEAR), endDateCalendar.get(Calendar.MONTH),
                        endDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        @SuppressLint("SimpleDateFormat") SimpleDateFormat sD1 = new SimpleDateFormat("yyyy-MM-dd");
        try {
            sDate = sD1.parse(getStartDate);
            startDateCalendar.setTime(sDate);
            tvStartDate.setText(getStartDate);
        } catch (ParseException e) {
            Logger.e("sDate parse ex", e.getMessage());
        }

        try {
            eDate = sD1.parse(getEndDate);
            endDateCalendar.setTime(eDate);
            tvEndDate.setText(getEndDate);
        } catch (ParseException e) {
            Logger.e("eDate parse ex", e.getMessage());
        }

        Button btnApply = findViewById(R.id.btn_apply);
        Button btnReset = findViewById(R.id.btn_reset);
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tvStartDate.getText().toString().trim().length() > 0 && tvEndDate.getText().toString().trim().length() > 0) {
                    Logger.e("sDate:" + startDateCalendar.getTime(), "eDate:" + endDateCalendar.getTime());
                    if (!startDateCalendar.getTime().before(endDateCalendar.getTime())) {
                        AlertUtils.showToast(getApplicationContext(), "End date must be after start date");
                        return;
                    }
                }

                Intent intent = new Intent();
                intent.putExtra("startDate", tvStartDate.getText().toString().trim());
                intent.putExtra("endDate", tvEndDate.getText().toString().trim());
                setResult(RESULT_OK, intent);
                FilterTripHistoryActivity.this.finish();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                tvStartDate.setText("");
                tvStartDate.setHint(calendarNow.get(Calendar.YEAR) + "-"
                        + String.format("%02d", (calendarNow.get(Calendar.MONTH) + 1))
                        + "-" + String.format("%02d", (calendarNow.get(Calendar.DAY_OF_MONTH))));

                tvEndDate.setText("");
                tvEndDate.setHint(calendarNow.get(Calendar.YEAR) + "-"
                        + String.format("%02d", (calendarNow.get(Calendar.MONTH) + 1))
                        + "-" + String.format("%02d", (calendarNow.get(Calendar.DAY_OF_MONTH))));

                setResult(RESULT_CANCELED);
            }
        });
    }
}
