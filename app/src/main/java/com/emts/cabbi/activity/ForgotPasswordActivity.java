package com.emts.cabbi.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.CabbiBaseActivity;
import com.emts.cabbi.R;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Srijana on 7/6/2017.
 */

public class ForgotPasswordActivity extends CabbiBaseActivity {
    EditText etEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgot_password);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText(R.string.title_forgot_pass);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        etEmail = findViewById(R.id.et_forget_email);


        Button sendPassword = findViewById(R.id.btn_send_pass);
        sendPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    if (Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
                        forgetPasswordTask(etEmail.getText().toString().trim());
                    } else {
                        AlertUtils.showSnack(ForgotPasswordActivity.this, view, getString(R.string.error_valid_email));
                    }
                } else {
                    AlertUtils.showSnack(ForgotPasswordActivity.this, view, getString(R.string.no_internet));
                }
            }
        });

    }

    private void forgetPasswordTask(String email) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(ForgotPasswordActivity.this, "Please wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        //post email value here
        postParams.put("email", email);

        //url left
        vHelper.addVolleyRequestListeners(Api.getInstance().forgotPasswordApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                etEmail.setText("");
                                AlertUtils.simpleAlert(ForgotPasswordActivity.this, "Error",
                                        res.getString("message"), "OK", "", null);
                            } else {
                                AlertUtils.simpleAlert(ForgotPasswordActivity.this, "Error",
                                        res.getString("message"), "OK", "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("forgetPasswordTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("forgetPasswordTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(ForgotPasswordActivity.this, "Error", errorMsg,
                                "OK", "", null);
                    }
                }, "forgetPasswordTask");
    }
}
