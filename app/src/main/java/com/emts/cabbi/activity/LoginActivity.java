package com.emts.cabbi.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.CabbiBaseActivity;
import com.emts.cabbi.R;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.PreferenceHelper;
import com.emts.cabbi.helper.VolleyHelper;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Srijana on 7/6/2017.
 */

public class LoginActivity extends CabbiBaseActivity {
    boolean isValid;
    EditText emailId, password;
    TextView errorEmail, errorPassword;
    PreferenceHelper prefsHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        FirebaseApp.initializeApp(getApplicationContext());
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        checkVersion();

        prefsHelper = PreferenceHelper.getInstance(this);

        Button btnLogin = findViewById(R.id.btn_login);
        final TextView tvForgotPassword = findViewById(R.id.tv_forgot_password);
        emailId = findViewById(R.id.edt_email);
        password = findViewById(R.id.edt_password);
        errorEmail = findViewById(R.id.error_email);
        errorPassword = findViewById(R.id.error_password);
        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ForgotPasswordActivity.class));
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isValid = true;
                if (TextUtils.isEmpty(password.getText().toString().trim())) {
                    isValid = false;
                    errorPassword.setVisibility(View.VISIBLE);
                    errorPassword.setText(R.string.error_field_required);
                } else {
                    errorPassword.setVisibility(View.GONE);
                }

                if (!Patterns.EMAIL_ADDRESS.matcher(emailId.getText().toString().trim()).matches()) {
                    isValid = false;
                    errorEmail.setVisibility(View.VISIBLE);
                    errorEmail.setText(R.string.error_valid_email);
                } else {
                    errorEmail.setVisibility(View.GONE);
                }

                if (isValid) {
                    if (NetworkUtils.isInNetwork(LoginActivity.this)) {
                        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                            @Override
                            public void onSuccess(InstanceIdResult instanceIdResult) {
                                String deviceId = instanceIdResult.getToken();
                                loginTask(deviceId);
                            }
                        });
//                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    } else {
                        AlertUtils.hideInputMethod(LoginActivity.this, view);
                        AlertUtils.simpleAlert(LoginActivity.this, "Error!!!", getString(R.string.no_internet));
                    }
                }
            }
        });
    }

    private void loginTask(String deviceId) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(LoginActivity.this, "Please wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("username", emailId.getText().toString().trim());
        postParams.put("password", password.getText().toString().trim());
        postParams.put("device_id", deviceId);

        vHelper.addVolleyRequestListeners(Api.getInstance().loginApi, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                final JSONObject dataObj = res.getJSONObject("user_info");
                                prefsHelper.edit().putString(PreferenceHelper.USER_ID, dataObj.getString("id")).apply();

                                String imgDirectory = dataObj.getString("directory");
                                String profileImg = dataObj.getString("profile_photo");
                                if (!TextUtils.isEmpty(profileImg) && !profileImg.equals("null")) {
                                    prefsHelper.edit().putString(PreferenceHelper.USER_IMG, imgDirectory + profileImg).apply();
                                }

                                prefsHelper.edit().putString(PreferenceHelper.USER_FULL_NAME, dataObj.getString("full_name")).apply();
                                prefsHelper.edit().putString(PreferenceHelper.USER_NAME, dataObj.getString("user_name")).apply();
                                prefsHelper.edit().putString(PreferenceHelper.USER_EMAIL, dataObj.getString("email")).apply();
                                prefsHelper.edit().putString(PreferenceHelper.TOKEN, dataObj.getString("user_token")).apply();
                                prefsHelper.edit().putString(PreferenceHelper.USER_MOBILE_NO, dataObj.getString("mobile_no")).apply();
                                prefsHelper.edit().putString(PreferenceHelper.USER_VEHICLE_PLATE_NO, dataObj.getString("plate_no")).apply();
                                prefsHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).apply();
                                prefsHelper.edit().putString(PreferenceHelper.USER_TOKEN, res.getString("user_token")).apply();
                                prefsHelper.edit().putString(PreferenceHelper.DRIVER_STATUS, PreferenceHelper.DRIVER_ONLINE).apply();
                                prefsHelper.edit().putString(PreferenceHelper.DRIVER_CATEGORY, dataObj.getString("vehicle_type")).apply();

                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            } else {
                                final String errorMsg = res.getString("message");
                                AlertUtils.simpleAlert(LoginActivity.this, "Login Error", errorMsg,
                                        "OK", "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("loginTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("loginTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(LoginActivity.this, "Error", errorMsg,
                                "OK", "", null);
                    }
                }, "loginTask");
    }
}
