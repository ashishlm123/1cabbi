package com.emts.cabbi.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.R;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.VolleyHelper;
import com.emts.cabbi.model.TripModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class TripDetailsActivity extends BookingBaseActivity {
    LinearLayout mainLayout;
    ProgressBar progressBar;
    TextView tvBottomInfo, tvErrorText;

    TextView tvTripDate, tvBookingNo, tvTripStatus, tvPickUpLocation, tvDropOffLocation, tvTripDistance,
            tvTripDuration, tvTripFare, tv1CabbiServiceCharge, tvOperatorServiceFee, tvOtherCharges, tvNetEarning;
    RelativeLayout layOperatorServiceFeeHolder;
    View holderEarning;

    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy MMM dd, HH:mm a");

    @Override
    protected void onResume() {
        currentActivity = this;
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentActivity = this;
    }

    @Override
    protected void onStop() {
//        currentActivity = null;
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_details);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Trip Details");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        String bookingNo = intent.getStringExtra("bookingId");

        init();

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            tripDetailsTask(bookingNo);
        } else {
            mainLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            tvBottomInfo.setVisibility(View.GONE);

            tvErrorText.setText(getResources().getString(R.string.no_internet));
            tvErrorText.setVisibility(View.VISIBLE);
        }
    }

    private void init() {
        layOperatorServiceFeeHolder = findViewById(R.id.lay_operator_service_fee_holder);

        mainLayout = findViewById(R.id.main_layout);
        progressBar = findViewById(R.id.progress_bar);
        tvErrorText = findViewById(R.id.tv_error_text);
        tvBottomInfo = findViewById(R.id.tv_info);

        tvTripDate = findViewById(R.id.tv_trip_date_time);
        tvBookingNo = findViewById(R.id.tv_booking_no);
        tvTripStatus = findViewById(R.id.tv_trip_status);
        tvPickUpLocation = findViewById(R.id.tv_pickup_location);
        tvDropOffLocation = findViewById(R.id.tv_drop_off_location);
        tvTripDistance = findViewById(R.id.tv_trip_distance);
        tvTripDuration = findViewById(R.id.tv_trip_time);
        holderEarning = findViewById(R.id.holderEarning);
        tvTripFare = findViewById(R.id.tv_trip_fare);
        tv1CabbiServiceCharge = findViewById(R.id.tv_1cabbi_service_charge);
        tvOperatorServiceFee = findViewById(R.id.tv_operator_service_fee);
        tvOtherCharges = findViewById(R.id.tv_other_charges);
        tvNetEarning = findViewById(R.id.tv_net_earning);
    }

    private void tripDetailsTask(String bookingID) {
        progressBar.setVisibility(View.VISIBLE);
        mainLayout.setVisibility(View.GONE);
        tvBottomInfo.setVisibility(View.GONE);
        tvErrorText.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("booking_id", bookingID);

        vHelper.addVolleyRequestListeners(Api.getInstance().tripDetailsApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONObject tripDetails = res.getJSONObject("trip_detail");

                                String tripDate = tripDetails.getString("trip_date_time");
                                try {
                                    Date date = sdf.parse(tripDate);
                                    tvTripDate.setText(sdf1.format(date));
                                } catch (ParseException e) {
                                    tvTripDate.setText(tripDate);
                                }

                                tvBookingNo.setText(tripDetails.getString("booking_no"));

                                String tripStatus = tripDetails.getString("trip_status");
                                if (tripStatus.equals(TripModel.TRIP_PENDING)) {
                                    tvTripStatus.setText(TripModel.PENDING_TEXT);
                                } else if (tripStatus.equals(TripModel.TRIP_SCHEDULED)) {
                                    tvTripStatus.setText(TripModel.SCHEDULED_TEXT);
                                } else if (tripStatus.equals(TripModel.TRIP_IN_PROGRESS)) {
                                    tvTripStatus.setText(TripModel.IN_PROGRESS_TEXT);
                                } else if (tripStatus.equals(TripModel.TRIP_COMPLETED)) {
                                    tvTripStatus.setText(TripModel.COMPLETED_TEXT);
                                } else if (tripStatus.equals(TripModel.TRIP_CANCELLED)) {
                                    tvTripStatus.setText(TripModel.CANCELLED_TEXT);
                                    tvTripStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.appRed));
                                    holderEarning.setVisibility(View.GONE);
                                }

                                tvPickUpLocation.setText(tripDetails.getString("pickup_location"));
                                tvDropOffLocation.setText(tripDetails.getString("dropoff_location"));

                                Double distance = Double.parseDouble(tripDetails.getString("distance"));
                                tvTripDistance.setText(String.format("%.2f", distance) + " " + "Km");

                                double timeInDouble = Double.parseDouble(tripDetails.getString("journey_time"));
                                int time = (int) timeInDouble;
                                if (time > 60) {
                                    int hour = time / 60;
                                    int minutes = time % 60;
                                    tvTripDuration.setText(hour + " " + "Hour" + " " + minutes + " " + "Minutes");
                                } else {
                                    tvTripDuration.setText(time + " " + "Minutes");
                                }

                                tvTripFare.setText("MYR" + " " + tripDetails.getString("trip_fare"));
                                tv1CabbiServiceCharge.setText("MYR" + " " + tripDetails.getString("cabbi_service_fee"));
                                tvOtherCharges.setText("MYR" + " " + tripDetails.getString("other_charges"));

                                double operatorFee = Double.parseDouble(tripDetails.getString("operator_service_fee"));
                                if (operatorFee > 0) {
                                    layOperatorServiceFeeHolder.setVisibility(View.VISIBLE);
                                    tvOperatorServiceFee.setText("MYR" + " " + String.format("%.2f", operatorFee));
                                } else {
                                    layOperatorServiceFeeHolder.setVisibility(View.GONE);
                                }
                                tvNetEarning.setText("MYR" + " " + tripDetails.getString("trip_income"));

                                mainLayout.setVisibility(View.VISIBLE);
                                tvBottomInfo.setVisibility(View.VISIBLE);
                                tvErrorText.setVisibility(View.GONE);
                            } else {
                                tvBottomInfo.setVisibility(View.GONE);
                                mainLayout.setVisibility(View.GONE);
                                tvErrorText.setText(res.getString("message"));
                                tvErrorText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Logger.e("tripDetailsTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("tripDetailsTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(TripDetailsActivity.this, "Error", errorMsg);
                        mainLayout.setVisibility(View.GONE);
                        tvBottomInfo.setVisibility(View.GONE);

                        tvErrorText.setText(errorMsg);
                        tvErrorText.setVisibility(View.VISIBLE);
                    }
                }, "tripDetailsTask");
    }

}
