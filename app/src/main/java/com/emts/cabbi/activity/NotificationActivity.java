package com.emts.cabbi.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.R;
import com.emts.cabbi.adapter.NotificationAdapter;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.VolleyHelper;
import com.emts.cabbi.model.NotificationModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class NotificationActivity extends BookingBaseActivity {
    ArrayList<NotificationModel> notificationLists;
    NotificationAdapter notificationAdapter;
    RecyclerView rvNotificationListings;
    ProgressBar progressBar, infiniteProgressBar;
    TextView tvErrorText;

    @Override
    protected void onResume() {
        currentActivity = this;
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentActivity = this;
    }

    @Override
    protected void onStop() {
//        currentActivity = null;
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Notification");

        progressBar = findViewById(R.id.progress_bar);
        infiniteProgressBar = findViewById(R.id.infinite_progress_bar);
        tvErrorText = findViewById(R.id.error_text);

        rvNotificationListings = findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvNotificationListings.setLayoutManager(linearLayoutManager);

        notificationLists = new ArrayList<>();

        notificationAdapter = new NotificationAdapter(NotificationActivity.this, notificationLists);
        notificationAdapter.setOnRecyclerViewItemClickListener(new NotificationAdapter.RecyclerItemClickListener() {
            @Override
            public void onRecyclerItemClickListener(int position) {
                NotificationModel not = notificationLists.get(position);
                if (!not.isSeen()) {
                    readTask(position, not.getNotificationId());
                }
                switch (not.getNotType()) {
                    case NotificationModel.NOT_TYPE_TRIP_CANCEL:
                        //go to my bookings
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        //not really accept booking but this will open mybooking fragment
                        intent.putExtra("acceptBooking", true);
                        startActivity(intent);
                        break;
                    case NotificationModel.NOT_TYPE_TRIP_COMPLETE:
                        Intent intent1 = new Intent(getApplicationContext(), TripDayActivity.class);
                        intent1.putExtra("tripDate", not.getNotificationDate());
                        startActivity(intent1);
                        break;
                    case NotificationModel.NOT_TYPE_UPDATE_APPROVE:
                        Intent intent2 = new Intent(getApplicationContext(), DriverProfileActivity.class);
                        startActivity(intent2);
                        break;
                    case NotificationModel.NOT_TYPE_UPDATE_REJECT:
                        Intent intent3 = new Intent(getApplicationContext(), HelpCenterActivity.class);
                        startActivity(intent3);
                        break;
                }
            }
        });
        rvNotificationListings.setAdapter(notificationAdapter);

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            notificationListingTask();
        } else {
            tvErrorText.setText(R.string.no_internet);
            tvErrorText.setVisibility(View.VISIBLE);
            rvNotificationListings.setVisibility(View.GONE);
        }
    }

    private void readTask(final int position, String notificationId) {
        if (!NetworkUtils.isInNetwork(getApplicationContext())) {
            return;
        }
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("notification_id", notificationId);
        postParams.put("action", "read");

        vHelper.addVolleyRequestListeners(Api.getInstance().notReadUnReadApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                notificationLists.get(position).setSeen(true);
                                try {
                                    notificationAdapter.notifyItemChanged(position);
                                } catch (Exception e) {
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("readTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("readTask error ex", e.getMessage() + " ");
                        }
                    }
                }, "readTask");


    }

    private void notificationListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().notificationApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray notArray = res.getJSONArray("all_notifications");
                                for (int i = 0; i < notArray.length(); i++) {
                                    JSONObject eachNot = notArray.getJSONObject(i);
                                    NotificationModel not = new NotificationModel();
                                    not.setNotificationBody(eachNot.getString("notification_msg"));
                                    not.setNotificationDate(eachNot.getString("date"));
                                    not.setNotificationId(eachNot.getString("id"));
                                    not.setSeen(eachNot.getString("status").equals("1"));
                                    not.setNotType(eachNot.getString("notification_code"));

                                    notificationLists.add(not);
                                }

                                notificationAdapter.notifyDataSetChanged();
                                rvNotificationListings.setVisibility(View.VISIBLE);
                                tvErrorText.setVisibility(View.GONE);
                            } else {
                                tvErrorText.setText(res.getString("message"));
                                tvErrorText.setVisibility(View.VISIBLE);
                                rvNotificationListings.setVisibility(View.GONE);
                            }
                            progressBar.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            Logger.e("notificationListingTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("notificationListingTask error ex", e.getMessage() + " ");
                        }
                        tvErrorText.setText(errorMsg);
                        tvErrorText.setVisibility(View.VISIBLE);
                        rvNotificationListings.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                    }
                }, "notificationListingTask");
    }

}
