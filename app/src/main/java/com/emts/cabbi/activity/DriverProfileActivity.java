package com.emts.cabbi.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.R;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.ImageHelper;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.PreferenceHelper;
import com.emts.cabbi.helper.Utils;
import com.emts.cabbi.helper.VolleyHelper;
import com.emts.cabbi.model.VehicleModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class DriverProfileActivity extends BookingBaseActivity {
    TextView tvDateJoined, tvUserName, tvFullName, tvIcNumber, tvHomeAddress, tvMobileNo, tvEmail, tvClass, tvServiceProvider;
    EditText etLicenseStartDate, etLicenseEndDate, etVehiclePlatePeriod, etYearRegistration, etVehicleColor;
    Spinner spVehicleModel;
    ImageView edtProfile, addProfilePic, addNric, addDrivingLicense, addInsuranceCertificate, addVocationalLicense, addVehiclePermit;
    Button btnSubmit;

    String profileImagePath = "";
    String nricImagePath = "";
    String licenseImagePath = "";
    String insuranceCertificateImagePath = "";
    String vocationalLicenseImagePath = "";
    String vehiclePermitImagePath = "";
    public static Uri profileFileUri, nricFileUri, licenseFileUri, insuranceFileUri, vocationFileUri, vehiclePermitUri;

    ArrayList<VehicleModel> vehicleModelLists;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;


    //camera capture request codes for all images
    public static final int CAMERA_CAPTURE_PROFILE_IMAGE_REQUEST_CODE = 6542;
    public static final int CAMERA_CAPTURE_NRIC_IMAGE_REQUEST_CODE = 6543;
    public static final int CAMERA_CAPTURE_LICENSE_IMAGE_REQUEST_CODE = 6544;
    public static final int CAMERA_CAPTURE_INSURANCE_IMAGE_REQUEST_CODE = 6545;
    public static final int CAMERA_CAPTURE_VOCATIONAL_IMAGE_REQUEST_CODE = 6546;
    public static final int CAMERA_CAPTURE_VEHICLE_PERMIT_REQUEST_CODE = 65467;

    //gallery load image request codes for all images
    public static final int RESULT_LOAD_IMAGE_PROFILE = 7653;
    public static final int RESULT_LOAD_IMAGE_NRIC = 7655;
    public static final int RESULT_LOAD_IMAGE_LICENSE = 7656;
    public static final int RESULT_LOAD_IMAGE_INSURANCE = 7657;
    public static final int RESULT_LOAD_IMAGE_VOCATIONAL = 7658;
    private static final int RESULT_LOAD_VEHICLE_PERMIT = 7659;

    PreferenceHelper prefsHelper;

    Calendar startDateCalendar, endDateCalendar, calendarNow;

    @Override
    protected void onResume() {
        currentActivity = this;
        super.onResume();
        Logger.e("fuck", "onResume");
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentActivity = this;
        Logger.e("fuck", "onStart");
    }

    @Override
    protected void onStop() {
//        currentActivity = null;
        super.onStop();
        VolleyHelper.getInstance(DriverProfileActivity.this).cancelRequest("editProfileTask");
        VolleyHelper.getInstance(DriverProfileActivity.this).cancelRequest("getProfileDetailsTask");
        Logger.e("fuck", "onStop");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onCreate(savedInstanceState);
        prefsHelper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_profile);
        calendarNow = Calendar.getInstance();
        vehicleModelLists = new ArrayList<>();

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("View Profile");

        init();

        etVehiclePlatePeriod.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                changeButtonState();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etYearRegistration.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                changeButtonState();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etVehicleColor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                changeButtonState();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        setEditEnabledDisabled(false);

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            getProfileDetailsTask();
        } else {
            AlertUtils.showSnack(DriverProfileActivity.this, btnSubmit, getResources().getString(R.string.no_internet));
        }

        edtProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEditEnabledDisabled(true);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        checkFromPush(getIntent());
    }

    private void init() {
        //textview
        tvDateJoined = findViewById(R.id.tv_joined_date);
        tvUserName = findViewById(R.id.tv_username);
        tvFullName = findViewById(R.id.tv_full_name);
        tvIcNumber = findViewById(R.id.tv_ic_number);
        tvHomeAddress = findViewById(R.id.tv_home_address);
        tvMobileNo = findViewById(R.id.tv_mobile_no);
        tvEmail = findViewById(R.id.tv_email_address);
        tvClass = findViewById(R.id.tv_class);
        tvServiceProvider = findViewById(R.id.tv_service_provider);

        //edittext
        etLicenseStartDate = findViewById(R.id.et_license_start_Date);
        etLicenseEndDate = findViewById(R.id.et_license_end_Date);

        startDateCalendar = Calendar.getInstance();
        endDateCalendar = Calendar.getInstance();

        pickStartEndDate();


        etVehiclePlatePeriod = findViewById(R.id.et_vehicle_plate_period);

        etYearRegistration = findViewById(R.id.et_year_registration);
        etVehicleColor = findViewById(R.id.et_vehicle_color);

        //spinner
        spVehicleModel = findViewById(R.id.sp_vehicle_model);

        //imageview
        edtProfile = findViewById(R.id.edt_profile);
        addProfilePic = findViewById(R.id.btn_add_profile_pic);
        addNric = findViewById(R.id.btn_add_nric);
        addDrivingLicense = findViewById(R.id.btn_add_driving_license);
        addInsuranceCertificate = findViewById(R.id.btn_add_insurance_certificate);
        addVocationalLicense = findViewById(R.id.btn_add_vocational_license);
        addVehiclePermit = findViewById(R.id.btn_add_vehicle_permit);

        //button
        btnSubmit = findViewById(R.id.btn_submit);
        btnSubmit.setVisibility(View.GONE);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    editProfileTask();
                } else {
                    AlertUtils.showSnack(DriverProfileActivity.this, etLicenseEndDate, getResources().getString(R.string.no_internet));
                }
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void pickStartEndDate() {
        final DatePickerDialog.OnDateSetListener startDate = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                startDateCalendar.set(Calendar.YEAR, year);
                startDateCalendar.set(Calendar.MONTH, month);
                startDateCalendar.set(Calendar.DAY_OF_MONTH, day);
                etLicenseStartDate.setText(year + "-" + String.format("%02d", (month + 1)) + "-" + String.format("%02d", day));
            }
        };

        etLicenseStartDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(DriverProfileActivity.this, startDate, startDateCalendar
                            .get(Calendar.YEAR), startDateCalendar.get(Calendar.MONTH),
                            startDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
                return true;
            }
        });

        final DatePickerDialog.OnDateSetListener expiryDate = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                endDateCalendar.set(Calendar.YEAR, year);
                endDateCalendar.set(Calendar.YEAR, month);
                endDateCalendar.set(Calendar.YEAR, day);
                etLicenseEndDate.setText(year + "-" + String.format("%02d", (month + 1)) + "-" + String.format("%02d", day));

            }
        };

        etLicenseEndDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    new DatePickerDialog(DriverProfileActivity.this, expiryDate, endDateCalendar
                            .get(Calendar.YEAR), endDateCalendar.get(Calendar.MONTH),
                            endDateCalendar.get(Calendar.DAY_OF_MONTH)).show();
                }
                return true;
            }
        });


    }

    private void setEditEnabledDisabled(boolean isEdit) {
        if (isEdit) {
            edtProfile.setVisibility(View.GONE);
            etLicenseStartDate.setEnabled(true);
            etLicenseEndDate.setEnabled(true);
            etVehiclePlatePeriod.setEnabled(true);
            etYearRegistration.setEnabled(true);
            etVehicleColor.setEnabled(true);

            spVehicleModel.setEnabled(true);

            //pick image click listener
            pickImageClickListener(true);

            btnSubmit.setEnabled(true);
            btnSubmit.setVisibility(View.VISIBLE);
        } else {
            etLicenseStartDate.setEnabled(false);
            etLicenseEndDate.setEnabled(false);
            etVehiclePlatePeriod.setEnabled(false);
            etYearRegistration.setEnabled(false);
            etVehicleColor.setEnabled(false);

            spVehicleModel.setEnabled(false);

            //pick image listener
            pickImageClickListener(false);
        }
    }

    private void getProfileDetailsTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(DriverProfileActivity.this, "Please wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().viewProfileApi, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        JSONObject userDetails = res.getJSONObject("driver_info");
                        tvDateJoined.setText(userDetails.getString("joined_date"));
                        tvUserName.setText(userDetails.getString("user_name"));
                        tvFullName.setText(userDetails.getString("full_name"));
                        tvIcNumber.setText(userDetails.getString("lc_no"));
                        tvHomeAddress.setText(userDetails.getString("home_address"));
                        tvMobileNo.setText(userDetails.getString("mobile_no"));
                        tvEmail.setText(userDetails.getString("email"));
                        tvClass.setText(userDetails.getString("class"));

                        String startDate = userDetails.getString("lc_valid_from");
                        etLicenseStartDate.setText(startDate);
                        String endDate = userDetails.getString("lc_valid_to");
                        etLicenseEndDate.setText(endDate);

                        Date sD, eD;
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                        try {
                            sD = dateFormat.parse(startDate);
                            startDateCalendar.setTime(sD);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        try {
                            eD = dateFormat.parse(endDate);
                            endDateCalendar.setTime(eD);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        etVehiclePlatePeriod.setText(userDetails.getString("plate_no"));

                        etYearRegistration.setText(userDetails.getString("year_of_reg"));
                        etVehicleColor.setText(userDetails.getString("vehicle_color"));

                        tvServiceProvider.setText(userDetails.getString("org_name"));

                        //image directory
                        String profileImgDirectory = userDetails.getString("profile_img_directory");
                        String nricImgDirectory = userDetails.getString("nric_img_directory");
                        String drivingLicenseDirectory = userDetails.getString("driving_license_img_directory");
                        String insuranceImgDirectory = userDetails.getString("vehicle_insurance_img_directory");
                        String vocationalImgDirectory = userDetails.getString("vocational_img_directory");
                        String vehiclePermitImgDir = userDetails.optString("vehicle_permit_dir");

                        //images
                        String profileImg = userDetails.getString("profile_photo");
                        String nricImg = userDetails.getString("nric_photo");
                        String lincenseImg = userDetails.getString("lc_photo");
                        String insuranceImg = userDetails.getString("insurance_photo");
                        String vocationalLicenseImg = userDetails.getString("vocational_lc");
                        String vehiclePermit = userDetails.optString("vehicle_permit");

                        if (!TextUtils.isEmpty(profileImg) && !profileImg.equals("null")) {
                            Glide.with(DriverProfileActivity.this)
                                    .load(profileImgDirectory + profileImg)
                                    .apply(RequestOptions.circleCropTransform())
                                    .into(addProfilePic);
                            profileImagePath = profileImgDirectory + profileImg;
                        } else {
                            addProfilePic.setImageResource(R.drawable.btn_add_profile_pic);
                            profileImagePath = "";
                        }
                        if (!TextUtils.isEmpty(nricImg) && !nricImg.equals("null")) {
                            Glide.with(DriverProfileActivity.this).load(nricImgDirectory + nricImg).into(addNric);
                            nricImagePath = nricImgDirectory + nricImg;
                        } else {
                            addNric.setImageResource(R.drawable.btn_add_nric);
                            nricImagePath = "";
                        }
                        if (!TextUtils.isEmpty(lincenseImg) && !lincenseImg.equals("null")) {
                            Glide.with(DriverProfileActivity.this).load(drivingLicenseDirectory + lincenseImg).into(addDrivingLicense);
                            licenseImagePath = drivingLicenseDirectory + lincenseImg;
                        } else {
                            addDrivingLicense.setImageResource(R.drawable.btn_add_driving_license);
                            licenseImagePath = "";
                        }
                        if (!TextUtils.isEmpty(insuranceImg) && !insuranceImg.equals("null")) {
                            Glide.with(DriverProfileActivity.this).load(insuranceImgDirectory + insuranceImg).into(addInsuranceCertificate);
                            insuranceCertificateImagePath = insuranceImgDirectory + insuranceImg;
                        } else {
                            addInsuranceCertificate.setImageResource(R.drawable.btn_add_insurance_certificate);
                            insuranceCertificateImagePath = "";
                        }
                        if (!TextUtils.isEmpty(vocationalLicenseImg) && !vocationalLicenseImg.equals("null")) {
                            Glide.with(DriverProfileActivity.this).load(vocationalImgDirectory + vocationalLicenseImg).into(addVocationalLicense);
                            vocationalLicenseImagePath = vocationalImgDirectory + vocationalLicenseImg;
                        } else {
                            addVocationalLicense.setImageResource(R.drawable.btn_add_driving_license);
                            vocationalLicenseImagePath = "";
                        }
                        if (!TextUtils.isEmpty(vehiclePermit) && !vehiclePermit.equals("null")) {
                            Glide.with(DriverProfileActivity.this).load(vehiclePermitImgDir + vehiclePermit).into(addVehiclePermit);
                            vehiclePermitImagePath = vehiclePermitImgDir + vehiclePermit;
                        } else {
                            addVehiclePermit.setImageResource(R.drawable.btn_add_insurance_certificate);
                            vehiclePermitImagePath = "";
                        }

                        JSONArray vehicleModelArray = res.getJSONArray("car_models");
                        int position = 0;
                        for (int i = 0; i < vehicleModelArray.length(); i++) {
                            JSONObject eachModel = vehicleModelArray.getJSONObject(i);
                            VehicleModel vehicleModel = new VehicleModel();
                            vehicleModel.setId(eachModel.getString("id"));
                            vehicleModel.setModelName(eachModel.getString("model_name"));
                            vehicleModelLists.add(vehicleModel);

                            if (vehicleModel.getId().equals(userDetails.getString("vehicle_type"))) {
                                position = i;
                            }
                        }

                        ArrayAdapter<VehicleModel> spinnerAdapter = new ArrayAdapter<>(DriverProfileActivity.this, R.layout.sp_layout_textview, vehicleModelLists);
                        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spVehicleModel.setAdapter(spinnerAdapter);
                        spVehicleModel.setSelection(position);
                    }
                } catch (JSONException e) {
                    Logger.e("getProfileDetailsTask json ex", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                pDialog.dismiss();
                String errorMsg = "Unexpected Error, Please try again with internet access!";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (JSONException e) {
                    Logger.e("getProfileDetailsTask error ex", e.getMessage() + " ");
                }
                AlertUtils.simpleAlert(DriverProfileActivity.this, "Error", errorMsg);
            }
        }, "getProfileDetailsTask");
    }

    private void editProfileTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(DriverProfileActivity.this, "Please wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("valid_from_date", etLicenseStartDate.getText().toString().trim());
        postParams.put("valid_to_date", etLicenseEndDate.getText().toString().trim());
        postParams.put("vehicle_plate_no", etVehiclePlatePeriod.getText().toString().trim());
        postParams.put("vehicle_model", vehicleModelLists.get(spVehicleModel.getSelectedItemPosition()).getId());
        postParams.put("year_of_registration", etYearRegistration.getText().toString().trim());
        postParams.put("vehicle_color", etVehicleColor.getText().toString().trim());


        HashMap<String, String> fileParams = new HashMap<>();
        if (!TextUtils.isEmpty(profileImagePath) && !profileImagePath.contains("http")) {
            fileParams.put("profile_photo", profileImagePath);
        }
        if (!TextUtils.isEmpty(nricImagePath) && !nricImagePath.contains("http")) {
            fileParams.put("nric_photo", nricImagePath);
        }
        if (!TextUtils.isEmpty(licenseImagePath) && !licenseImagePath.contains("http")) {
            fileParams.put("driving_license_photo", licenseImagePath);
        }
        if (!TextUtils.isEmpty(insuranceCertificateImagePath) && !insuranceCertificateImagePath.contains("http")) {
            fileParams.put("vehicle_insurance", insuranceCertificateImagePath);
        }
        if (!TextUtils.isEmpty(vocationalLicenseImagePath) && !vocationalLicenseImagePath.contains("http")) {
            fileParams.put("vocational_license", vocationalLicenseImagePath);
        }
        if (!TextUtils.isEmpty(vehiclePermitImagePath) && !vehiclePermitImagePath.contains("http")) {
            fileParams.put("vehicle_permit", vehiclePermitImagePath);
        }

        vHelper.addMultipartRequest(Api.getInstance().editProfileApi, Request.Method.POST, postParams,
                fileParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            final JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.simpleAlert(DriverProfileActivity.this, false, "Success",
                                        res.getString("message"),
                                        "OK", "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                if (isPositiveButton) {
                                                    prefsHelper.edit().putString(PreferenceHelper.USER_VEHICLE_PLATE_NO, etVehiclePlatePeriod.getText().toString().trim()).apply();
                                                    try {
                                                        String profilePic = res.getString("profile_img_directory") + res.getString("profile_photo");
                                                        prefsHelper.edit().putString(PreferenceHelper.USER_IMG, profilePic).apply();
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
//PreferenceHelper.getInstance(DriverProfileActivity.this).edit().putBoolean(PreferenceHelper.IS_LOGIN, false).apply();
//                                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
//                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                                            startActivity(intent);
                                                }
                                            }
                                        });

                                btnSubmit.setEnabled(false);
                                btnSubmit.setVisibility(View.GONE);
                                edtProfile.setVisibility(View.VISIBLE);
                                btnSubmit.setBackgroundColor(getResources().getColor(R.color.textDisable));
                            } else {
                                AlertUtils.simpleAlert(DriverProfileActivity.this, "Error", res.getString("message"));
                                btnSubmit.setVisibility(View.VISIBLE);
                                edtProfile.setVisibility(View.GONE);
                                btnSubmit.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                btnSubmit.setEnabled(true);
                            }
                        } catch (JSONException e) {
                            Logger.e("editProfileTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("editProfileTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(DriverProfileActivity.this, "Error", errorMsg);
                    }
                }, "editProfileTask");


    }

    private void pickImageClickListener(boolean isSet) {
        if (!isSet) {
            addProfilePic.setClickable(false);
            addNric.setClickable(false);
            addDrivingLicense.setClickable(false);
            addVocationalLicense.setClickable(false);
            addInsuranceCertificate.setClickable(false);
            addVehiclePermit.setClickable(false);
            return;
        }

        addProfilePic.setClickable(true);
        addProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageChooseDialog(view.getId());
            }
        });

        addNric.setClickable(true);
        addNric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageChooseDialog(view.getId());
            }
        });

        addDrivingLicense.setClickable(true);
        addDrivingLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageChooseDialog(view.getId());
            }
        });

        addInsuranceCertificate.setClickable(true);
        addInsuranceCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageChooseDialog(view.getId());
            }
        });

        addVocationalLicense.setClickable(true);
        addVocationalLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageChooseDialog(view.getId());
            }
        });

        addVehiclePermit.setClickable(true);
        addVehiclePermit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageChooseDialog(view.getId());
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RESULT_LOAD_IMAGE_PROFILE || requestCode == RESULT_LOAD_IMAGE_NRIC ||
                    requestCode == RESULT_LOAD_IMAGE_LICENSE || requestCode == RESULT_LOAD_IMAGE_INSURANCE ||
                    requestCode == RESULT_LOAD_IMAGE_VOCATIONAL || requestCode == RESULT_LOAD_VEHICLE_PERMIT) {

                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                assert selectedImageUri != null;
                CursorLoader cursorLoader = new CursorLoader(DriverProfileActivity.this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                assert cursor != null;
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selImgPath = cursor.getString(column_index);
                Logger.e("imagePath loaded gallary", selImgPath);

                Bitmap bm = ImageHelper.decodeImageFileWithCompression(selImgPath,
                        (int) Utils.pxFromDp(getApplicationContext(), 85f), (int) Utils.pxFromDp(getApplicationContext(), 85f));
                if (requestCode == RESULT_LOAD_IMAGE_PROFILE) {
//                    addProfilePic.setImageBitmap(bm);
                    Glide.with(getApplicationContext()).load(bm)
                            .apply(RequestOptions.circleCropTransform())
                            .into(addProfilePic);
                    profileImagePath = selImgPath;
                    ImageHelper.saveBitMapToFile(profileImagePath, bm);
                } else if (requestCode == RESULT_LOAD_IMAGE_NRIC) {
                    addNric.setImageBitmap(bm);
                    nricImagePath = selImgPath;
                    ImageHelper.saveBitMapToFile(nricImagePath, bm);
                } else if (requestCode == RESULT_LOAD_IMAGE_LICENSE) {
                    addDrivingLicense.setImageBitmap(bm);
                    licenseImagePath = selImgPath;
                    ImageHelper.saveBitMapToFile(licenseImagePath, bm);
                } else if (requestCode == RESULT_LOAD_IMAGE_INSURANCE) {
                    addInsuranceCertificate.setImageBitmap(bm);
                    insuranceCertificateImagePath = selImgPath;
                    ImageHelper.saveBitMapToFile(insuranceCertificateImagePath, bm);
                } else if (requestCode == RESULT_LOAD_IMAGE_VOCATIONAL) {
                    addVocationalLicense.setImageBitmap(bm);
                    vocationalLicenseImagePath = selImgPath;
                    ImageHelper.saveBitMapToFile(vocationalLicenseImagePath, bm);
                } else if (requestCode == RESULT_LOAD_VEHICLE_PERMIT) {
                    addVehiclePermit.setImageBitmap(bm);
                    vehiclePermitImagePath = selImgPath;
                    ImageHelper.saveBitMapToFile(vehiclePermitImagePath, bm);
                }

            } else if (requestCode == CAMERA_CAPTURE_PROFILE_IMAGE_REQUEST_CODE || requestCode == CAMERA_CAPTURE_NRIC_IMAGE_REQUEST_CODE
                    || requestCode == CAMERA_CAPTURE_LICENSE_IMAGE_REQUEST_CODE || requestCode == CAMERA_CAPTURE_INSURANCE_IMAGE_REQUEST_CODE
                    || requestCode == CAMERA_CAPTURE_VOCATIONAL_IMAGE_REQUEST_CODE || requestCode == CAMERA_CAPTURE_VEHICLE_PERMIT_REQUEST_CODE) {
                if (requestCode == CAMERA_CAPTURE_PROFILE_IMAGE_REQUEST_CODE) {
                    Bitmap bm = ImageHelper.decodeImageFileWithCompression(profileFileUri.getPath(), 300, 300);
                    ImageHelper.saveBitMapToFile(profileFileUri.getPath(), bm);
//                    addProfilePic.setImageBitmap(bm);
                    Glide.with(getApplicationContext()).load(bm)
                            .apply(RequestOptions.circleCropTransform())
                            .into(addProfilePic);
                    profileImagePath = profileFileUri.getPath();
                    ImageHelper.saveBitMapToFile(profileImagePath, bm);
                } else if (requestCode == CAMERA_CAPTURE_NRIC_IMAGE_REQUEST_CODE) {
                    Bitmap bm = ImageHelper.decodeImageFileWithCompression(nricFileUri.getPath(), 300, 300);
                    ImageHelper.saveBitMapToFile(nricFileUri.getPath(), bm);
                    addNric.setImageBitmap(bm);
                    nricImagePath = nricFileUri.getPath();
                    ImageHelper.saveBitMapToFile(nricImagePath, bm);
                } else if (requestCode == CAMERA_CAPTURE_LICENSE_IMAGE_REQUEST_CODE) {
                    Bitmap bm = ImageHelper.decodeImageFileWithCompression(licenseFileUri.getPath(), 300, 300);
                    ImageHelper.saveBitMapToFile(licenseFileUri.getPath(), bm);
                    addDrivingLicense.setImageBitmap(bm);
                    licenseImagePath = licenseFileUri.getPath();
                    ImageHelper.saveBitMapToFile(licenseImagePath, bm);
                } else if (requestCode == CAMERA_CAPTURE_INSURANCE_IMAGE_REQUEST_CODE) {
                    Bitmap bm = ImageHelper.decodeImageFileWithCompression(insuranceFileUri.getPath(), 300, 300);
                    ImageHelper.saveBitMapToFile(insuranceFileUri.getPath(), bm);
                    addInsuranceCertificate.setImageBitmap(bm);
                    insuranceCertificateImagePath = insuranceFileUri.getPath();
                    ImageHelper.saveBitMapToFile(insuranceCertificateImagePath, bm);
                } else if (requestCode == CAMERA_CAPTURE_VOCATIONAL_IMAGE_REQUEST_CODE) {
                    Bitmap bm = ImageHelper.decodeImageFileWithCompression(vocationFileUri.getPath(), 300, 300);
                    ImageHelper.saveBitMapToFile(vocationFileUri.getPath(), bm);
                    addVocationalLicense.setImageBitmap(bm);
                    vocationalLicenseImagePath = vocationFileUri.getPath();
                    ImageHelper.saveBitMapToFile(vocationalLicenseImagePath, bm);
                } else if (requestCode == CAMERA_CAPTURE_VEHICLE_PERMIT_REQUEST_CODE) {
                    Bitmap bm = ImageHelper.decodeImageFileWithCompression(vehiclePermitUri.getPath(), 300, 300);
                    ImageHelper.saveBitMapToFile(vehiclePermitUri.getPath(), bm);
                    addVehiclePermit.setImageBitmap(bm);
                    vehiclePermitImagePath = vehiclePermitUri.getPath();
                    ImageHelper.saveBitMapToFile(vehiclePermitImagePath, bm);
                }
            }
            changeButtonState();
        }
    }

    private boolean validation() {
        boolean isValid;
        isValid = !TextUtils.isEmpty(etVehiclePlatePeriod.getText().toString().trim()) &&
                !TextUtils.isEmpty(etYearRegistration.getText().toString().trim()) &&
                !TextUtils.isEmpty(etVehicleColor.getText().toString().trim()) &&
                !TextUtils.isEmpty(profileImagePath) && !TextUtils.isEmpty(nricImagePath) && !TextUtils.isEmpty(licenseImagePath) &&
                !TextUtils.isEmpty(insuranceCertificateImagePath) && !TextUtils.isEmpty(vocationalLicenseImagePath)
                && !TextUtils.isEmpty(vehiclePermitImagePath);
        return isValid;
    }

    private void changeButtonState() {
        if (validation()) {
            btnSubmit.setEnabled(true);
            btnSubmit.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        } else {
            btnSubmit.setEnabled(false);
            btnSubmit.setBackgroundColor(getResources().getColor(R.color.textDisable));
        }
    }

    private void showImageChooseDialog(final int imagViewID) {
        final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.choose_photo), getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(DriverProfileActivity.this);
        builder.setTitle(R.string.choose_media);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getString(R.string.take_photo))) {
                    int imageRequestCode = 0;
                    if (imagViewID == R.id.btn_add_profile_pic) {
                        imageRequestCode = CAMERA_CAPTURE_PROFILE_IMAGE_REQUEST_CODE;
                    } else if (imagViewID == R.id.btn_add_nric) {
                        imageRequestCode = CAMERA_CAPTURE_NRIC_IMAGE_REQUEST_CODE;
                    } else if (imagViewID == R.id.btn_add_driving_license) {
                        imageRequestCode = CAMERA_CAPTURE_LICENSE_IMAGE_REQUEST_CODE;
                    } else if (imagViewID == R.id.btn_add_insurance_certificate) {
                        imageRequestCode = CAMERA_CAPTURE_INSURANCE_IMAGE_REQUEST_CODE;
                    } else if (imagViewID == R.id.btn_add_vocational_license) {
                        imageRequestCode = CAMERA_CAPTURE_VOCATIONAL_IMAGE_REQUEST_CODE;
                    } else if (imagViewID == R.id.btn_add_vehicle_permit) {
                        imageRequestCode = CAMERA_CAPTURE_VEHICLE_PERMIT_REQUEST_CODE;
                    }
                    if (ContextCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(DriverProfileActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                imageRequestCode);

                    } else {
                        takePhoto(imageRequestCode);
                    }
                } else if (options[item].equals(getString(R.string.choose_photo))) {
                    int imageRequestCode = 0;
                    if (imagViewID == R.id.btn_add_profile_pic) {
                        imageRequestCode = RESULT_LOAD_IMAGE_PROFILE;
                    } else if (imagViewID == R.id.btn_add_nric) {
                        imageRequestCode = RESULT_LOAD_IMAGE_NRIC;
                    } else if (imagViewID == R.id.btn_add_driving_license) {
                        imageRequestCode = RESULT_LOAD_IMAGE_LICENSE;
                    } else if (imagViewID == R.id.btn_add_insurance_certificate) {
                        imageRequestCode = RESULT_LOAD_IMAGE_INSURANCE;
                    } else if (imagViewID == R.id.btn_add_vocational_license) {
                        imageRequestCode = RESULT_LOAD_IMAGE_VOCATIONAL;
                    } else if (imagViewID == R.id.btn_add_vehicle_permit) {
                        imageRequestCode = RESULT_LOAD_VEHICLE_PERMIT;
                    }
                    if (ContextCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(DriverProfileActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                imageRequestCode);

                    } else {
                        chooseFrmGallery(imageRequestCode);
                    }
                } else if (options[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void chooseFrmGallery(int requestImageCode) {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, requestImageCode);
    }

    public void takePhoto(int imageRequestCode) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        // start the image capture Intent
        if (imageRequestCode == CAMERA_CAPTURE_PROFILE_IMAGE_REQUEST_CODE) {
            profileFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, profileFileUri);
        } else if (imageRequestCode == CAMERA_CAPTURE_NRIC_IMAGE_REQUEST_CODE) {
            nricFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, nricFileUri);
        } else if (imageRequestCode == CAMERA_CAPTURE_LICENSE_IMAGE_REQUEST_CODE) {
            licenseFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, licenseFileUri);
        } else if (imageRequestCode == CAMERA_CAPTURE_INSURANCE_IMAGE_REQUEST_CODE) {
            insuranceFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, insuranceFileUri);
        } else if (imageRequestCode == CAMERA_CAPTURE_VOCATIONAL_IMAGE_REQUEST_CODE) {
            vocationFileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, vocationFileUri);
        } else if (imageRequestCode == CAMERA_CAPTURE_VEHICLE_PERMIT_REQUEST_CODE) {
            vehiclePermitUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, vehiclePermitUri);
        }

        startActivityForResult(intent, imageRequestCode);

    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));

//        Uri photoURI = FileProvider.getUriForFile(getActivity(),
//                BuildConfig.APPLICATION_ID + ".provider",
//                getOutputMediaFile(type));
//        Uri photoURI = FileProvider.getUriForFile(getActivity(),
//                getApplicationContext().getPackageName() + ".provider", getOutputMediaFile(type));
//        return photoURI;
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile(int type) {
        // External sdcard location
//        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
//                "Alacer");
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
                "file_1cabbi");
//        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath());

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Logger.e("getOutputMediaFile", "Oops! Failed create "
                        + "file_1cabbi" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".png");
        } else {
            return null;
        }
        return mediaFile;
    }

}

