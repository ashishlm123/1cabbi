package com.emts.cabbi.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.R;
import com.emts.cabbi.adapter.CashOutHistoryAdapter;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.PreferenceHelper;
import com.emts.cabbi.helper.VolleyHelper;
import com.emts.cabbi.model.CashOutHistoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CashOutHistoryActivity extends BookingBaseActivity {

    PreferenceHelper prefsHelper;
    ArrayList<CashOutHistoryModel> cashOutHistoryLists;
    CashOutHistoryAdapter cashOutHistoryAdapter;
    RecyclerView rvCashOutHistoryListings;
    ProgressBar progressBar, infiniteProgressBar;
    TextView tvErrorText;

    @Override
    protected void onResume() {
        currentActivity = this;
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentActivity = this;
    }

    @Override
    protected void onStop() {
//        currentActivity = null;
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cash_out_history);

        prefsHelper = PreferenceHelper.getInstance(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Cash Out History");

        progressBar = findViewById(R.id.progress_bar);
        infiniteProgressBar = findViewById(R.id.infinite_progress_bar);
        tvErrorText = findViewById(R.id.error_text);

        rvCashOutHistoryListings = findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvCashOutHistoryListings.setLayoutManager(linearLayoutManager);

        cashOutHistoryLists = new ArrayList<>();
        cashOutHistoryAdapter = new CashOutHistoryAdapter(CashOutHistoryActivity.this, cashOutHistoryLists);
        rvCashOutHistoryListings.setAdapter(cashOutHistoryAdapter);

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            cashOutHistoryListingTask();
        } else {
            rvCashOutHistoryListings.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            tvErrorText.setText(R.string.no_internet);
            tvErrorText.setVisibility(View.VISIBLE);
        }
    }

    private void cashOutHistoryListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        tvErrorText.setVisibility(View.GONE);
        rvCashOutHistoryListings.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().cashOutHistory, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {

                                JSONArray historyArray = res.getJSONArray("cashout_history");
                                for (int i = 0; i<historyArray.length(); i++){
                                    JSONObject eachHistory = historyArray.getJSONObject(i);
                                    CashOutHistoryModel cashOutHistoryModel = new CashOutHistoryModel();
                                    cashOutHistoryModel.setCashOutAmt(eachHistory.getString("amount"));
                                    cashOutHistoryModel.setCashOutDate(eachHistory.getString("transaction_date"));
                                    cashOutHistoryModel.setCashOutRequestId(eachHistory.getString("invoice_id"));
                                    cashOutHistoryModel.setCashOutStatus(eachHistory.getString("status"));

                                    cashOutHistoryLists.add(cashOutHistoryModel);
                                }

                                cashOutHistoryAdapter.notifyDataSetChanged();
                                rvCashOutHistoryListings.setVisibility(View.VISIBLE);
                                tvErrorText.setVisibility(View.GONE);
                            } else {
                                tvErrorText.setText(res.getString("message"));
                                tvErrorText.setVisibility(View.VISIBLE);
                                rvCashOutHistoryListings.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("cashOutHistoryListingTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("cashOutHistoryListingTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(CashOutHistoryActivity.this, "Error !!!", errorMsg);
                        tvErrorText.setText(errorMsg);
                        tvErrorText.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        rvCashOutHistoryListings.setVisibility(View.GONE);
                    }
                }, "cashOutHistoryListingTask");
    }

}
