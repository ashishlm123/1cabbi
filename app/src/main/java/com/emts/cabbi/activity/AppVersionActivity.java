package com.emts.cabbi.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.BuildConfig;
import com.emts.cabbi.R;

public class AppVersionActivity extends BookingBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_versionactivity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("App Version");

        TextView tvAppVersion = findViewById(R.id.tv_current_version);
        tvAppVersion.setText(BuildConfig.VERSION_NAME);

    }

    @Override
    protected void onStart() {
        super.onStart();
        currentActivity = this;
    }

    @Override
    protected void onStop() {
//        currentActivity = null;
        super.onStop();
    }

    @Override
    protected void onResume() {
        currentActivity = this;
        super.onResume();
    }
}
