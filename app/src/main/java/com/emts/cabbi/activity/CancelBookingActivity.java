package com.emts.cabbi.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.R;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.VolleyHelper;
import com.emts.cabbi.model.Booking;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class CancelBookingActivity extends BookingBaseActivity {
//    boolean onGoingRequest;
//    Handler mHandler;


    @Override
    protected void onResume() {
        currentActivity = this;
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentActivity = this;
    }

    @Override
    protected void onStop() {
//        currentActivity = null;
        super.onStop();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel_booking);

        ImageView closeIcon = findViewById(R.id.close);
        closeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        Intent intent = getIntent();
        if (intent == null) {
            onBackPressed();
            return;
        }
        final Booking booking = (Booking) intent.getSerializableExtra("booking");
        if (booking == null) {
            onBackPressed();
            return;
        }

        final EditText etRejectReason = findViewById(R.id.et_reason);
        final Button btnSubmit = findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etRejectReason.getText().toString().trim())) {
                    AlertUtils.showSnack(CancelBookingActivity.this, btnSubmit, "Enter your reason first");
                } else {
                    rejectJobTask(booking.getBookingNo(), etRejectReason.getText().toString().trim());
                }


            }
        });

        //start timer: on 30 second go back
//        mHandler = new Handler();
//        mHandler.postDelayed(runnable, 34000);
    }

//    Runnable runnable = new Runnable() {
//        @Override
//        public void run() {
//            if (!onGoingRequest) {
//                onBackPressed();
//            } else {
//                onGoingRequest = false;
//                //give him a chance
//                mHandler.postDelayed(runnable, 30000);
//            }
//        }
//    };

    @Override
    public void onBackPressed() {
        try {
            VolleyHelper.getInstance(this).cancelRequest("rejectJobTask");
//            mHandler.removeCallbacks(runnable);
        } catch (Exception e) {
            Logger.e("rejectJobTask onBackPressed Ex", e.getMessage());
        }
        super.onBackPressed();
    }

    private void rejectJobTask(String bookingId, String reason) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(CancelBookingActivity.this, "Please wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(CancelBookingActivity.this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("booking_id", bookingId);
        postParams.put("cancel_reason", reason);

        vHelper.addVolleyRequestListeners(Api.getInstance().cancelBooking, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
//                onGoingRequest = false;
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                emitJobAccept(response);
                                AlertUtils.simpleAlert(CancelBookingActivity.this, "Booking Cancelled",
                                        resObj.getString("message"), "OK", "",
                                        new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
//                                                CancelBookingActivity.this.finish();
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                            }
                                        });
                                setResult(RESULT_OK);
                            } else {
                                AlertUtils.simpleAlert(CancelBookingActivity.this, "Error",
                                        resObj.getString("message"), "OK", "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("rejectJobTask res ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
//                onGoingRequest = false;
                        pDialog.dismiss();
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            AlertUtils.simpleAlert(CancelBookingActivity.this, "Error",
                                    errorObj.getString("message"),
                                    "OK", "", null);
                        } catch (JSONException e) {
                            Logger.e("rejectJobTask error ex", e.getMessage() + " ");
                        }
                    }
                }, "rejectJobTask");
//        onGoingRequest = true;
    }

}
