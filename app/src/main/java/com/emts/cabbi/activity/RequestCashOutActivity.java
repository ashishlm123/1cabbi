package com.emts.cabbi.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.R;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RequestCashOutActivity extends BookingBaseActivity {
    EditText edtWithdrawlAmt;

    @Override
    protected void onResume() {
        currentActivity = this;
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentActivity = this;
    }

    @Override
    protected void onStop() {
//        currentActivity = null;
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_request_cash_out);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Request Cash Out");

        TextView tvBankName, tvAccountNo, tvRecipientName;
        final Button btnSubmit;

        tvBankName = findViewById(R.id.tv_bank_name);
        tvAccountNo = findViewById(R.id.tv_account_no);
        tvRecipientName = findViewById(R.id.tv_recipient_name);

        try {
            JSONObject bankRes = new JSONObject(getIntent().getStringExtra("bankInfo"));
            tvBankName.setText(bankRes.getString("bank_name"));
            tvAccountNo.setText(bankRes.getString("ac_no"));
            tvRecipientName.setText(bankRes.getString("recipient_name"));
        } catch (Exception e) {
            Logger.e("RequestCashOutActivity bankInfo ex", e.getMessage() + " ");
        }

        edtWithdrawlAmt = findViewById(R.id.edt_withdrawl_amt);

        btnSubmit = findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    double inputAmount = Double.parseDouble(edtWithdrawlAmt.getText().toString());
                    if (inputAmount > 0) {
                        submitWithDrawlRequestTask(inputAmount);
                    } else {
                        AlertUtils.showSnack(RequestCashOutActivity.this, btnSubmit,
                                "Please input amount more than zero");
                    }
                } else {
                    AlertUtils.showSnack(RequestCashOutActivity.this, btnSubmit, getResources().getString(R.string.no_internet));
                }
            }
        });
    }

    private void submitWithDrawlRequestTask(final double amount) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(RequestCashOutActivity.this,
                "Please wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("amount", String.valueOf(amount));

        vHelper.addVolleyRequestListeners(Api.getInstance().requestCashOut, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            String title = "Error !!!";
                            if (res.getBoolean("status")) {
                                title = "Request Success ";
                                edtWithdrawlAmt.setText("");
                                Intent intent = new Intent();
                                intent.putExtra("amtWithdraw", amount);
                                setResult(RESULT_OK, intent);
                            }
                            AlertUtils.simpleAlert(RequestCashOutActivity.this, title, res.getString("message"),
                                    "OK", "", null);
                        } catch (JSONException e) {
                            Logger.e("submitWithdrawlRequestTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("submitWithdrawlRequestTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(RequestCashOutActivity.this, "Error",
                                errorMsg, "OK", "", null);
                    }
                }, "submitWithdrawlRequestTask");
    }

}
