package com.emts.cabbi.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.emts.cabbi.BookingParser;
import com.emts.cabbi.CabbiBaseActivity;
import com.emts.cabbi.R;
import com.emts.cabbi.fcm.MyFireBaseMessagingService;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.PreferenceHelper;
import com.emts.cabbi.model.Booking;
import com.emts.cabbi.model.NotificationModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.HashMap;


/**
 * Created by Srijana on 7/6/2017.
 */

public class SplashActivity extends CabbiBaseActivity {
    private final int splashTimer = 3000;
    PreferenceHelper prefHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FirebaseApp.initializeApp(getApplicationContext());
        super.onCreate(savedInstanceState);

//       //heck for fcm system tray notification launch
        if (checkIfFCM()) {
            finish();
            return;
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        setContentView(R.layout.activity_splash);

        prefHelper = PreferenceHelper.getInstance(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (prefHelper.isLogin()) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    SplashActivity.this.finish();
                } else {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    SplashActivity.this.finish();
                }
            }
        }, splashTimer);

        //update devicePush toke if not
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String deviceToken = instanceIdResult.getToken();
                Logger.e("Splash Device Token", deviceToken);
//                if (!PreferenceHelper.getInstance(SplashActivity.this).getBoolean(PreferenceHelper.FCM_TOKEN_UPDATED, false)) {
                    if (prefHelper.isLogin()) {
                        MyFireBaseMessagingService.registerDeviceTokenToServer(deviceToken, SplashActivity.this);
                    }
//                }
            }
        });
    }

    private boolean checkIfFCM() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return false;
        }

        String notType = bundle.getString("note_type");
        //bundle must contain all info sent in "data" field of the notification
        boolean isBookingNotification = false;
        if (notType == null) return false;
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        HashMap<String, String> dataMap = new HashMap<>();
        for (String key : bundle.keySet()) {
            dataMap.put(key, bundle.get(key).toString()); //To Implement
        }
        switch (notType) {
            case NotificationModel.NOT_BOOKING:
                //send notification
                Booking booking = BookingParser.bookingFromPush(dataMap);

                intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("booking", booking);
                Logger.e("MyFirebaseActivity Booking Alert",
                        "Activity is not in forGround came from system tray");
                isBookingNotification = true;
                break;
            case NotificationModel.NOT_TYPE_TRIP_COMPLETE:
                //goto trip of the day
                intent = new Intent(getApplicationContext(), TripDayActivity.class);
                intent.putExtra("tripDate", dataMap.get("date"));
                break;
            case NotificationModel.NOT_TYPE_UPDATE_APPROVE:
                //goto view profile
                intent = new Intent(getApplicationContext(), DriverProfileActivity.class);
                break;
            case NotificationModel.NOT_TYPE_UPDATE_REJECT:
                //goto help center
                intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("helpCenter", true);
                break;
            case NotificationModel.NOT_TYPE_TRIP_CANCEL:
                intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("acceptBooking", true); //NOT really accept booking but
                // that extra will open mybookings fragment so reuse
                break;
//            case NotificationModel.NOT_ADMIN_ALERT:
//                intent = new Intent(getApplicationContext(), MainActivity.class);
//                intent.putExtra("adminAlert", true);
//                break;
//            case NotificationModel.NOT_DRIVER_APPROVED:
//                intent = new Intent(getApplicationContext(), LoginActivity.class);
//                break;
            case NotificationModel.NOT_CASHOUT_ACCEPTED:
                return false;//this will stay in splash and proceed
            case NotificationModel.NOT_CASHOUT_REJECTED:
                return false;//this will stay in splash and proceed
        }
        startActivity(intent);

        return true;
    }
}
