package com.emts.cabbi.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.BookingParser;
import com.emts.cabbi.R;
import com.emts.cabbi.fragment.AboutFrag;
import com.emts.cabbi.fragment.HomeFrag;
import com.emts.cabbi.fragment.MyBookingTripFrag;
import com.emts.cabbi.fragment.MyWalletFrag;
import com.emts.cabbi.fragment.PickUpFrag;
import com.emts.cabbi.fragment.ProfileFrag;
import com.emts.cabbi.fragment.TripHistoryFrag;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.PreferenceHelper;
import com.emts.cabbi.helper.VolleyHelper;
import com.emts.cabbi.model.Booking;
import com.emts.cabbi.periodicupdate.LocUpdateScheduler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;

public class MainActivity extends BookingBaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    Toolbar toolbar;
    PreferenceHelper prefsHelper;
    FragmentManager fm;
    DrawerLayout drawer;
    MenuItem menuStatus;
    TextView toolbarTitle;
    PopupWindow popupWindow;
    TextView tvOnlineStatus, tvOfflineStatus, tvBreakStatus, tvVehiclePlate;
    ImageView profilePic;

    View views;

    @Override
    protected void onStart() {
        super.onStart();
        currentActivity = this;
    }

    @Override
    protected void onStop() {
//        currentActivity = null;
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        checkVersion();
        //loc update scheduler
        LocUpdateScheduler.schedule(this, true);

        prefsHelper = PreferenceHelper.getInstance(this);

        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.custom_toolbar_title);
        toolbar.inflateMenu(R.menu.status_menu);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if (!popupWindow.isShowing())
                    showPopUpWindow();
                else
                    hidePopUpWindow();
                return false;
            }
        });
        drawer = findViewById(R.id.drawer_layout);
        fm = getSupportFragmentManager();
        NavigationView navigationView = findViewById(R.id.nav_view);
        views = navigationView.getHeaderView(0);
        setHeaderData();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        createPopUpWindow();

        toolbar.post(new Runnable() {
            @Override
            public void run() {
                menuStatus = toolbar.getMenu().findItem(R.id.status);

                switch (prefsHelper.getDriverStatus()) {
                    case PreferenceHelper.DRIVER_ONLINE:
                        statusMenuIcon(R.id.tv_status_online);
                        break;
                    case PreferenceHelper.DRIVER_OFFLINE:
                        statusMenuIcon(R.id.tv_status_offline);
                        break;
                    case PreferenceHelper.DRIVER_BREAK:
                        statusMenuIcon(R.id.tv_status_break);
                        break;
                }
            }
        });

        Intent intent = getIntent();
        //In case of booking push there will be booking on intent
        Booking booking = (Booking) intent.getSerializableExtra("booking");
        if (intent.getBooleanExtra("acceptBooking", false)) {
            openMyBookingFrag();
            return;
        } else if (booking != null) {
//            showBookingAlert(this, booking);
            //first validate the booking and show alert
            validateBookingTask(booking);
            //from recent showing alert
            intent.putExtra("booking", (Serializable) null);
        }

        //to show my booking frag if there is Booking Taken Already else to show Home Frag
//        fm.beginTransaction().replace(R.id.content_frame, new HomeFrag(), "HomeFrag").commit();
        Fragment fragment = new MyBookingTripFrag();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isFirstPageShow", true);
        fragment.setArguments(bundle);
        fm.beginTransaction().replace(R.id.content_frame, fragment, fragment.getClass().getSimpleName()).commit();
        setToolbarTitle("");
    }

    private void validateBookingTask(final Booking booking) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(MainActivity.this, "Validating booking...");
        VolleyHelper volleyHelper = VolleyHelper.getInstance(getApplicationContext());
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("booking_id", booking.getBookingNo());
        volleyHelper.addVolleyRequestListeners(Api.getInstance().validateBookingApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                showBookingAlert(MainActivity.this, booking);
                            } else {
                                AlertUtils.simpleAlert(MainActivity.this, "Error",
                                        resObj.getString("message"), "OK", "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("validateBookingTask res ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected error !!! Please try again with internet access";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("getNotificationCountTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(MainActivity.this, "Error",
                                errorMsg, "OK", "", null);
                    }
                }, "validateBookingTask");
    }

    private void createPopUpWindow() {
        View contentView = getLayoutInflater().inflate(R.layout.layout_status_popup_menu, null);
        popupWindow = new PopupWindow(contentView, ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, false);

        tvOnlineStatus = contentView.findViewById(R.id.tv_status_online);
        tvOfflineStatus = contentView.findViewById(R.id.tv_status_offline);
        tvBreakStatus = contentView.findViewById(R.id.tv_status_break);

        tvOnlineStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext()))
                    setDriverStatusTask(view.getId());
                else
                    AlertUtils.showSnack(MainActivity.this, toolbar, getResources().getString(R.string.no_internet));
            }
        });

        tvOfflineStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                AlertUtils.simpleAlert(MainActivity.this, "Update online status",
                        "You will not receive any booking notification if you turn your status \"OFFLINE\"",
                        "OK", "Cancel", new AlertUtils.OnAlertButtonClickListener() {
                            @Override
                            public void onAlertButtonClick(boolean isPositiveButton) {
                                if (isPositiveButton) {
                                    if (NetworkUtils.isInNetwork(getApplicationContext()))
                                        setDriverStatusTask(view.getId());
                                    else
                                        AlertUtils.showSnack(MainActivity.this, toolbar, getResources().getString(R.string.no_internet));
                                }
                            }
                        });
            }
        });

        tvBreakStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                AlertUtils.simpleAlert(MainActivity.this, "Update online status",
                        "You will not receive any instant booking if you turn you active status to \"BREAK\". You will only receive pre-booking",
                        "OK", "Cancel", new AlertUtils.OnAlertButtonClickListener() {
                            @Override
                            public void onAlertButtonClick(boolean isPositiveButton) {
                                if (isPositiveButton) {
                                    if (NetworkUtils.isInNetwork(getApplicationContext()))
                                        setDriverStatusTask(view.getId());
                                    else
                                        AlertUtils.showSnack(MainActivity.this, toolbar, getResources().getString(R.string.no_internet));
                                }
                            }
                        });
            }
        });

        popupWindow.setContentView(contentView);
//        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.setElevation(12);
        popupWindow.setOutsideTouchable(false);
    }

    private void setDriverStatusTask(final int id) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        if (id == R.id.tv_status_online)
            postParams.put("new_status", "1");
        else if (id == R.id.tv_status_offline)
            postParams.put("new_status", "0");
        else if (id == R.id.tv_status_break)
            postParams.put("new_status", "2");

        vHelper.addVolleyRequestListeners(Api.getInstance().changeDriverStatusApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                statusMenuIcon(id);
//                                AlertUtils.simpleAlert(MainActivity.this, "Status Updated", res.getString("message"),
//                                        "OK", "", null);
                            } else {
                                AlertUtils.simpleAlert(MainActivity.this, "Error", res.getString("message"),
                                        "OK", "", null);
                            }
                        } catch (JSONException e) {
                            Logger.e("setStatusTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("setStatusTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(MainActivity.this, "Error", errorMsg, "OK", "", null);
                    }
                }, "setStatusTask");
    }

    @Override
    protected void onResume() {
        currentActivity = this;
        super.onResume();
        tvVehiclePlate.setText(prefsHelper.getUserVehiclePlateNo());

        Glide.with(MainActivity.this).load(prefsHelper.getAppUserImg()).into(profilePic);
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), DriverProfileActivity.class));
            }
        });

//        BookingParser.getBookingAlertTest(this);
    }

    private void statusMenuIcon(int id) {
        if (id == R.id.tv_status_online) {
            menuStatus.setIcon(R.drawable.btn_online);
            tvOnlineStatus.setBackgroundResource(R.color.colorAccent);
            prefsHelper.edit().putString(PreferenceHelper.DRIVER_STATUS, PreferenceHelper.DRIVER_ONLINE).apply();

            tvOfflineStatus.setBackgroundResource(R.color.white);
            tvBreakStatus.setBackgroundResource(R.color.white);
        } else if (id == R.id.tv_status_offline) {
            menuStatus.setIcon(R.drawable.btn_offline);
            tvOnlineStatus.setBackgroundResource(R.color.white);
            tvOfflineStatus.setBackgroundResource(R.color.colorAccent);
            prefsHelper.edit().putString(PreferenceHelper.DRIVER_STATUS, PreferenceHelper.DRIVER_OFFLINE).apply();

            tvBreakStatus.setBackgroundResource(R.color.white);
        } else if (id == R.id.tv_status_break) {
            menuStatus.setIcon(R.drawable.btn_break);
            tvOnlineStatus.setBackgroundResource(R.color.white);
            tvOfflineStatus.setBackgroundResource(R.color.white);
            tvBreakStatus.setBackgroundResource(R.color.colorAccent);

            prefsHelper.edit().putString(PreferenceHelper.DRIVER_STATUS, PreferenceHelper.DRIVER_BREAK).apply();
        }
        hidePopUpWindow();
    }

    private void showPopUpWindow() {
        if (!popupWindow.isShowing())
            popupWindow.showAsDropDown(toolbar, 0, 0, Gravity.END);
    }

    private void hidePopUpWindow() {
        if (popupWindow.isShowing())
            popupWindow.dismiss();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStack();
            setToolbarTitle("");
        } else {
            VolleyHelper.getInstance(this).cancelAllRequests();
//            super.onBackPressed();
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        recycle();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = fm.findFragmentById(R.id.content_frame);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Intent intent = null;
        Fragment fragment = null;
        Bundle bundle = new Bundle();

        item.setChecked(true);

        switch (item.getItemId()) {
            case R.id.nav_profile:
                fragment = new ProfileFrag();
                break;
            case R.id.nav_home:
                fragment = new HomeFrag();
                break;
            case R.id.nav_booking_trip:
                fragment = new MyBookingTripFrag();
                break;
            case R.id.nav_wallet:
                fragment = new MyWalletFrag();
                break;
            case R.id.nav_trip_history:
                fragment = new TripHistoryFrag();
                break;
            case R.id.nav_notification:
                intent = new Intent(getApplicationContext(), NotificationActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_about:
                fragment = new AboutFrag();
                break;
            case R.id.nav_app_version:
                intent = new Intent(getApplicationContext(), AppVersionActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_logout:
                if (NetworkUtils.isInNetwork(MainActivity.this)) {
                    logoutTask();
                    PreferenceHelper.getInstance(MainActivity.this).edit().putBoolean(PreferenceHelper.IS_LOGIN, false).apply();
                    Intent intent1 = new Intent(getApplicationContext(), LoginActivity.class);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent1);
                } else {
                    AlertUtils.showSnack(MainActivity.this, toolbar, getString(R.string.no_internet));
                }
                break;
        }

        if (fragment != null) {
            fm.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            drawer.closeDrawer(GravityCompat.START);
            setToolbarTitle("");
        }
        return false;
    }

    public void openHomePage() {
        Fragment fragment = new HomeFrag();
        fm.beginTransaction().replace(R.id.content_frame, fragment)
                .commit();
        setToolbarTitle("");
    }

    public void openMyBookingFrag() {
        Fragment fragment = new MyBookingTripFrag();
        fm.beginTransaction().replace(R.id.content_frame, fragment)
                .commit();
        drawer.closeDrawer(GravityCompat.START);
        setToolbarTitle("");
    }

    public void goForPickUp(Booking booking) {
        Fragment fragment = new PickUpFrag();
        Bundle bundle = new Bundle();
        bundle.putSerializable("booking", booking);
        fragment.setArguments(bundle);
        fm.beginTransaction().replace(R.id.content_frame, fragment)
                .addToBackStack(null)
                .commit();
        drawer.closeDrawer(GravityCompat.START);
        setToolbarTitle("Pick Up");
    }

    public void goForDropOff(Booking booking) {
        Fragment fragment = new PickUpFrag();
        Bundle bundle = new Bundle();
        bundle.putSerializable("booking", booking);
        fragment.setArguments(bundle);
        fm.beginTransaction().replace(R.id.content_frame, fragment)
                .addToBackStack(null)
                .commit();
        drawer.closeDrawer(GravityCompat.START);
        setToolbarTitle("End Trip");
    }

    private void logoutTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().logoutApi, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                            } else {
                            }
                        } catch (JSONException e) {
                            Logger.e("logoutTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("logoutTask error ex", e.getMessage() + " ");
                        }
                        Logger.e("logoutTask error msg", errorMsg);
                    }
                }, "logoutTask");
    }

    public void setToolbarTitle(final String title) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (menuStatus != null) {
                    menuStatus.setIcon(0);
                    menuStatus.setEnabled(false);
                }
                Fragment fragment = fm.findFragmentById(R.id.content_frame);
                if (fragment instanceof ProfileFrag) {
                    toolbarTitle.setText("Profile");
                } else if (fragment instanceof HomeFrag) {
                    menuStatus.setEnabled(true);
                    menuStatus.setVisible(true);
                    menuStatus.setIcon(R.drawable.btn_online);
                    toolbarTitle.setText("Home");
                } else if (fragment instanceof TripHistoryFrag) {
                    toolbarTitle.setText("Trip History");
                } else if (fragment instanceof AboutFrag) {
                    toolbarTitle.setText("About Us");
                } else if (fragment instanceof MyWalletFrag) {
                    toolbarTitle.setText("My Wallet");
                } else if (fragment instanceof MyBookingTripFrag) {
                    toolbarTitle.setText("My Booking Trip");
                } else if (fragment instanceof PickUpFrag) {
                    toolbarTitle.setText(title);
                }
            }

        }, 200);
    }

    private void setHeaderData() {
        LinearLayout layAppHeader = views.findViewById(R.id.lay_header);
        profilePic = layAppHeader.findViewById(R.id.iv_driver_pic);

        Logger.e("profile pic", prefsHelper.getAppUserImg());
        Glide.with(MainActivity.this)
                .load(prefsHelper.getAppUserImg())
                .apply(RequestOptions.circleCropTransform())
                .into(profilePic);
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), DriverProfileActivity.class));
            }
        });

        TextView tvDriverName = layAppHeader.findViewById(R.id.tv_driver_name);
        tvDriverName.setText(prefsHelper.getAppUserFullName());
        tvDriverName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), DriverProfileActivity.class));
            }
        });

        tvVehiclePlate = layAppHeader.findViewById(R.id.tv_vehicle_plate);
        tvVehiclePlate.setText(prefsHelper.getUserVehiclePlateNo());

        ImageView edtProfile = layAppHeader.findViewById(R.id.edt_profile);
        edtProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), DriverProfileActivity.class));
            }
        });
    }
}
