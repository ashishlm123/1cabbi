package com.emts.cabbi.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.R;
import com.emts.cabbi.adapter.EmergencyHotlineAdapter;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.VolleyHelper;
import com.emts.cabbi.model.EmergencyModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class EmergencyHotLineActivity extends BookingBaseActivity {
    ArrayList<EmergencyModel> emergencyLists;
    EmergencyHotlineAdapter emergencyHotlineAdapter;
    RecyclerView rvEmergencyNumberListings;
    ProgressBar progressBar, infiniteProgressBar;
    TextView tvErrorText;

    @Override
    protected void onResume() {
        currentActivity = this;
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentActivity = this;
    }

    @Override
    protected void onStop() {
//        currentActivity = null;
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_hot_line);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Emergency Hotline");

        progressBar = findViewById(R.id.progress_bar);
        infiniteProgressBar = findViewById(R.id.infinite_progress_bar);
        tvErrorText = findViewById(R.id.error_text);

        rvEmergencyNumberListings = findViewById(R.id.recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvEmergencyNumberListings.setLayoutManager(linearLayoutManager);

        emergencyLists = new ArrayList<>();

        emergencyHotlineAdapter = new EmergencyHotlineAdapter(EmergencyHotLineActivity.this, emergencyLists);
        rvEmergencyNumberListings.setAdapter(emergencyHotlineAdapter);

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            emergencyTask();
        } else {
            progressBar.setVisibility(View.GONE);
            rvEmergencyNumberListings.setVisibility(View.GONE);
            tvErrorText.setText(getResources().getString(R.string.no_internet));
            tvErrorText.setVisibility(View.VISIBLE);
        }
    }

    private void emergencyTask() {
        progressBar.setVisibility(View.VISIBLE);
        tvErrorText.setVisibility(View.GONE);
        rvEmergencyNumberListings.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().emergencyHotLineApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray emergencyArray = res.getJSONArray("emergency_hotlines");
                                for (int i = 0; i < emergencyArray.length(); i++) {
                                    JSONObject eachEmerge = emergencyArray.getJSONObject(i);
                                    EmergencyModel emergencyModel = new EmergencyModel();
                                    emergencyModel.setEmergencyNumber(eachEmerge.getString("contact_number"));
                                    emergencyModel.setEmergencyName(eachEmerge.getString("contact_name"));

                                    emergencyLists.add(emergencyModel);
                                }
                                emergencyHotlineAdapter.notifyDataSetChanged();
                                rvEmergencyNumberListings.setVisibility(View.VISIBLE);
                                tvErrorText.setVisibility(View.GONE);
                            } else {
                                rvEmergencyNumberListings.setVisibility(View.GONE);
                                tvErrorText.setText(res.getString("message"));
                                tvErrorText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Logger.e("emergencyTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("emergencyTask error ex", e.getMessage() + " ");
                        }
                        rvEmergencyNumberListings.setVisibility(View.GONE);
                        tvErrorText.setText(errorMsg);
                        tvErrorText.setVisibility(View.VISIBLE);
                    }
                }, "emergencyTask");
    }
}
