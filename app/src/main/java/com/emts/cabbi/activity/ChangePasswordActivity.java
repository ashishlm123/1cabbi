package com.emts.cabbi.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.R;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ChangePasswordActivity extends BookingBaseActivity {
    EditText edtCurrentPassword, edtNewPassword, edtConfirmPassword;
    TextView tvErrCurrentPassword, tvErrNewPassword, tvErrConfirmPassword;

    @Override
    protected void onResume() {
        currentActivity = this;
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentActivity = this;
    }

    @Override
    protected void onStop() {
//        currentActivity = null;
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Change Password");

        edtCurrentPassword = findViewById(R.id.edt_current_password);
        edtNewPassword = findViewById(R.id.edt_new_password);
        edtConfirmPassword = findViewById(R.id.edt_confirm_password);

        tvErrCurrentPassword = findViewById(R.id.err_curr_passwd);
        tvErrNewPassword = findViewById(R.id.err_new_passwd);
        tvErrConfirmPassword = findViewById(R.id.err_con_password);

        final Button btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        changePasswordTask();
                    } else {
                        AlertUtils.showSnack(ChangePasswordActivity.this, btnSave, getResources().getString(R.string.no_internet));
                    }
                }
            }
        });
    }

    private boolean validation() {
        boolean isValid = true;

        if (edtCurrentPassword.getText().toString().trim().equals("")) {
            tvErrCurrentPassword.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrCurrentPassword.setVisibility(View.GONE);
        }

        if (edtNewPassword.getText().toString().trim().equals("")) {
            tvErrNewPassword.setVisibility(View.VISIBLE);
            isValid = false;
        } else {
            tvErrNewPassword.setVisibility(View.GONE);
        }

        if (edtConfirmPassword.getText().toString().trim().equals("")) {
            tvErrConfirmPassword.setVisibility(View.VISIBLE);
            tvErrConfirmPassword.setText("This field is required...");
            isValid = false;
        } else {
            if (!edtConfirmPassword.getText().toString().trim().equals(edtNewPassword.getText().toString().trim())) {
                tvErrConfirmPassword.setVisibility(View.VISIBLE);
                tvErrConfirmPassword.setText("Password do not match...");
                isValid = false;
            } else {
                tvErrConfirmPassword.setVisibility(View.GONE);
            }
        }
        return isValid;
    }

    private void changePasswordTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(ChangePasswordActivity.this, "Please wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("old_password", edtCurrentPassword.getText().toString().trim());
        postParams.put("new_password", edtConfirmPassword.getText().toString().trim());

        vHelper.addVolleyRequestListeners(Api.getInstance().changePasswordApi, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        AlertUtils.simpleAlert(ChangePasswordActivity.this, "Success",
                                res.getString("message"), "OK", "", null);
                        edtCurrentPassword.setText("");
                        edtNewPassword.setText("");
                        edtConfirmPassword.setText("");
                    } else {
                        AlertUtils.simpleAlert(ChangePasswordActivity.this, "Error",
                                res.getString("message"), "OK", "", null);
                    }
                } catch (JSONException e) {
                    Logger.e("changePasswordTask json ex", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                pDialog.dismiss();
                String errorMsg = "Unexpected Error, Please try again with internet access!";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (JSONException e) {
                    Logger.e("changePasswordTask error ex", e.getMessage() + " ");
                }
                AlertUtils.simpleAlert(ChangePasswordActivity.this, "Error", errorMsg,
                        "OK", "", null);
            }
        }, "changePasswordTask");
    }

}
