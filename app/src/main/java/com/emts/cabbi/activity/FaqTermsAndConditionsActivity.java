package com.emts.cabbi.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.R;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class FaqTermsAndConditionsActivity extends BookingBaseActivity {
    boolean isFaq, isPrivacy, isClientTnC;
    ProgressBar progressBar;
    TextView tvError, tvFaqTerms;

    @Override
    protected void onResume() {
        currentActivity = this;
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentActivity = this;
    }

    @Override
    protected void onStop() {
//        currentActivity = null;
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_faq_terms_and_conditions);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);

        Intent intent = getIntent();
        isFaq = intent.getBooleanExtra("isFaq", false);
        isPrivacy = intent.getBooleanExtra("privacy_policy", false);
        isClientTnC = intent.getBooleanExtra("client_terms", false);

        if (isFaq) {
            toolbarTitle.setText("FAQ");
        } else if (isPrivacy) {
            toolbarTitle.setText("Privacy Policy");
        } else if (isClientTnC) {
            toolbarTitle.setText("Client Terms and Conditions");
        } else {
            toolbarTitle.setText("Terms and Conditions");
        }

        progressBar = findViewById(R.id.progress);
        tvError = findViewById(R.id.tvError);
        tvFaqTerms = findViewById(R.id.tv_faq_tnc);

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            faqTermsTask();
        } else {
            AlertUtils.showSnack(FaqTermsAndConditionsActivity.this, toolbar, getResources().getString(R.string.no_internet));
            tvFaqTerms.setText(getResources().getString(R.string.no_internet));
        }
    }

    private void faqTermsTask() {
        progressBar.setVisibility(View.VISIBLE);
        tvError.setVisibility(View.GONE);
        tvFaqTerms.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        String url = "";
        if (isFaq) {
            url = Api.getInstance().faqApi;
        } else if (isPrivacy) {
            url = Api.getInstance().privacyPolicyApi;
        } else if (isClientTnC) {
            url = Api.getInstance().clientTermsApi;
        } else {
            url = Api.getInstance().termsAndConditionsApi;
        }

        vHelper.addVolleyRequestListeners(url, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        StringBuilder content = new StringBuilder();
                        if (isFaq) {
                            JSONArray faqs = res.getJSONArray("faq");
                            for (int i = 0; i < faqs.length(); i++) {
                                JSONObject eachFaq = faqs.getJSONObject(i);

                                content.append(i + 1)
                                        .append(". ")
                                        .append(eachFaq.getString("faq_topic"))
                                        .append("<br>")
                                        .append(eachFaq.getString("faq_desc"))
                                        .append("<br><br>");
                            }
                            tvFaqTerms.setTextColor(Color.BLACK);
                        } else if (isPrivacy || isClientTnC) {
                            content.append(res.getJSONObject("about_us_data").getString("content"));
                        } else {
                            content.append(res.getJSONObject("terms_data").getString("content"));
                        }
                        tvFaqTerms.setText(Html.fromHtml(content.toString()));
                        tvFaqTerms.setVisibility(View.VISIBLE);
                        tvError.setVisibility(View.GONE);
                    } else {
                        tvFaqTerms.setVisibility(View.GONE);
                        tvError.setText(res.getString("message"));
                        tvError.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    Logger.e("faqTermsTask json ex", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressBar.setVisibility(View.GONE);
                String errorMsg = "Unexpected Error, Please try again with internet access!";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (JSONException e) {
                    Logger.e("faqTermsTask error ex", e.getMessage() + " ");
                }
                tvFaqTerms.setVisibility(View.GONE);
                tvError.setText(Html.fromHtml(errorMsg));
                tvError.setVisibility(View.VISIBLE);
            }
        }, "faqTermsTask");
    }

}
