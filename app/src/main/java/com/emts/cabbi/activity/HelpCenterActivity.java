package com.emts.cabbi.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.BookingBaseActivity;
import com.emts.cabbi.R;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.PreferenceHelper;
import com.emts.cabbi.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class HelpCenterActivity extends BookingBaseActivity {
    EditText edtMessage;

    @Override
    protected void onResume() {
        currentActivity = this;
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        currentActivity = this;
    }

    @Override
    protected void onStop() {
//        currentActivity = null;
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_help_center);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.custom_toolbar_title);
        toolbarTitle.setText("Help Center");

        final PreferenceHelper prefHelper = PreferenceHelper.getInstance(this);
        TextView btnCallSupport = findViewById(R.id.btn_call_support);
        btnCallSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" +
                        prefHelper.getContactNo()));
                startActivity(intent);
            }
        });
        if (TextUtils.isEmpty(prefHelper.getContactNo())) {
            btnCallSupport.setVisibility(View.GONE);
        }

        edtMessage = findViewById(R.id.edt_message_field);

        final Button btnSend = findViewById(R.id.btn_send);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext()))
                    contactHelpCenterTask(edtMessage.getText().toString().trim());
                else
                    AlertUtils.showSnack(HelpCenterActivity.this, btnSend, getResources().getString(R.string.no_internet));
            }
        });

    }

    private void contactHelpCenterTask(String message) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(HelpCenterActivity.this, "Please wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        // remember ::: User information such as full name, ic no, email, and contact no shall be send along with form.
        postParams.put("message", message);

        vHelper.addVolleyRequestListeners(Api.getInstance().helpCenterApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                edtMessage.setText("");
                                AlertUtils.simpleAlert(HelpCenterActivity.this, "Message send success",
                                        res.getString("message"));
                            } else {
                                AlertUtils.simpleAlert(HelpCenterActivity.this, "Error !!!",
                                        res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("contactHelpCenterTask json ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        pDialog.dismiss();
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("contactHelpCenterTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(HelpCenterActivity.this, "Error !!!", errorMsg);
                    }
                }, "contactHelpCenterTask");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        checkFromPush(getIntent());
    }
}
