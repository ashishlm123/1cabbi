package com.emts.cabbi;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.cabbi.helper.AlertUtils;
import com.emts.cabbi.helper.Api;
import com.emts.cabbi.helper.Logger;
import com.emts.cabbi.helper.NetworkUtils;
import com.emts.cabbi.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

public abstract class CabbiBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showUpdateVersionAlert() {
        AlertUtils.simpleAlert(CabbiBaseActivity.this, "Update App", "A new version of "
                        + getString(R.string.app_name) + " is available in store.\nDownload Now??", "Update",
                "Cancel", new AlertUtils.OnAlertButtonClickListener() {
                    @Override
                    public void onAlertButtonClick(boolean isPositiveButton) {
                        if (isPositiveButton) {
                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    }
                });
    }

    public void checkVersion() {
        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            versionCheckTask();
        }
    }

    @Override
    protected void onDestroy() {
        VolleyHelper.getInstance(this).cancelRequest("versionCheckTask");
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        VolleyHelper.getInstance(this).cancelRequest("versionCheckTask");
        super.onStop();
    }

    private void versionCheckTask() {
        VolleyHelper volleyHelper = VolleyHelper.getInstance(getApplicationContext());
        volleyHelper.addVolleyRequestListeners(Api.getInstance().versionCheckApi, Request.Method.POST,
                volleyHelper.getPostParams(), new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                if (res.getInt("app_version_code") > BuildConfig.VERSION_CODE) {
                                    showUpdateVersionAlert();
                                }
                            }
                        } catch (JSONException e) {
                            Logger.e("versionCheckTaask res ex", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {

                    }
                }, "versionCheckTask");
    }

}
