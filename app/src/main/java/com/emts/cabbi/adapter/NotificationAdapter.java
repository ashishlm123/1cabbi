package com.emts.cabbi.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.cabbi.R;
import com.emts.cabbi.model.NotificationModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<NotificationModel> notificationLists;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy");

    public NotificationAdapter(Context context, ArrayList<NotificationModel> notificationLists) {
        this.context = context;
        this.notificationLists = notificationLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        NotificationModel notificationModel = notificationLists.get(position);

        try {
            viewHolder.tvNotificationDate.setText(sdf1.format(sdf.parse(notificationModel.getNotificationDate())));
        } catch (Exception e) {
            viewHolder.tvNotificationDate.setText(notificationModel.getNotificationDate());
        }
        viewHolder.tvNotificationBody.setText(Html.fromHtml(notificationModel.getNotificationBody()));
        if (!notificationModel.isSeen()) {
            viewHolder.tvNotificationDate.setAlpha(1f);
            viewHolder.tvNotificationBody.setAlpha(1f);
        } else {
            viewHolder.tvNotificationDate.setAlpha(0.6f);
            viewHolder.tvNotificationBody.setAlpha(0.6f);
        }
    }

    @Override
    public int getItemCount() {
        return notificationLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvNotificationDate, tvNotificationBody;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNotificationDate = itemView.findViewById(R.id.tv_notification_date);
            tvNotificationBody = itemView.findViewById(R.id.tv_notification_body);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        listener.onRecyclerItemClickListener(getLayoutPosition());
                    }
                }
            });
        }
    }

    public interface RecyclerItemClickListener {
        void onRecyclerItemClickListener(int position);
    }

    private RecyclerItemClickListener listener;

    public void setOnRecyclerViewItemClickListener(RecyclerItemClickListener listener) {
        this.listener = listener;
    }

}
