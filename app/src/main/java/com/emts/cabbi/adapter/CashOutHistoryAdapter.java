package com.emts.cabbi.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.cabbi.R;
import com.emts.cabbi.model.CashOutHistoryModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CashOutHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<CashOutHistoryModel> cashOutHistoryLists;
    private Context context;
    private static final String STATUS_APPROVED = "1";
    private static final String STATUS_PENDING = "0";
    private static final String STATUS_REJECTED = "2";
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy");

    public CashOutHistoryAdapter(Context context, ArrayList<CashOutHistoryModel> cashOutHistoryLists) {
        this.context = context;
        this.cashOutHistoryLists = cashOutHistoryLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_cash_out_history,
                viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        ViewHolder viewHolder = (ViewHolder) holder;
        CashOutHistoryModel cashOutHistoryModel = cashOutHistoryLists.get(i);

        try {
            Date date = sdf.parse(cashOutHistoryModel.getCashOutDate());
            viewHolder.tvCashOutDate.setText(sdf1.format(date));
        } catch (ParseException e) {
            viewHolder.tvCashOutDate.setText(cashOutHistoryModel.getCashOutDate());
        }
        viewHolder.tvRequestId.setText(cashOutHistoryModel.getCashOutRequestId());
        viewHolder.tvCashOutAmt.setText(cashOutHistoryModel.getCashOutAmt());

        switch (cashOutHistoryModel.getCashOutStatus()) {
            case STATUS_APPROVED:
                viewHolder.tvCashOutStatus.setTextColor(context.getResources().getColor(R.color.textGreen));
                viewHolder.tvCashOutStatus.setText("Approved");
                break;
            case STATUS_PENDING:
                viewHolder.tvCashOutStatus.setTextColor(context.getResources().getColor(R.color.textAccent));
                viewHolder.tvCashOutStatus.setText("Pending");
                break;
            default:
                viewHolder.tvCashOutStatus.setTextColor(context.getResources().getColor(R.color.appRedDark));
                viewHolder.tvCashOutStatus.setText("Rejected");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return cashOutHistoryLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvCashOutDate, tvRequestId, tvCashOutAmt, tvCashOutStatus;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvCashOutDate = itemView.findViewById(R.id.tv_cash_out_date);
            tvRequestId = itemView.findViewById(R.id.tv_request_id);
            tvCashOutAmt = itemView.findViewById(R.id.tv_cash_out_amt);
            tvCashOutStatus = itemView.findViewById(R.id.tv_cash_out_status);
        }
    }
}
