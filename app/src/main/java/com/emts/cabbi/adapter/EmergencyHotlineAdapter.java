package com.emts.cabbi.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.cabbi.R;
import com.emts.cabbi.model.EmergencyModel;

import java.util.ArrayList;

public class EmergencyHotlineAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<EmergencyModel> emergencyLists;

    public EmergencyHotlineAdapter(Context context, ArrayList<EmergencyModel> emergencyLists) {
        this.context = context;
        this.emergencyLists = emergencyLists;
    }

    @NonNull
    @Override

    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_emergency_hotline, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        ViewHolder viewHolder = (ViewHolder) holder;
        EmergencyModel emergencyModel = emergencyLists.get(i);

        viewHolder.tvEmergencyNumber.setText(emergencyModel.getEmergencyNumber());
        viewHolder.tvEmergencyName.setText(emergencyModel.getEmergencyName());

        if (emergencyLists.size() - 1 == i) {
            viewHolder.lastDivider.setVisibility(View.GONE);
        } else {
            viewHolder.lastDivider.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return emergencyLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvEmergencyName, tvEmergencyNumber;
        View lastDivider;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvEmergencyName = itemView.findViewById(R.id.tv_emergency_name);
            tvEmergencyNumber = itemView.findViewById(R.id.tv_emergency_num);
            tvEmergencyNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + emergencyLists.get(getLayoutPosition()).getEmergencyNumber()));
                    context.startActivity(intent);
                }
            });

            lastDivider = itemView.findViewById(R.id.view);
        }
    }
}
